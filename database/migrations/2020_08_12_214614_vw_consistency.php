<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class VwConsistency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("DROP VIEW IF EXISTS vw_consistency");

        DB::statement("CREATE VIEW vw_consistency AS
                       SELECT `i`.`idr` AS `idr`,`w`.`id_weight` AS `id_weight`,`i`.`value` AS `value`,`c`.`jumlah_ratio` AS `jumlah_ratio`,`c`.`n` AS `n`,`c`.`jumlah_ratio` / `c`.`n` AS `lamda_max`,(`c`.`jumlah_ratio` / `c`.`n` - `c`.`n`) / (`c`.`n` - 1) AS `CI`,(`c`.`jumlah_ratio` / `c`.`n` - `c`.`n`) / (`c`.`n` - 1) / `i`.`value` AS `CR`,IF(IFNULL((`c`.`jumlah_ratio` / `c`.`n` - `c`.`n`) / (`c`.`n` - 1) / `i`.`value`, -0.1) <  0.1 ,'ACCEPTED','REJECTED') AS `consistency` 
                        FROM ((`criterias` `a` LEFT JOIN (SELECT `criterias`.`id_criteria` AS `id_criteria`,SUM(`criterias`.`total_multiple`) + 1 AS `jumlah_ratio`,CONCAT('IDR00',COUNT(`criterias`.`id_criteria`) )AS `idr`, COUNT(`criterias`.`id_criteria`) AS n FROM `criterias`) `c` ON(`a`.`id_criteria` = `c`.`id_criteria`)) 
                        LEFT JOIN (SELECT `index_random`.`idr` AS `idr`,`index_random`.`value` AS `value` FROM `index_random`) `i` ON (`i`.`idr` = `c`.`idr`)) 
                        LEFT JOIN (SELECT `weights`.`id_weight`,`weights`.`criteria_row` FROM `weights`) `w` ON (`c`.`id_criteria` = `w`.`criteria_row`)
                        WHERE `c`.`jumlah_ratio` IS NOT NULL
                        GROUP BY `i`.`idr`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
