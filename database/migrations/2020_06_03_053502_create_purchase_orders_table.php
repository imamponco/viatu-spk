<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->string('id_po',7)->primary();
            $table->string('id_customer',7);
            $table->string('id_user',7);
            // $table->string('po_code',20)->nullable();
            $table->date('date_order');
            $table->date('deadline');
            $table->date('date_production')->nullable();
            $table->date('date_finish')->nullable();
            $table->smallInteger('status');
            // $table->smallInteger('status_verification')->nullable();
            $table->string('resi_code',30)->nullable();
            $table->timestamps();

            $table->foreign('id_user')->references('id_user')->on('users')->constrained()->onUpdate('cascade');
            $table->foreign('id_customer')->references('id_customer')->on('customers')->constrained()->onDelete('cascade')->onUpdate('cascade');
        });


        Schema::create('additional', function (Blueprint $table) {
            $table->string('id_add',7)->primary();
            $table->string('id_po',7)->nullable();
            $table->string('add_name',20);
            $table->double('add_price',8,2);
            $table->double('add_unit',8,2);
            $table->text('add_description');
            $table->timestamps();

            $table->foreign('id_po')->references('id_po')->on('purchase_orders')->constrained()->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
