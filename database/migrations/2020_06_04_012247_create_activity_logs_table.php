<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_logs', function (Blueprint $table) {
            $table->string('id_log',7)->primary();
            $table->string('id_user',7);
            $table->string('activity_name',30);
            $table->tinyInteger('read')->default('0');
            $table->ipAddress('ip_address');
            $table->timestamps();

            $table->foreign('id_user')->references('id_user')->on('users')->constrained()->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_logs');
    }
}
