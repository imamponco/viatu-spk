<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryDss extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_dss', function (Blueprint $table) {
            $table->string('id_dss',7)->primary();
            $table->string('id_user',7);
            $table->string('id_po',7);
            $table->text('description_dss');
            $table->timestamps();

            $table->foreign('id_user')->references('id_user')->on('users')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_po')->references('id_po')->on('purchase_orders')->constrained()->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_dss');
    }
}
