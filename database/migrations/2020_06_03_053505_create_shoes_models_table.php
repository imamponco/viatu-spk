<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShoesModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shoes_models', function (Blueprint $table) {
            $table->string('id_shoes_model',7)->primary();
            $table->string('model_name',20);
            $table->string('model_code',10)->unique();
            $table->integer('difficulty');
            $table->double('unit_price', 11, 2);
            $table->text('shoes_description');
            $table->timestamps();

        });

        Schema::create('tr_shoes_models', function (Blueprint $table) {
            $table->string('id_tr_shoes',7)->primary();
            $table->string('id_shoes_model',7)->nullable();
            $table->string('id_po',7)->nullable();
            $table->double('current_price', 11, 2);
            $table->timestamps();

            $table->foreign('id_shoes_model')->references('id_shoes_model')->on('shoes_models')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_po')->references('id_po')->on('purchase_orders')->constrained()->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::create('detail_orders', function (Blueprint $table) {
            $table->string('id_do',7)->primary();
            $table->string('id_tr_shoes',7)->nullable();
            $table->string('detail_name',20);
            $table->string('colour',20);
            $table->double('size',8,2)->nullable();
            $table->double('width',8,2)->nullable();
            $table->double('length',8,2)->nullable();
            $table->text('detail_description')->nullable();
            $table->smallInteger('quality_status')->nullable();
            $table->timestamps();

            $table->foreign('id_tr_shoes')->references('id_tr_shoes')->on('tr_shoes_models')->constrained()->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('images', function (Blueprint $table) {
            $table->string('id_images',7)->primary();
            $table->string('id_shoes_model',7)->nullable();
            $table->string('img_path');
            $table->timestamps();

            $table->foreign('id_shoes_model')->references('id_shoes_model')->on('shoes_models')->constrained()->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shoes_models');
    }
}
