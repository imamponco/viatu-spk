<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStuffTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stuff_transactions', function (Blueprint $table) {
            $table->string('id_tr',7)->primary();
            $table->string('id_user',7);
            $table->string('stuff_code',10);
            $table->integer('stuff_unit');
            $table->boolean('inout');
            $table->timestamps();

            $table->foreign('id_user')->references('id_user')->on('users')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('stuff_code')->references('stuff_code')->on('inventories')->constrained()->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stuff_transactions');
    }
}
