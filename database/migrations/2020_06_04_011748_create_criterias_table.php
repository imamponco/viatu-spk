<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Query\Expression;
use Illuminate\Support\Facades\Schema;

class CreateCriteriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('criterias', function (Blueprint $table) {
            $table->string('id_criteria',7)->primary();
            $table->string('criteria_code',10)->unique();
            $table->string('criteria_name',20);
            $table->boolean('optimization_direction')->nullable();
            $table->json('options');
            $table->double('total_value', 11, 2)->nullable();
            $table->double('eigen_value', 11, 3)->nullable();
            $table->double('total_multiple', 11, 2)->nullable();
            $table->double('result', 11, 2)->nullable();
            $table->timestamps();
        });

        Schema::create('weights', function (Blueprint $table) {
            $table->string('id_weight')->primary();
            $table->string('criteria_row',7)->nullable();
            $table->string('criteria_col',7)->nullable();
            $table->double('weight_value', 11, 2)->nullable();
            $table->timestamps();

            $table->foreign('criteria_row')->references('id_criteria')->on('criterias')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('criteria_col')->references('id_criteria')->on('criterias')->constrained()->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('tr_criterias', function (Blueprint $table) {
            $table->string('id_trc',7)->primary();
            $table->string('id_po',7)->nullable();
            $table->string('criteria_code',10);
            $table->integer('min_value');
            $table->integer('max_value');
            $table->timestamps();

            $table->foreign('criteria_code')->references('criteria_code')->on('criterias')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_po')->references('id_po')->on('purchase_orders')->constrained()->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criterias');
    }
}
