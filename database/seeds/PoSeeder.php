<?php

use Illuminate\Database\Seeder;

use App\Purchase_order;
use Carbon\Carbon;

class PoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Purchase_order::create([
            'id_po' => 'PO00001',
            'id_customer' => 'CO00001',
            'id_user' => 'U000003',
            'date_order' => Carbon::now(),
            'deadline' => Carbon::now()->addDays(60),
            'status' => 0,
            'resi_code' => '',
        ]);

        Purchase_order::create([
            'id_po' => 'PO00002',
            'id_customer' => 'CO00003',
            'id_user' => 'U000003',
            'date_order' => Carbon::now(),
            'deadline' => Carbon::now()->addDays(21),
            'status' => 0,
            'resi_code' => '',
        ]);

        Purchase_order::create([
            'id_po' => 'PO00003',
            'id_customer' => 'CO00002',
            'id_user' => 'U000003',
            'date_order' => Carbon::now(),
            'deadline' => Carbon::now()->addDays(14),
            'status' => 0,
            'resi_code' => '',
        ]);

        Purchase_order::create([
            'id_po' => 'PO00004',
            'id_customer' => 'CO00001',
            'id_user' => 'U000003',
            'date_order' => Carbon::now(),
            'deadline' => Carbon::now()->addDays(33),
            'status' => 0,
            'resi_code' => '',
        ]);

        Purchase_order::create([
            'id_po' => 'PO00005',
            'id_customer' => 'CO00003',
            'id_user' => 'U000003',
            'date_order' => Carbon::now(),
            'deadline' => Carbon::now()->addDays(2),
            'status' => 0,
            'resi_code' => '',
        ]);
    }
}
