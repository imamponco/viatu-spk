<?php

use Illuminate\Database\Seeder;

class IrSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ir  = [0.00, 0.00, 0.52, 0.89, 1.11, 1.25,1.35,1.40,1.45,1.49,1.52,1.54,1.56,1.58,1.59];
        
        for ($a = 0; $a < count($ir); $a++) {
                DB::table('index_random')->insert([
                    'idr' => 'IDR00'.($a+1),
                    'value' => $ir[$a],
                ]);
        }

    }
}
