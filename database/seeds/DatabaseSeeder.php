<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
       
        $this->call([
            UserSeeder::class,
        ]);

        $db_name = ENV('DB_DATABASE');
        $sql = "SELECT `TABLE_NAME` FROM `INFORMATION_SCHEMA`.`TABLES` WHERE `TABLE_TYPE` = 'BASE TABLE' AND TABLE_SCHEMA='".$db_name."'";
        $table_name = DB::select(DB::RAW($sql));
       
       
        foreach ($table_name as $tb ) {
            $import = new MainSeeder;

            $table = $tb->TABLE_NAME;
            //EXCLUDE SEEDING
            if($table == 'users' || $table == 'migrations' || $table == 'purchase_orders'){

            }else{
            //INCLUDE SEEDING
            DB::statement('SET FOREIGN_KEY_CHECKS = 0');
                $import->table         = $tb->TABLE_NAME;
                $import->filename      = base_path().'/database/csv/'.$tb->TABLE_NAME.'.csv';
                $import->timestamps    = true;
                $import->run();
            }

        }
        
        $this->call([
            PoSeeder::class,
            IrSeeder::class,
        ]);
        
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
