<?php
use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id_user' => 'U000001',
            'name' => 'Marullah',
            'role' => 'Manager Production',
            'phone_number' => '085777944556',
            'email' => 'viatu_mproduction@viatushop.com',
            'password' => Hash::make('qwe123')
        ]);

        User::create([
            'id_user' => 'U000002',
            'name' => 'Aswin Rasyidi',
            'role' => 'Administrator',
            'phone_number' => '085710061173',
            'email' => 'viatu_admin@viatushop.com',
            'password' => Hash::make('qwe123')
        ]);

          User::create([
            'id_user' => 'U000003',
            'name' => 'Indri Hikmah',
            'role' => 'Marketing',
            'phone_number' => '085693302128',
            'email' => 'viatu_marketing@viatushop.com',
            'password' => Hash::make('qwe123')
        ]);

           User::create([
            'id_user' => 'U000004',
            'name' => 'Alfiansyah',
            'role' => 'Warehouse',
            'phone_number' => '081384719417',
            'email' => 'viatu_warehouse@viatushop.com',
            'password' => Hash::make('qwe123')
        ]);

        // User::create([
        //     'id_user' => 'U000005',
        //     'name' => 'Fika Setya',
        //     'role' => 'Manager Marketing',
        //     'phone_number' => '082245246107',
        //     'email' => 'viatu_mmarketing@viatushop.com',
        //     'password' => Hash::make('qwe123')
        // ]);
    }
}
