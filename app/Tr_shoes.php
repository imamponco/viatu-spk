<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tr_shoes extends Model
{
    protected $table = "tr_shoes_models";
    protected $primaryKey = "id_tr_shoes";
    
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'id_tr_shoes',
        'id_shoes_model',
        'id_po',
        'current_price',
        'created_at',
        'updated_at'
    ];
}
