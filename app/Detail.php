<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $table = "detail_orders";
    protected $primaryKey = "id_do";
    
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'id_do',
        'id_tr_shoes',
        'detail_name',
        'colour',
        'size',
        'width',
        'length', 
        'quality_status',
        'detail_description',
        'created_at',
        'updated_at'
    ];
}
