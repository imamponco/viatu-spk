<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tr_Criteria extends Model
{
    //
    protected $table = "tr_criterias";
    protected $primaryKey = "id_trc";
    
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'id_trc',
        'id_po',
        'criteria_code',
        'min_value',
        'max_value',
        'created_at',
        'updated_at'
    ];
}
