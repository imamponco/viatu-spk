<?php

namespace App\Exports;

use App\HistoryDss;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithHeadings;


class HistoryExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $history_dss = HistoryDss::select('history_dss.id_dss','u.name','po.po_code','po.date_production','po.date_finish','history_dss.description_dss',DB::RAW('IF(po.status = 1, "Is Doing", "Done") as status'))
                                ->leftJoin('purchase_orders as po','history_dss.id_po','=','po.id_po')
                                ->leftJoin('users as u','history_dss.id_user','=','u.id_user')
                                // ->orderBy('history_dss.created_at','desc')
                                ->get();
        return $history_dss;
    }

    public function headings(): array
    {
        return [
            'ID DSS',
            'Decision Makers',
            'Po Code',
            'Date Production',
            'Date Finish',
            'Descriptions',
            'Status PO',
        ];
    }

}
