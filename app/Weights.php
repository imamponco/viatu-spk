<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weights extends Model
{
    //
    protected $table = "weights";
    protected $primaryKey = "id_weight";
    
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'id_weight',
        'criteria_row',
        'criteria_col',
        'weight_value',
        'created_at',
        'updated_at'
    ];
}
