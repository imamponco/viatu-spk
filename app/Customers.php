<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    //
    protected $table = "customers";
    protected $primaryKey = "id_customer";
    
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'id_customer',
        'customer_name',
        'customer_phone',
        'customer_email',
        'company', 
        'address',
        'created_at',
        'updated_at'
    ];
}
