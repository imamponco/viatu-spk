<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stuff_transactions extends Model
{
    //
    protected $table = "stuff_transactions";
    protected $primaryKey = "id_tr";
    
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'id_tr',
        'id_user',
        'stuff_code',
        'stuff_unit',
        'inout',
    ];
}
