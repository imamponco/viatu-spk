<?php

namespace App\Http\Controllers;

use Haruncpi\LaravelIdGenerator\IdGenerator;
use Maatwebsite\Excel\Facades\Excel;

use App\Activity_log;
use App\HistoryDss;
use App\User;
use App\Purchase_order;
use App\Exports\HistoryExport;

use Illuminate\Http\Request;

class ActivityLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $activity = Activity_log::select('activity_logs.id_log as id','u.name','activity_logs.activity_name','activity_logs.ip_address','activity_logs.created_at')
                                ->leftjoin('users as u','activity_logs.id_user','=','u.id_user')
                                ->orderBy('activity_logs.created_at','desc')
                                ->get();

        return view('activity.activity',['activity' => $activity]);
    }

    public function download_history(){
        return Excel::download(new HistoryExport,'history_dss-'.NOW()->format('Y-m-d').'.xlsx');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\activity_log  $activity_log
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity_log $activity_log,$id)
    {
        $data = Activity_log::where('id_log',$id)->get()->first();

        if($data){

            $data->delete();

             // redirect
            return response()->json(['success'=>'Data has been deleted']);
        }else{
            return  response()->json(['failed'=>'Failed to deleted data','data'=> $data],422);
        }
    }

    public function destroy_dss(Request $request,$id)
    {
        $data = HistoryDss::where('id_dss',$id)->first();

        if($data){
        
            // $po = Purchase_order::where('id_po',$data->id_po)->update(['status'=>0]);

            // if($po){

                $log = [
                    'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                    'id_user'       => session('id_user'),
                    'activity_name' => 'Cancel decision for po '.$data->id_po,
                    'ip_address'    => $request->ip()
                ];
    
                Activity_log::create($log);
                
                $data->delete();
                    // redirect
                return response()->json(['success'=>'Data has been deleted']);

            // }else{
            //     return  response()->json(['failed'=>'Failed to deleted data','data'=> $data],422);
            
            // }
        }else{
            return  response()->json(['failed'=>'Failed to deleted data','data'=> $id],422);
        }
    }

    public function destroy_all(Activity_log $activity_log)
    {
        $data = Activity_log::all();

        if($data){

            $data = Activity_log::truncate();

             // redirect
            return response()->json(['success'=>'All activity has been deleted']);
        }else{
            return  response()->json(['failed'=>'Failed to deleted all activity','data'=> $data],422);
        }
    }

    

    public function read_notification(Request $request){

        $data = [
            'read' => 1,
        ];

        $read =  Activity_log::where('activity_name','like','%'.$request->activity_name.'%')->where('read',0)->update($data);

        if($read){
             // redirect
            return response()->json(['success'=>'Notification readed']);
        }else{
            return  response()->json(['failed'=>'Failed to read notification','data'=> $read],422);
        }
    }
}
