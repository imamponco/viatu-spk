<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Haruncpi\LaravelIdGenerator\IdGenerator;

use App\Inventory;
use App\Activity_log;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $inventory = DB::table('inventories as i')
                        ->select('i.stuff_name','i.stuff_code','m.total','i.id_inv','i.stuff_description')
                        ->leftJoin(DB::raw('(SELECT id_tr,stuff_code,SUM(stuff_unit) as total FROM stuff_transactions GROUP BY stuff_code) as m'),'i.stuff_code','=','m.stuff_code')
                        ->orderBy('created_at','desc')
                        ->groupBy('i.stuff_code')
                        ->get();
      
        return view('inventory.inventory',['inventory'=>$inventory]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'stuff_name'        => 'required|regex:/^[a-zA-Z ]+$/|unique:inventories|min:4|max:30:',
            'stuff_code'        => 'required|min:4|max:10|unique:inventories',
            'stuff_description' => 'required'

        ];

        $validator = Validator::make($request->all(),$rules );

        if ($validator->fails()) {
            return  response()->json(['success'=>'Failed to add data','data'=> $request->all(),'errors'=>$validator->errors()],422);
        }
        
        $data = [
            'id_inv'            => IdGenerator::generate(['table' => 'inventories','field'=>'id_inv', 'length' => 7, 'prefix' =>'IN','reset_on_prefix_change'=>'true']),
            'id_user'           => session('id_user'),
            'stuff_name'        => $request->stuff_name,
            'stuff_code'        => $request->stuff_code,
            'stuff_description' => $request->stuff_description,
        ];
       
        $data = Inventory::create($data);
      
        if($data){

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Add new stuff.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return response()->json(['success'=>'Data is successfully added','data'=> $data]);
        }else{
            return  response()->json(['success'=>'Failed to add data','data'=> $request->all()],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $inventory = Inventory::where('id_inv',$id)->get()->first();
       
        if($inventory){
            return response()->json(['success'=>'Data is successfully get','all_data'=> $inventory]);
        }else{
            return  response()->json(['success'=>'Failed to get data','all_data'=> $inventory],422);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function edit(inventory $inventory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $rules = [
            'stuff_name'        => ['required','regex:/^[a-zA-Z ]+$/',Rule::unique('inventories')->ignore($id,'id_inv'),'min:4','max:30:'],
            'stuff_code'        => ['required','min:4','max:10',Rule::unique('inventories')->ignore($id,'id_inv')],
            'stuff_description' => 'required'

        ];

        $validator = Validator::make($request->all(),$rules );

        if ($validator->fails()) {
            return  response()->json(['success'=>'Failed to update data','data'=> $request->all(),'errors'=>$validator->errors()],422);
        }
        
        $data = [
            'id_user'           => session('id_user'),
            'stuff_name'        => $request->stuff_name,
            'stuff_code'        => $request->stuff_code,
            'stuff_description' => $request->stuff_description,
        ];
       
        $data = Inventory::where('id_inv',$id)->update($data);
      
        if($data){
            
            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Update the stuff.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return response()->json(['success'=>'Data is successfully updated','data'=> $data]);
        }else{
            return  response()->json(['success'=>'Failed to update data','data'=> $request->all()],422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $data = Inventory::where('id_inv',$id)->get()->first();

        if($data){

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Delete the stuff.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            $data->delete();

             // redirect
            return response()->json(['success'=>'Data has been deleted']);
        }else{
            return  response()->json(['failed'=>'Failed to deleted data','data'=> $data],422);
        }
    }

    

}
