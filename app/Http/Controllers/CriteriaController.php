<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Haruncpi\LaravelIdGenerator\IdGenerator;

use App\Criteria;
use App\Weights;
use App\Activity_log;
use App\Tr_Criteria;

class CriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $criterias = Criteria::orderBy('created_at','desc')->get();

        return view('criteria.criteria',['criterias' => $criterias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'criteria_name'             =>  'required|regex:/^[a-zA-Z ]+$/|min:5|max:20|unique:criterias',
            'criteria_code'             =>  'required|regex:/^\S*$/|min:4|max:10|unique:criterias',
            'opt'                       =>  'required',

        ];

        if(!empty($request->options['data_option'])){
            $rules += [
                'options.data_option.*.name' => 'required|regex:/^[a-zA-Z ]+$/',
                'options.data_option.*.value' => 'required|regex:/^(?=.*?[1-9])[0-9-]+$/'
            ];
        }else{
            $rules += [
                'options'         =>  'required',
            ];
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return  response()->json(['success'=>'Failed to add data','data'=> $request->all(),'errors'=>$validator->errors()],422);
        }


        $data = [
                'id_criteria'               => IdGenerator::generate(['table' => 'criterias','field'=>'id_criteria', 'length' => 7, 'prefix' =>'C','reset_on_prefix_change'=>'true']),
                'criteria_name'             => $request->criteria_name,
                'criteria_code'             => $request->criteria_code,
                'optimization_direction'    => $request->opt,
                'options'                   => json_encode([$request->options])
            ];

        $check = count(Criteria::get());

        if($check == 15){
            return  response()->json(['success'=>'Tidak dapat menambah kriteria lagi','data'=> $request->all()],200);
        }else{

            $data = Criteria::create($data);

            $criterias = Criteria::get();

            if($data){

                $log = [
                    'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                    'id_user'       => session('id_user'),
                    'activity_name' => 'Add new criteria.',
                    'ip_address'    => $request->ip()
                ];

                Activity_log::create($log);

                //clear criteria
                $clear_crit =   [
                                'total_value'             => null,
                                'eigen_value'             => null,
                                'total_multiple'          => null,
                                'result'                  => null
                            ];

                foreach ($criterias as $c) {
                   Criteria::where('id_criteria',$c->id_criteria)->update($clear_crit);
                }

                $clear = Weights::truncate();

                if($clear){
                    //create weight data to db
                    foreach ($criterias as $row ) {
                        foreach ($criterias as $col ) {
                            if($row->id_criteria == $col->id_criteria){
                                $weight_value = 1.0;
                            }else{
                                $weight_value = 0;
                            }
                            $weight = [
                                'id_weight'    => IdGenerator::generate(['table' => 'weights','field'=>'id_weight', 'length' => 7, 'prefix' =>'W','reset_on_prefix_change'=>'true']),
                                'criteria_row' => $row->id_criteria,
                                'criteria_col' => $col->id_criteria,
                                'weight_value' => $weight_value
                            ];
                            //insert to weight db
                            $insert = Weights::create($weight);
                        }
                    }
                }

                return response()->json(['success'=>'Data is successfully added','data'=> $request->all(),'all_data'=>$criterias]);
            }else{
                return  response()->json(['success'=>'Failed to add data','data'=> $request->all()],422);
            }
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\criteria  $criteria
     * @return \Illuminate\Http\Response
     */
    public function show(criteria $criteria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\criteria  $criteria
     * @return \Illuminate\Http\Response
     */
    public function edit(criteria $criteria,$id)
    {
        //
        $criterias = Criteria::where('id_criteria',$id)->get()->first();

         if($criterias){
            // $criterias->options = json_encode($criterias->options);
            return response()->json(['success'=>'Data is successfully get','data'=> $criterias]);
        }else{
            return  response()->json(['success'=>'Failed to get data'],422);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\criteria  $criteria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, criteria $criteria,$id)
    {
        //
        $rules = [
            'criteria_name'             => [
                                            'required',
                                            'regex:/^[a-zA-Z ]+$/',
                                            'min:5',
                                            'max:20',
                                            Rule::unique('criterias')->ignore($id,'id_criteria')
                                        ],
            'criteria_code'            =>  [
                                            'required',
                                            'min:4',
                                            'max:10',
                                            'regex:/^\S*$/',
                                             Rule::unique('criterias')->ignore($id,'id_criteria')
                                        ],
            'opt'                       =>  'required',

        ];

        if(!empty($request->options['data_option'])){
            $rules += [
                'options.data_option.*.name' => 'required|regex:/^[a-zA-Z ]+$/',
                'options.data_option.*.value' => 'required|regex:/^(?=.*?[1-9])[0-9-]+$/'
            ];
        }else{
            $rules += [
                'options'         =>  'required',
            ];
        }

        $validator = Validator::make($request->all(), $rules);


        if ($validator->fails()) {
         return  response()->json(['success'=>'Failed to add data','data'=> $request->all(),'errors'=>$validator->errors()],422);
        }

        $data = [
                'criteria_name'             => $request->criteria_name,
                'criteria_code'             => $request->criteria_code,
                'optimization_direction'    => $request->opt,
                'options'                   => json_encode(json_encode([$request->options]))
            ];

        $criteria_code_old = Criteria::select("criteria_code")->where('id_criteria',$id)->first()->criteria_code;

        $data = Criteria::where('id_criteria',$id)->update($data);

        $criterias = Criteria::get();

        if($data){

           //clear tr
           $tr = Tr_Criteria::where('criteria_code',$criteria_code_old)->delete();

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Update new criteria.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            //clear criteria
            $clear_crit =   [
                                'total_value'             => null,
                                'eigen_value'             => null,
                                'total_multiple'          => null,
                                'result'                  => null
                            ];

            foreach ($criterias as $c) {
                   Criteria::where('id_criteria',$c->id_criteria)->update($clear_crit);
            }

            $clear = Weights::truncate();


            if($clear){
                //create weight data to db
                foreach ($criterias as $row ) {
                    foreach ($criterias as $col ) {
                        if($row->id_criteria == $col->id_criteria){
                            $weight_value = 1.0;
                        }else{
                            $weight_value = 0;
                        }
                        $weight = [
                            'id_weight'    => IdGenerator::generate(['table' => 'weights','field'=>'id_weight', 'length' => 7, 'prefix' =>'W','reset_on_prefix_change'=>'true']),
                            'criteria_row' => $row->id_criteria,
                            'criteria_col' => $col->id_criteria,
                            'weight_value' => $weight_value
                        ];
                        //insert to weight db
                        $insert = Weights::create($weight);
                    }
                }
            }


            $criterias = json_encode($criterias);

            return response()->json(['success'=>'Data is successfully updated','data'=> $request->all(),'all_data'=>$criterias]);
        }else{
            return  response()->json(['success'=>'Failed to update data','data'=> $request->all()],422);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\criteria  $criteria
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        $criterias = Criteria::where('id_criteria',$id)->get()->first();

        if($criterias){
            //clear tr
            $tr = Tr_Criteria::where('criteria_code',$criterias->criteria_code)->delete();

            $criterias->delete();

            $clear = Weights::truncate();

            $criterias = Criteria::get();

            //clear criteria
            $clear_crit =   [
                                'total_value'             => null,
                                'eigen_value'             => null,
                                'total_multiple'          => null,
                                'result'                  => null
                            ];

            foreach ($criterias as $c) {
                   Criteria::where('id_criteria',$c->id_criteria)->update($clear_crit);
            }

            if($clear){
                //create weight data to db
                foreach ($criterias as $row ) {
                    foreach ($criterias as $col ) {
                        if($row->id_criteria == $col->id_criteria){
                            $weight_value = 1.0;
                        }else{
                            $weight_value = 0;
                        }
                        $weight = [
                            'id_weight'    => IdGenerator::generate(['table' => 'weights','field'=>'id_weight', 'length' => 7, 'prefix' =>'W','reset_on_prefix_change'=>'true']),
                            'criteria_row' => $row->id_criteria,
                            'criteria_col' => $col->id_criteria,
                            'weight_value' => $weight_value
                        ];
                        //insert to weight db
                        $insert = Weights::create($weight);
                    }
                }
            }

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Delete criteria.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            // redirect
            return redirect('criteria')->with('alert-success','Successfully deleted the criterias!');
        }else{
            return redirect('criteria')->with('alert-success','Data has been deleted!');
        }
    }


    // =================================== WEIGHT ===========================================

     public function index_weight()
    {
        //
        $criterias = Criteria::orderBy('id_criteria','asc')->get();

        if (count($criterias) < 2) {
           return response()->json(['success'=>'Tidak dapat membuka halaman ini, kriteria tidak mencukupi untuk dilakukan perhitungan.'],422);
        }else{
           return response()->json(['success'=>'Success.'],200);
        }

    }

    public function reload(){

        $criterias = Criteria::get();

        $weights = Weights::get();


        //get matrix weight by row
        $array1 = Weights::select('weight_value','criteria_row','criteria_col')->orderBy('id_weight','asc')->get()->toArray();

        //get matrix weight by col
        $array2 = Weights::select('weight_value')->orderBy('criteria_col','asc')->get()->toArray();

        $sigma_total = 0;
        $sum = 0;
        $count = 1;

        //limit
        $limit = count($criterias);

        //value_multiple
        $value_multiple = 0;

        //get total value from db
        for ($i=1; $i<=$limit; $i++) {
            $qry = Weights::select(DB::raw('SUM(weight_value) as total'))
                            ->where('criteria_col',$i)
                            ->get()
                            ->toArray();

            $sigma_total = $sigma_total + $qry[0]['total'];
        }

        //sqrt matrix by ordo
        $matrix = $this->sqrt_matrix($array1,$array2,$limit);

        $total = array_sum($matrix);
        $total = number_format($total, 2, '.', '');

        //get eigen vector and sum
        for ($a=0; $a<count($matrix); $a++) {
            $sum += $matrix[$a];
            $check = $a % $limit;
            if($check == ($limit-1) && $a != 0){
                $sum = number_format($sum, 2, '.', '');
                $eigen_value = $sum / $total ;
                $eigen_value = number_format($eigen_value, 3, '.', '');

                $data = [
                    'total_value'      => $sum,
                    'eigen_value'      => $eigen_value
                ];

                $data = Criteria::where('id_criteria',$criterias[$count-1]->id_criteria)->update($data);

                $sum = 0;

                $count++;
            }
        }

        $hitung  = 1;
        $hitung2 = 0;
        $result  = 0;

        $criterias = Criteria::get();
        //get total multiple
        for ($r=0; $r < count($weights); $r++) {

            $value_multiple += ($weights[$r]->weight_value * $criterias[$hitung-1]->eigen_value);

            $check = ($r+1) % $limit;

            if($check == 0 && ($r+1) != 1){

                $value_multiple = $value_multiple;

                $result = $value_multiple + ($criterias[$hitung2]->eigen_value);

                $data = [
                    'result'              => $result,
                    'total_multiple'      => $value_multiple
                ];

                $data = Criteria::where('id_criteria',$criterias[$hitung2]->id_criteria)->update($data);

                $value_multiple = 0;
                $result = 0;

                if($hitung==$limit){
                    $hitung=0;
                }
                $hitung2++;
            }
            $hitung++;

        }
        $criterias = Criteria::get();

        $weight_value = Weights::select(DB::RAW("COUNT(id_weight) AS count"))->where('weight_value',0)->get();


        return view('weights.weights',['criterias'=>$criterias,'weights'=> $weights,'matrix'=> $matrix,'sum' => $sum,'sigma_total'=>$sigma_total,'weight_value'=>$weight_value]);
    }

    public function update_weight(Request $request,$id){
            //
            $validator = Validator::make($request->all(), [
                'weight_value'             => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/|min:1|max:5|lte:9|gt:0'
            ]);


            if ($validator->fails()) {
            return  response()->json(['success'=>'Failed to add data','data'=> $request->all(),'errors'=>$validator->errors()],422);
            }

            $data1 = [
                    'weight_value'      => $request->weight_value
                ];

            $data2 = [
                     'weight_value'     => 1/$request->weight_value
                ];

            $data  = Weights::where('id_weight',$id)->update($data1);
            $data2 = Weights::where('criteria_row',$request->criteria_col)
                            ->where('criteria_col',$request->criteria_row)
                            ->update($data2);

            $weights = Weights::get();

            if($data){
                return response()->json(['success'=>'Data is successfully updated','data'=> $request->all(),'all_data'=>$weights]);
            }else{
                return  response()->json(['success'=>'Failed to update data','data'=> $request->all()],422);
            }

        }

        public function sqrt_matrix($array1,$array2,$ordo){
            //COUNT TOTAL CRITERIA
            $criteria = Criteria::get();
            //COUNT TOTAL ELEMENT
            $weights = Weights::get();
            //GETTING ORDO OF MATRIX
            $limit = count($weights);
            //ALL FLAG AND TEMP VARIABLE
            $sum = 0;
            $flag = 0;
            $flag2 = 0;
            $check = 0;



            //INITIALIZATION ARRAY SUM
            for($i=0; $i<count($criteria); $i++){
                $arraysum[$array1[$i]['criteria_col']] = 0;
            }

            //INITIALIZATION ARRAY MATRIX
            for ($i=0; $i < $limit; $i++) {
                $tampung[$i] = 0;
            }

            for ($a=0; $a<$limit; $a++) {
                $arraysum[$array1[$a]['criteria_col']] += $array1[$a]['weight_value'];
            }

            for ($i=0; $i < $limit; $i++){
                $tampung[$i] = number_format(($array1[$i]['weight_value'] / $arraysum[($array1[$i]['criteria_col'])]), 3, '.', '');
            }

            //SQRT MARTRIX BY ORDO
            // for ($a=0; $a < $limit; $a++) {
            //     echo "a:".$a." ";
            //     $check++;
            //         for ($i=0; $i < $ordo; $i++) {
            //             // var_dump("flag :".$flag);
            //             echo $a.". ";
            //             if($i == ($ordo-1)){

            //                 echo "(".$array1[$flag2+$i]['weight_value']." * ".$array2[$flag+$i]['weight_value'].")";
            //                 $sum += ($array1[$flag2+$i]['weight_value']*$array2[$flag+$i]['weight_value']);
            //                 $tampung[$a] = $sum;
            //                 echo " = ".$sum;
            //                 echo ($flag2+$i)."".($flag+$i);
            //                 echo "<br>";
            //                 $sum = 0;
            //             }else{
            //                 echo "(".$array1[$flag2+$i]['weight_value']." * ".$array2[$flag+$i]['weight_value'].")";
            //                 echo ($flag2+$i)."".($flag+$i);
            //                 echo "+";
            //                 $sum += ($array1[$flag2+$i]['weight_value']*$array2[$flag+$i]['weight_value']);
            //             }

            //         }
            //     if($check == $ordo && $a != 0){
            //         $flag = 0;
            //         $check = 0;
            //         $flag2 += $ordo;

            //     }else{
            //         $flag += $ordo;
            //     }

            // }

            //RETURN THE RESULT
            return $tampung;
        }


    }

