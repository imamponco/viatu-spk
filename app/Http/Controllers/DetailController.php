<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Haruncpi\LaravelIdGenerator\IdGenerator;

use App\Activity_log;
use App\Detail;
use App\Tr_shoes;

class DetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $shoes_model = Tr_shoes::select('*','tr_shoes_models.id_tr_shoes as id_tr')
                                ->leftJoin('shoes_models','tr_shoes_models.id_shoes_model','=','shoes_models.id_shoes_model')
                                ->where('tr_shoes_models.id_tr_shoes',$id)
                                ->orderBy('tr_shoes_models.created_at','desc')
                                ->get();

        $detail = Detail::select("*","detail_orders.detail_description as description","detail_orders.id_do as id")
                          ->leftJoin('tr_shoes_models as t', 'detail_orders.id_tr_shoes', '=', 't.id_tr_shoes')
                          ->leftJoin('shoes_models', 't.id_shoes_model', '=', 'shoes_models.id_shoes_model')
                          ->where('detail_orders.id_tr_shoes',$id)
                          ->orderBy('detail_orders.created_at','desc')
                          ->get();

        return view('detail.detail',['detail' => $detail,'shoes_model' => $shoes_model]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        //
        if($request->type_form =="common"){
            
            $rules = [
                'total'      => 'required|numeric',
                'colour'    => 'required|regex:/^[a-zA-Z ]+$/|min:3|max:15',
            ];

            if($request->total != null){
                $request->name = "SHOES NAME";
                $request->description = "SHOES DESC";
            }

            if($request->size == null AND $request->width == null AND $request->length == null){
                $rules += [
                    'size'      => 'required|numeric',
                    'width' => 'required|numeric|min:1',
                    'length' => 'required|numeric|min:1'
                ];
            }
            
            if($request->width != null){
                $rules += [
                    'width' => 'required|numeric|min:1',
                    'length' => 'required|numeric|min:1'
                ];
            }
            
            if($request->length != null){
                $rules += [
                    'width' => 'required|numeric|min:1',
                    'length' => 'required|numeric|min:1'
                ];
            }
            
            if($request->size != null){
                $rules += [
                    'size'      => 'required|numeric',
                ];
            }

        }else{
            
            $rules = [
                'name'      => 'required|regex:/^[a-zA-Z ]+$/|min:4|max:20:',
                'colour'    => 'required|regex:/^[a-zA-Z ]+$/|min:3|max:15',
            ];

            if($request->size == null AND $request->width == null AND $request->length == null){
                $rules += [
                    'size'      => 'required|numeric',
                    'width' => 'required|numeric|min:1',
                    'length' => 'required|numeric|min:1'
                ];
            }

            if($request->width != null){
                $rules += [
                    'width' => 'required|numeric|min:1',
                    'length' => 'required|numeric|min:1'
                ];
            }

            if($request->length != null){
                $rules += [
                    'width' => 'required|numeric|min:1',
                    'length' => 'required|numeric|min:1'
                ];
            }
            
            if($request->size != null){
                $rules += [
                    'size'      => 'required|numeric',
                ];
            }

        }

        $validator = Validator::make($request->all(),$rules );

        if ($validator->fails()) {
            return  response()->json(['success'=>'Failed to add data','data'=> $request->all(),'errors'=>$validator->errors()],422);
        }
        
       

        if($request->type_form=="common"){
            for ($i=0; $i < $request->total; $i++) { 
                $data = [
                    'id_do'                 => IdGenerator::generate(['table' => 'detail_orders','field'=>'id_do', 'length' => 7, 'prefix' =>'DO','reset_on_prefix_change'=>'true']),
                    'id_tr_shoes'           => $id,
                    'detail_name'           => $request->name,
                    'colour'                => $request->colour,
                    'size'                  => $request->size,
                    'width'                 => $request->width,
                    'length'                => $request->length,
                    'detail_description'    => $request->description,
                    'quality_status'        => 0
                ];

                $data = Detail::create($data);
            }
           
        }else{
            
            $data = [
                'id_do'                 => IdGenerator::generate(['table' => 'detail_orders','field'=>'id_do', 'length' => 7, 'prefix' =>'DO','reset_on_prefix_change'=>'true']),
                'id_tr_shoes'           => $id,
                'detail_name'           => $request->name,
                'colour'                => $request->colour,
                'size'                  => $request->size,
                'width'                 => $request->width,
                'length'                => $request->length,
                'detail_description'    => $request->description,
                'quality_status'        => 0
            ];

            $data = Detail::create($data);
        }
      
        if($data){
            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Add new detail order.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return response()->json(['success'=>'Data is successfully added','data'=> $data]);
        }else{
            return  response()->json(['success'=>'Failed to add data','data'=> $request->all()],422);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $detail = Detail::where('id_do',$id)->get()->first();
       
        if($detail){
            return response()->json(['success'=>'Data is successfully updated','all_data'=> $detail]);
        }else{
            return  response()->json(['success'=>'Failed to update data','all_data'=> $detail],422);
        }
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $rules = [
            'name'      => 'required|regex:/^[a-zA-Z ]+$/|min:4|max:20:',
            'colour'      => 'required|regex:/^[a-zA-Z ]+$/|min:3|max:15',
        ];

        if($request->size == null AND $request->width == null AND $request->length == null){
            $rules += [
                'width' => 'required|numeric|min:1',
                'length' => 'required|numeric|min:1'
            ];
        }
        if($request->width != null){
            $rules += [
                'width' => 'required|numeric|min:1',
                'length' => 'required|numeric|min:1'
            ];
        }
        
        if($request->length != null){
            $rules += [
                'width' => 'required|numeric|min:1',
                'length' => 'required|numeric|min:1'
            ];
        }

        if($request->size != null){
            $rules += [
                'size'      => 'required|numeric',
            ];
        }
        
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return  response()->json(['success'=>'Failed to add data','data'=> $request->all(),'errors'=>$validator->errors()],422);
        }
        
        $data = [
            'detail_name'              => $request->name,
            'colour'            => $request->colour,
            'size'              => $request->size,
            'width'             => $request->width,
            'length'            => $request->length,
            'detail_description'       => $request->description
        ];

        $data = Detail::where('id_do',$id)->update($data);
    
        if($data){
            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Update detail order.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return response()->json(['success'=>'Data is successfully updated','data'=> $data]);
        }else{
            return  response()->json(['success'=>'Failed to update data','data'=> $request->all()],422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        $data = Detail::where('id_do',$id)->get()->first();

        if($data){
            
            $data->delete();

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Delete detail order.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

             // redirect
            return response()->json(['success'=>'Data has been deleted']);
        }else{
            return  response()->json(['failed'=>'Failed to deleted data','data'=> $data],422);
        }
    }

    public function destroy_detail(Request $request,$id){
       
        $data = Detail::where('id_tr_shoes',$id);
       
        if(count($data->get())>0){

            $data->delete();

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Delete detail order.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

             // redirect
            return response()->json(['success'=>'Data has been deleted']);
        }else{
            return  response()->json(['failed'=>'Failed to deleted data','data'=> $data],422);
        }
    }
}
