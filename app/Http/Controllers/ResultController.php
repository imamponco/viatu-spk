<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Haruncpi\LaravelIdGenerator\IdGenerator;

use App\Tr_Criteria;
use App\Criteria;
use App\Purchase_order;
use App\VwConsistency;
use App\HistoryDss;
use App\Weights;

class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $result = DB::table('tr_criterias as t')
                      ->select('t.id_trc','t.id_po','t.criteria_code','t.min_value','t.max_value','c.eigen_value','c.optimization_direction')
                      ->leftJoin('criterias as c','t.criteria_code','=','c.criteria_code')
                      ->leftJoin('purchase_orders as p','t.id_po','=','p.id_po')
                    //   ->where('p.status_verification','1')
                      ->orderBy('c.id_criteria','asc')
                      ->where('p.status','0')
                      ->get();

        $criterias = Criteria::orderBy('created_at','asc')->get();

        $norm = DB::table('tr_criterias as t')
                    ->select('c.id_criteria','t.criteria_code','c.eigen_value','c.optimization_direction',DB::raw('SUM(t.min_value)+SUM(t.max_value) AS amount'))
                    ->leftJoin('criterias as c','t.criteria_code','=','c.criteria_code')
                    ->groupBy('t.criteria_code')
                    ->orderBy('c.id_criteria','asc')
                    ->get();

        $po = Purchase_order::select('id_po')->where('status',0)
                            // ->where('status_verification',1)
                            ->get();

        $check_tr = Tr_Criteria::select('p.id_po as id_po','tr_criterias.id_trc as id','c.criteria_code','c.options',
                                'tr_criterias.min_value','tr_criterias.max_value','p.status')
                                ->leftJoin('purchase_orders as p','tr_criterias.id_po','=','p.id_po')
                                ->leftJoin('criterias as c','tr_criterias.criteria_code','=','c.criteria_code')
                                ->orderBy('tr_criterias.id_trc','asc')
                                ->where('status',0)
                                // ->where('status_verification',1)
                                ->where('min_value',0)
                                ->where('max_value',0)
                                ->get();

        $current_criteria = Tr_Criteria::select('criteria_code')
                                        ->groupBy('criteria_code')
                                        ->get();

        $consistency = VwConsistency::get();

        $history_dss = HistoryDss::select('history_dss.id_dss','history_dss.id_user','po.date_finish','history_dss.id_po','po.date_production','history_dss.description_dss','po.status','u.name','history_dss.created_at')
                                   ->leftJoin('purchase_orders as po','history_dss.id_po','=','po.id_po')
                                   ->leftJoin('users as u','history_dss.id_user','=','u.id_user')
                                //    ->orderBy('history_dss.created_at','desc')
                                   ->get();

        $weight_value = Weights::select(DB::RAW("COUNT(id_weight) AS count"))->where('weight_value',0)->get();
     
        if (!empty($consistency[0]->consistency) && empty($weight_value[0]->count)) {
            if($consistency[0]->consistency == "REJECTED"){
                return response()->json(['success'=>'Tidak dapat membuka halaman ini, karena nilai konsistensi tidak dapat diterima.'],422);
            }
        }else{
            return response()->json(['success'=>'Tidak dapat membuka halaman ini, karena nilai konsistensi tidak dapat diterima.'],422);
         }

        // if(count($criterias)!=count($current_criteria)){
        //     return response()->json(['success'=>'Tidak dapat membuka halaman ini, penilaian alternatif tidak mencukupi untuk dilakukan perhitungan.'],422);
        // }

        // if(count($check_tr)>0){
        //     return response()->json(['success'=>'Tidak dapat membuka halaman ini, penilaian alternatif tidak dapat bernilai kosong.'],422);
        // }

        // if (count($result) < 1) {
        //     return response()->json(['success'=>'Tidak dapat membuka halaman ini, penilaian alternatif tidak mencukupi untuk dilakukan perhitungan.'],422);
        //  }else{
            return view('result.result',['result'=>$result,'criterias'=>$criterias,'po'=>$po,'norm'=>$norm,'history'=>$history_dss]);
        //  }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

}
