<?php

namespace App\Http\Controllers;

use App\Shoes_model;
use App\Image_file;
use App\Tr_shoes;
use App\Activity_log;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Haruncpi\LaravelIdGenerator\IdGenerator;

use Image;

class ShoesModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $shoes_model = Shoes_model::orderBy('created_at','desc')->get();
        $images = Image_file::get()->whereNull('id_shoes_model');

        return view('shoes_model.shoes_model',['shoes_model' => $shoes_model,'images'=> $images]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'model_name'      => 'required|regex:/^[a-zA-Z ]+$/|min:4|max:20|unique:shoes_models',
            'model_code'      => 'required|min:5|max:10|unique:shoes_models',
            'unit_price'      => 'required|min:1|numeric',
            'difficulty'      => 'required',
            'description'     => 'required'
        ]);

        if ($validator->fails()) {
            return  response()->json(['success'=>'Failed to add data','data'=> $request->all(),'errors'=>$validator->errors()],422);
        }

        $data = [
            'id_shoes_model'    => IdGenerator::generate(['table' => 'shoes_models','field'=>'id_shoes_model', 'length' => 7, 'prefix' =>'M','reset_on_prefix_change'=>'true']),
            'model_name'        => $request->model_name,
            'model_code'        => $request->model_code,
            'unit_price'        => $request->unit_price,
            'difficulty'        => $request->difficulty,
            'shoes_description' => $request->description
        ];

        $data = Shoes_model::create($data);

        if($data){

            $image = [
                'id_shoes_model'        => $data->id_shoes_model,
            ];

            $image = Image_file::whereNull('id_shoes_model')->update($image);

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Add new shoes model.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            if($image){
                return response()->json(['success'=>'Data is successfully added','data'=> $data,'current_img'=>$image]);
            }else{
                return  response()->json(['success'=>'Data is successfully added','data'=> $data]);
            }

        }else{
            return  response()->json(['success'=>'Failed to add data','data'=> $request->all()],422);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shoes_model  $shoes_model
     * @return \Illuminate\Http\Response
     */
    public function show(Shoes_model $shoes_model)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shoes_model  $shoes_model
     * @return \Illuminate\Http\Response
     */
    public function edit(Shoes_model $shoes_model,$id)
    {
        //

        $current = Shoes_model::where('id_shoes_model',$id)->get()->first();

        $current_img = Image_file::where('id_shoes_model',$current->id_shoes_model)->get();

        if($current){
            return response()->json(['success'=>'Data success to get','all_data'=> $current,'current_img'=>$current_img]);

        }else{
            return  response()->json(['failed'=>'Failed to get data','id'=> $id],422);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shoes_model  $shoes_model
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shoes_model $shoes_model,$id)
    {
        //
        $validator = Validator::make($request->all(), [
            'model_name'      => [
                'required',
                'regex:/^[a-zA-Z ]+$/',
                'min:4',
                'max:20',
                Rule::unique('shoes_models')->ignore($id,'id_shoes_model')
            ],
            'model_code'      => [
                'required',
                'min:5',
                'max:10',
                Rule::unique('shoes_models')->ignore($id,'id_shoes_model')
            ],
            'unit_price'      => 'required|min:1|numeric',
            'description'     => 'required',
            'difficulty'      => 'required'
        ]);

        if ($validator->fails()) {
            return  response()->json(['success'=>'Failed to add data','data'=> $request->all(),'errors'=>$validator->errors()],422);
        }

        $data = [
            'model_name'            => $request->model_name,
            'model_code'            => $request->model_code,
            'unit_price'            => $request->unit_price,
            'difficulty'            => $request->difficulty,
            'shoes_description'     => $request->description
        ];

        $data = Shoes_model::where('id_shoes_model',$id)->update($data);

        $data = Shoes_model::where('id_shoes_model',$id)->get();

        if($data){

            $image = Image_file::where('id_shoes_model',$id)->get();


            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Update the shoes model.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            if($image){
                return response()->json(['success'=>'Data is successfully updated','data'=> $data,'current_img'=>$image]);
            }else{
                return  response()->json(['success'=>'Data is successfully updated','data'=> $data]);
            }

        }else{
            return  response()->json(['success'=>'Failed to update data','data'=> $request->all()],422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shoes_model  $shoes_model
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shoes_model $shoes_model,Request $request,$id)
    {
         //
         $shoes = Shoes_model::where('id_shoes_model',$id)->get()->first();

         if($shoes){
             $image = Image_file::where('id_shoes_model',$id)->get();

             if($image){

                foreach($image as $img):

                    if(file_exists($img->img_path))
                    {
                        $success = (unlink($img->$img_path));
                        $image = Image_file::where('id_images',$img->id_images)->delete();
                    }else{
                        $success = false;
                    }

                endforeach;
             }

             $shoes->delete();


            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Delete the shoes model.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

             $shoes = Shoes_model::get();

                // redirect
                return response()->json(['success'=>'Data has been deleted','all_data'=> $shoes]);

            }else{
                return  response()->json(['failed'=>'Failed to deleted data','data'=> $shoes],422);
            }
    }

    public function get_all_shoes($id){

        if($id != 'null'){
            $current_model  = Tr_shoes::select('id_shoes_model')->where('id_po',$id)->get();
        }else{
            $current_model  = Tr_shoes::select('id_shoes_model')->whereNull('id_po')->get();
        }

        $shoes_model = Shoes_model::whereNotIn('id_shoes_model',$current_model)->get();

        if($shoes_model){
            return response()->json(['success'=>'Data is successfully added','all_data'=> $shoes_model]);
        }else{
            return  response()->json(['failed'=>'Data is successfully added','all_data'=> $data]);
        }
    }

    public function store_tr_shoes(Request $request){

        $validator = Validator::make($request->all(), [
            'id'   => 'required'
        ]);

        if ($validator->fails()) {

            $message = [
                    "id" => [
                        0 => "The shoes model is required"
                    ]
            ] ;

            return  response()->json(['success'=>'Failed to add data','errors'=>$message],422);
        }


        $current_price = Shoes_model::select('unit_price')
                                      ->where('id_shoes_model',$request->id)
                                      ->get()
                                      ->toArray();
        $current_price = $current_price[0];

        $data = [
            'id_tr_shoes'    => IdGenerator::generate(['table' => 'tr_shoes_models','field'=>'id_tr_shoes', 'length' => 7, 'prefix' =>'ITR','reset_on_prefix_change'=>'true']),
            'id_shoes_model' => $request->id,
            'current_price'  => $current_price['unit_price']
        ];

        if(!empty($request->id_po)){
            $data += [
              'id_po' => $request->id_po
            ];
        }

        $data = Tr_shoes::create($data);

        if($data){
            return response()->json(['success'=>'Data is successfully added','data'=> $data]);
        }else{
            return  response()->json(['success'=>'Failed to add data','data'=> $data],422);
        }
    }

    public function destroy_tr_shoes(Request $request,$id){

        $data = Tr_shoes::where('id_tr_shoes',$id)->get()->first();

        if($data){

            $data->delete();

            $data = Tr_shoes::whereNull('id_po')->get();
             // redirect
            return response()->json(['success'=>'Data has been deleted','all_data'=> $data]);
        }else{
            return  response()->json(['failed'=>'Failed to deleted data','data'=> $data],422);
        }
    }


    public function upload_image(Request $request){
        if(!empty($request->file('file'))){
            $input = array('file' => $request->file('file'));
        }else{
            $input = array('file' => ['null']);
            $msg = [
                "file" => [0 => "The file must be required."]
            ];
            return  response()->json(['success'=>'Failed to upload picture','data'=> $request->file('file'),'errors'=>$msg],422);
        }


        $validator = Validator::make($input, [
            'file.*' => 'required|mimes:jpeg,bmp,png,jpg'
        ]);

        if ($validator->fails()) {
            return  response()->json(['success'=>'Failed to upload picture','data'=> $request->file('file'),'errors'=>$validator->errors()],422);
        }

        if(!empty($request->file('file'))){
            for ($i=0; $i < count($request->file('file')) ; $i++) {
                $image = $request->file('file')[$i];
                $image_name = md5(time().$i).'.' . $image->getClientOriginalExtension();
                $path = 'shoes_model/'.date("Y").'/'.date("m")."/";
                $destinationPath = public_path('assets/images/'.$path);
                // $background = Image::canvas(200,200);

                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0775, true);
                }

                $image = Image::make($image->getRealPath());

                // $image = $background->insert($image,'center');

                $upload =  $image->save($destinationPath.$image_name);

                if(!empty($upload)){
                    $data = [
                        'id_images'   => IdGenerator::generate(['table' => 'images','field'=>'id_images', 'length' => 7, 'prefix' =>'IMG','reset_on_prefix_change'=>'true']),
                        'img_path'    => $path.$image_name,
                    ];
                }else{
                    $data = [
                        'id_images'             => IdGenerator::generate(['table' => 'images','field'=>'id_images', 'length' => 7, 'prefix' =>'IMG','reset_on_prefix_change'=>'true']),
                        'img_path' => 'shoes_model/default.jpg',
                    ];

                }

                if(file_exists($destinationPath.$image_name)){
                    $data = Image_file::create($data);
                }
            }
        }

        $images = Image_file::whereNull('id_shoes_model')->get();

        if($data){
            return response()->json(['success'=>'Data is successfully added','data'=> $request->file('file'),'current_img'=>$images]);
        }else{
            return  response()->json(['success'=>'Failed to add data','data'=> $request->file('file')],422);
        }
    }

    public function update_image(Request $request,$id){

        if(!empty($request->file('update_file'))){
            $input = array('update_file' => $request->file('update_file'));
        }else{
            $input = array('update_file' => ['null']);
            $msg = [
                "update_file" => [0 => "The file must be required."]
            ];
            return  response()->json(['success'=>'Failed to upload picture','data'=> $request->file('update_file'),'errors'=>$msg],422);
        }


        $validator = Validator::make($input, [
            'update_file.*' => 'required|mimes:jpeg,bmp,png,jpg'
        ]);

        if ($validator->fails()) {
            return  response()->json(['success'=>'Failed to upload picture','data'=> $request->file('update_file'),'errors'=>$validator->errors()],422);
        }

        if(!empty($request->file('update_file'))){
            for ($i=0; $i < count($request->file('update_file')) ; $i++) {
                $image = $request->file('update_file')[$i];
                $image_name = md5(time().$i).'.' . $image->getClientOriginalExtension();
                $path = 'shoes_model/'.date("Y").'/'.date("m")."/";
                $destinationPath = public_path('assets/images/'.$path);
                // $background = Image::canvas(200,200);

                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0775, true);
                }

                $image = Image::make($image->getRealPath());

                // $image = $background->insert($image,'center');

                $upload =  $image->save($destinationPath.$image_name);

                if(!empty($upload)){
                    $data = [
                        'id_images'      => IdGenerator::generate(['table' => 'images','field'=>'id_images', 'length' => 7, 'prefix' =>'IMG','reset_on_prefix_change'=>'true']),
                        'img_path'       => $path.$image_name,
                        'id_shoes_model' => $id
                    ];
                }else{
                    $data = [
                        'id_images'             => IdGenerator::generate(['table' => 'images','field'=>'id_images', 'length' => 7, 'prefix' =>'IMG','reset_on_prefix_change'=>'true']),
                        'img_path' => 'shoes_model/default.jpg',
                    ];
                }

                $data = Image_file::create($data);
            }
        }

        $images = Image_file::where('id_shoes_model',$id)->get();

        if($data){
            return response()->json(['success'=>'Data is successfully added','data'=> $request->file('update_file'),'current_img'=>$images]);
        }else{
            return  response()->json(['failed'=>'Failed to deleted data','data'=> $shoes],422);
        }
    }

    public function delete_image(Request $request,$id){

        $image = Image_file::find($id);

        if($image){

            $image_path = public_path('assets/images/'.$image->img_path);

                if(!empty($image->id_shoes_model)){
                    $image->delete();

                    if(file_exists($image_path)){
                        unlink($image_path);
                    }

                    $images = Image_file::where('id_shoes_model',$image->id_shoes_model)->get();
                }else{
                    $image->delete();

                    if(file_exists($image_path)){
                        unlink($image_path);
                    }

                    $images = Image_file::whereNull('id_shoes_model')->get();
                }

                return response()->json(['success'=>'Data has been deleted','current_img'=>$images]);

        }else{
            return  response()->json(['failed'=>'Data has been deleted','data'=> $id],422);
        }
    }
}
