<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Haruncpi\LaravelIdGenerator\IdGenerator;

use App\Providers\RouteServiceProvider;
use App\User;
use App\Activity_log;

use Image;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
       $this->middleware('auth');
    }

    public function main_dashboard(){
         return view('main');
    }

    public function index()
    {
        $users = User::orderBy('created_at','desc')->get();

        return view('users.users',['users' => $users]);
    }

    public function logout(Request $request){

       $id = IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL', 'reset_on_prefix_change'=>'true']);

       $log = [
                'id_log'        => $id,
                'id_user'       => session('id_user'),
                'activity_name' => 'Logout of the systems.',
                'ip_address'    => $request->ip()
               ];

        Activity_log::create($log);
        Session::flush();
        return redirect('/')->with('alert-success','Kamu sudah logout');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('users.users_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
                'name'             => 'required|regex:/^[a-zA-Z ]+$/|min:5|max:25',
                'email'            => 'required|min:5|max:30|email|unique:users',
                'phone_number'     => 'required|unique:users|numeric',
                'password'         => 'required|min:6|max:20',
                'repassword'       => 'required|same:password',
                'role'             => 'required',
        ]);

        if ($validator->fails()) {
            return  redirect()->back()->withErrors($validator)->withInput();
        }

        $data = [
                'id_user'         => IdGenerator::generate(['table' => 'users','field'=>'id_user', 'length' => 7, 'prefix' =>'U','reset_on_prefix_change'=>'true']),
                'name'            => $request->name,
                'phone_number'    => $request->phone_number,
                'email'           => $request->email,
                'password'        => bcrypt($request->password),
                'role'            => $request->role
            ];

        $data = User::create($data);

        if($data){

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Create new user.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return redirect('users')->with('alert-success','Berhasil menambah data.');
        }else{
            return redirect('users/create')->with('alert','Gagal menambah data.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $users = User::where('id_user',$id)->get();

        return view('users.users_edit',['users' => $users]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = User::where('id_user',$id)->get()->first();
        //
        $data = [
                    'name'            => $request->name,
                    'role'            => $request->role
                ];

        $validation_value = [
                'name' => 'required|regex:/^[a-zA-Z ]+$/|min:5|max:25',
                'role' => 'required',
        ];

        if(!empty($request->password)){
            $validation_value += [
                'password' => 'required|min:6|max:20',
                'repassword' => 'required|same:password'
            ];
            $data += [
                'password' => bcrypt($request->password)
            ];
        }

        if(!empty($request->email) ){

            if($request->email != $users->email){
                $validation_value +=  [
                    'email'           => 'required|email|min:5|max:30|unique:users',
                ];
                $data += [
                    'email' => $request->email
                ];
            }
        }else{
            $validation_value +=  [
                'email'           => 'required|email|min:5|max:30|unique:users',
            ];
        }

        if(!empty($request->phone_number)){
            if($request->phone_number != $users->phone_number){
              $validation_value +=  [
                  'phone_number'     => 'required|unique:users|numeric'
              ];
              $data += [
                'phone_number' => $request->phone_number
              ];
          }
        }

        $validator = Validator::make($request->all(),$validation_value);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $data = User::where('id_user',$id)->update($data);

        if($data){

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Update the user.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

             return redirect('users')->with('alert-success','Berhasil mengubah data.');
        }else{
             return redirect('users')->with('alert-success','Gagal merubah data.');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
       // delete
        $users = User::where('id_user',$id)->get()->first();

        if($users){
            $users->delete();

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Delete the user.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            // redirect
            Session::flash('message', 'Successfully deleted the users!');
            return redirect('users')->with('alert-success','Successfully deleted the users!');
        }else{
            return redirect('users')->with('alert-success','Data has been deleted!');
        }

    }

}
