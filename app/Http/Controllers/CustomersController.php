<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Haruncpi\LaravelIdGenerator\IdGenerator;


use App\Customers;
use App\Activity_log;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $customers = Customers::orderBy('created_at','desc')->get();

        return view('customers.customers',['customers' => $customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('customers.customers_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $validator = Validator::make($request->all(), [
                'name'              => 'required|regex:/^[a-zA-Z ]+$/|min:5|max:20',
                'customer_email'    => 'required|min:5|max:30|email|unique:customers',
                'customer_phone'    => 'required|unique:customers|numeric',
                'company'           => 'required',
                'address'           => 'required'

        ]);

        if ($validator->fails()) {
            return  redirect()->back()->withErrors($validator)->withInput();
        }else{
            $data = [
                'id_customer'        => IdGenerator::generate(['table' => 'customers','field'=>'id_customer', 'length' => 7, 'prefix' =>'CO','reset_on_prefix_change'=>'true']),
                'customer_name'     => $request->name,
                'customer_email'    => $request->customer_email,
                'customer_phone'    => $request->customer_phone,
                'company'           => $request->company,
                'address'           => $request->address
            ];

            $data = Customers::create($data);
        }


        if($data){
            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Created new customers.',
                'ip_address'    => $request->ip()
               ];

            Activity_log::create($log);
            return redirect('customers')->with('alert-success','Berhasil menambah data.');
        }else{
            return redirect('customers/create')->with('alert','Gagal menambah data.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function show(Customers $customers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $customers = Customers::where('id_customer',$id)->get();

        return view('customers.customers_edit',['customers' => $customers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, customers $customers,$id)
    {
        //
        $customers = Customers::find($id);
        //
        $data = [
                'customer_name'     => $request->name,
                'customer_email'    => $request->customer_email,
                'customer_phone'    => $request->customer_phone,
                'company'           => $request->company,
                'address'           => $request->address
        ];

        $validation_value = [
                'name'           => 'required|regex:/^[a-zA-Z ]+$/|min:5|max:25',
                'company'        => 'required',
                'address'        => 'required',
                'customer_email' =>
                                    [
                                        'required',
                                        'min:5',
                                        'max:30',
                                        Rule::unique('customers')->ignore($id,'id_customer')
                                    ],
                'customer_phone' =>
                                    [
                                        'required',
                                        'min:10',
                                        'numeric',
                                        Rule::unique('customers')->ignore($id,'id_customer')
                                    ],
        ];



        $validator = Validator::make($request->all(),$validation_value);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $data = Customers::where('id_customer',$id)->update($data);

        if($data){
            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Edited customers.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);
            return redirect('customers')->with('alert-success','Berhasil mengubah data.');
        }else{
            return redirect('customers/create')->with('alert','Gagal mengubah data.');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customers $customers,Request $request,$id)
    {
        //
        $customers = Customers::find($id);

        if($customers){
            $customers->delete();
            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Deleted customers.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);
            // redirect
            Session::flash('message', 'Successfully deleted the customers!');
            return redirect('customers')->with('alert-success','Successfully deleted the customers!');
        }else{
            return redirect('customers')->with('alert-success','Data has been deleted!');
        }
    }
}
