<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Haruncpi\LaravelIdGenerator\IdGenerator;

use App\Additional;
use App\Activity_log;


class AdditionalController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'add_name'              =>  [
                                        'required',
                                        'regex:/^[a-zA-Z ]+$/',
                                        'min:4',
                                        'max:20',
                                        Rule::unique('additional')->where(function ($query) use ($request) {
                                            return $query->where('id_po', null);
                                        })
                                    ],
            'unit_price'            =>  'required|min:1|numeric',
            'unit'                  =>  'required|min:1|numeric',
            'description'           =>  'required'
        ]);


        if ($validator->fails()) {
            return  response()->json(['success'=>'Failed to add data','data'=> $request->all(),'errors'=>$validator->errors()],422);
        } 
        
        $data = [
                'id_add'            => IdGenerator::generate(['table' => 'additional','field'=>'id_add', 'length' => 7, 'prefix' =>'AD','reset_on_prefix_change'=>'true']),
                'add_name'          => $request->add_name,
                'add_price'         => $request->unit_price,
                'add_unit'          => $request->unit,
                'add_description'   => $request->description
            ];

        if(!empty($request->id_po)){
            $data += [
                'id_po' => $request->id_po
            ];
        }

        $data = Additional::create($data);

        if($data){
            $additionals = Additional::orderBy('created_at','desc')->get();

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Create additional.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return response()->json(['success'=>'Data is successfully added','data'=> $request->all(),'all_data'=>$additionals]);
        }else{
            return  response()->json(['success'=>'Failed to add data','data'=> $request->all()],422);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Additional  $additional
     * @return \Illuminate\Http\Response
     */
    public function show(Additional $additional)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Additional  $additional
     * @return \Illuminate\Http\Response
     */
    public function edit(Additional $additional)
    {
        //
        $additionals = Additional::where('id_add',$additional->id_add)->get()->first();

         if($additionals){
            return response()->json(['success'=>'Data is successfully got','data'=> $additionals]);
        }else{
            return  response()->json(['success'=>'Failed to get data'],422);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Additional  $additional
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Additional $additional)
    {
        //
        $validator = Validator::make($request->all(), [
            'add_name'             => [
                                            'required',
                                            'regex:/^[a-zA-Z ]+$/',
                                            'min:4',
                                            'max:20',
                                            Rule::unique('additional')
                                                  ->ignore($additional->id_add,'id_add')
                                                  ->where(function ($query) use ($request) {
                                                        return $query->where('id_po',null);
                                                  })
                                        ],
            'add_price'            =>  [
                                            'required',
                                            'min:1',
                                            'numeric' 
                                        ],
            'add_unit'            =>  [
                                            'required',
                                            'min:1',
                                            'numeric'
                                        ],
            'add_description'            =>  [
                                            'required',
                                        ],
        ]);


        if ($validator->fails()) {
        return  response()->json(['success'=>'Failed to add data','data'=> $request->all(),'errors'=>$validator->errors()],422);
        }

        $data = [
            'add_name'          => $request->add_name,
            'add_price'         => $request->add_price,
            'add_unit'          => $request->add_unit,
            'add_description'   => $request->add_description
        ];

        $data = Additional::where('id_add',$additional->id_add)->update($data);

        $additionals = Additional::orderBy('created_at','desc')->get();

        if($data){

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Update additional.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);
            
            return response()->json(['success'=>'Data is successfully updated','data'=> $request->all(),'all_data'=>$additionals]);
        }else{
            return  response()->json(['success'=>'Failed to update data','data'=> $request->all()],422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Additional  $additional
     * @return \Illuminate\Http\Response
     */
    public function destroy(Additional $additional,Request $request)
    {
        $additionals = Additional::where('id_add',$additional->id_add)->get()->first();
       
        
        if($additionals){

            $additionals->delete();

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Delete additional.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return redirect('po/create')->with('alert-success','Successfully deleted the additional!');
        }else{

            return redirect('po/create')->with('alert-success','Data has been deleted!');
        }
    }
}
