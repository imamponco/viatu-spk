<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Haruncpi\LaravelIdGenerator\IdGenerator;

use App\Purchase_order;
use App\Detail;
use App\Customers;
use App\Additional;
use App\Shoes_model;
use App\Tr_shoes;
use App\Activity_log;

class QualityController extends Controller
{
    //
    public function index(){
        $now = NOW()->format('Y-m-d');
        $po = Purchase_order::select('*','users.id_user as id_user','users.name as username','customers.customer_name as customer_name','purchase_orders.id_po as id')
                ->leftJoin('customers', 'purchase_orders.id_customer', '=', 'customers.id_customer')
                ->leftJoin('users', 'purchase_orders.id_user', '=', 'users.id_user')
                // ->leftJoin('history_dss', 'purchase_orders.id_po', '=', 'history_dss.id_po')
                ->where('purchase_orders.status','=',1)
                ->whereRaw('CURDATE() <= purchase_orders.date_production')
                // ->where('purchase_orders.status_verification','=',1)
                ->orderBy('purchase_orders.updated_at','asc')
                ->get();
       
        return view('qa.qa',['po' => $po]);
    }

    public function edit($id){

        $shoes_model = Tr_shoes::select('*','tr_shoes_models.id_tr_shoes as id_tr')
                                ->leftJoin('shoes_models','tr_shoes_models.id_shoes_model','=','shoes_models.id_shoes_model')
                                ->leftJoin('purchase_orders','tr_shoes_models.id_po','=','purchase_orders.id_po')
                                ->where('tr_shoes_models.id_tr_shoes',$id)
                                ->get();

        $detail = Detail::select("*","detail_orders.detail_description as description","detail_orders.id_do as id")
                          ->leftJoin('tr_shoes_models as t', 'detail_orders.id_tr_shoes', '=', 't.id_tr_shoes')
                          ->leftJoin('shoes_models', 't.id_shoes_model', '=', 'shoes_models.id_shoes_model')
                          ->where('detail_orders.id_tr_shoes',$id)
                          ->orderBy('detail_orders.created_at','desc')
                          ->get();

        return view('detail.detail_view',['detail' => $detail,'shoes_model' => $shoes_model]);
    }

    public function show($id){

         //
         $current_data = Purchase_order::select('*','purchase_orders.id_po as id')
         ->where('purchase_orders.id_po',$id)
         ->leftJoin('customers as c','purchase_orders.id_customer','=','c.id_customer')
         ->get();

        $customers = Customers::get();

        $current_shoes = DB::table('tr_shoes_models as t')
        ->select('*','t.id_tr_shoes as id_tr',DB::raw('COUNT(d.id_do) as amount'))
        ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
        ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
        ->groupBy('t.id_tr_shoes')
        ->orderBy('t.created_at','desc')
        ->where('id_po',$id)
        ->get();

        $shoes = Shoes_model::get();

        $additionals = Additional::select('*')
        ->where('id_po',$id)
        ->get();

        return view('purchase.po_view',['customers' => $customers,'shoes' => $shoes,'additionals' => $additionals,'current_data' => $current_data,'current_shoes' => $current_shoes]);

    }

    public function update(Request $request,$id){

        $split = explode('-',$id);

        $id = $split[2];

        $id_tr = $split[0];

        $id_po = $split[1];

        $data = [
            'quality_status' => 1
        ];

        $data = Detail::where('id_do',$id)->where('id_tr_shoes',$id_tr)->update($data);

        if($data){

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Check detail shoes.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return redirect('quality/'.$id_tr.'/edit')->with('alert-success','Berhasil mengubah data.');
        }else{
            return redirect('quality/'.$id_tr.'/edit')->with('alert','Gagal mengubah data.');
        }

    }


    public function cancel(Request $request,$id){

        $split = explode('-',$id);

        $id = $split[2];

        $id_tr = $split[0];

        $id_po = $split[1];

        $data = [
            'quality_status' => 0
        ];

        $data = Detail::where('id_do',$id)->where('id_tr_shoes',$id_tr)->update($data);

        if($data){

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Cancel check detail shoes.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return redirect('quality/'.$id_tr.'/edit')->with('alert-success','Berhasil mengubah data.');
        }else{
            return redirect('quality/'.$id_tr.'/edit')->with('alert','Gagal mengubah data.');
        }

    }

    public function check_all($idpo){

        $data = [
            'quality_status' => 1
        ];

        $check = Tr_shoes::select('*')
                          ->leftJoin('detail_orders as d','tr_shoes_models.id_tr_shoes','=','d.id_tr_shoes')
                          ->where('tr_shoes_models.id_po',$idpo)
                          ->update($data);
        if($check){
            
            return redirect('do')->with('alert-success','Berhasil mengubah data.');
        }
    }


}
