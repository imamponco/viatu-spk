<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Haruncpi\LaravelIdGenerator\IdGenerator;

use App\Tr_Criteria;
use App\VwConsistency;
use App\Purchase_order;
use App\Criteria;
use App\Customers;
use App\Shoes_model;
use App\Tr_shoes;
use App\Additional;
use App\Image_file;
use App\Activity_log;
use App\Detail;
use App\Weights;

use Carbon;

class JudgementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         //
         $consistency = VwConsistency::get();

         $weight_value = Weights::select(DB::RAW("COUNT(id_weight) AS count"))->where('weight_value',0)->get();

         $raw          = Tr_Criteria::select('p.id_po as id_po','tr_criterias.id_trc as id','c.criteria_code','c.options','tr_criterias.min_value','tr_criterias.max_value','p.status')
                                    ->leftJoin('purchase_orders as p','tr_criterias.id_po','=','p.id_po')
                                    ->leftJoin('criterias as c','tr_criterias.criteria_code','=','c.criteria_code');

        //clear tr
        $clear = Tr_Criteria::select('p.id_po as id_po','tr_criterias.id_trc as id','c.criteria_code','c.options','tr_criterias.min_value','tr_criterias.max_value','p.status')
                                    ->leftJoin('purchase_orders as p','tr_criterias.id_po','=','p.id_po')
                                    ->leftJoin('criterias as c','tr_criterias.criteria_code','=','c.criteria_code')
                                    ->where('status','!=',0)
                                    ->delete();

        //truncate tr
        $truncate = Tr_Criteria::where('criteria_code','WP-01')
                                ->orWhere('criteria_code','JMS-01')
                                ->orWhere('criteria_code','JS-01')
                                ->orWhere('criteria_code','TKP-01')
                                ->delete();

        $criterias = Criteria::orderBy('id_criteria','asc')->get();

        $po = Purchase_order::where('status',0)
                            // ->orWhere('status',1)
                            // ->where('status_verification',1)
                            ->orderBy('created_at','asc')->get();


         $current_po = Tr_Criteria::select('id_po')
                            ->groupBy('id_po')
                            ->get();

         $new_po = Purchase_order::whereNotIn('id_po',$current_po)->where('status',0)
                                // ->where('status_verification',1)
                                ->get();

         if(!empty($new_po[0])){
            //create data tr_criteria
            foreach ($new_po as $k) {
                foreach ($criterias as $cri) {

                    switch ($cri->criteria_code) {
                        case 'JMS-01':
                            $count = Tr_shoes::select(DB::Raw('COUNT(*) as jms'))
                                             ->where('id_po',$k->id_po)
                                             ->get()->first();

                            $jms = $count->jms;

                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => $jms,
                                'max_value'     => $jms
                            ];

                            break;
                        case 'JS-01':
                            $js = Detail::select(DB::raw('COUNT(id_do) as js'))
                                          ->leftJoin('tr_shoes_models as t','detail_orders.id_tr_shoes','=','t.id_tr_shoes')
                                          ->where('t.id_po',$k->id_po)
                                          ->first();
                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => $js->js,
                                'max_value'     => $js->js
                            ];
                            break;
                        case 'WP-01':
                            $deadline = Purchase_order::select(DB::raw('DATEDIFF(purchase_orders.deadline,CURDATE()) as deadline'))
                                                        ->where('purchase_orders.id_po',$k->id_po)
                                                        ->get()
                                                        ->first();

                            $deadline = $deadline->deadline;

                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => $deadline,
                                'max_value'     => $deadline
                            ];
                            break;
                        case 'TKP-01':
                            $js = Detail::select(DB::raw('COUNT(id_do) as js'))
                                        ->leftJoin('tr_shoes_models as t','detail_orders.id_tr_shoes','=','t.id_tr_shoes')
                                        ->where('t.id_po',$k->id_po)
                                        ->first();

                            $current_shoes = DB::table('tr_shoes_models as t')
                                                ->select('*','t.id_tr_shoes as id_tr',DB::raw('COUNT(d.id_do) as unit'))
                                                ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
                                                ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                                                ->groupBy('t.id_tr_shoes')
                                                ->orderBy('t.created_at','desc')
                                                ->where('id_po',$k->id_po)
                                                ->get();

                            $amount_shoes = count($current_shoes);

                            $tkp = 0;
                            //count total tingkat kesulitan berdasarkan model sepatu
                            foreach ($current_shoes as $row) {
                                $tkp = $tkp + ($row->difficulty * $row->unit);
                            }

                            if(!empty($js->js)){
                                $tkp = $tkp/$js->js;
                            }else{
                                $tkp = 1;
                            }

                            //RULES TINGKAT KESULITAN

                            if($tkp > 0 && $tkp <= 1){ // 0-1 Mudah
                                $min_value = 1;
                                $max_value = 10;
                            }elseif($tkp >1 && $tkp <= 2){//1-2 Sedang
                                $min_value = 11;
                                $max_value = 20;
                            }elseif($tkp > 2 && $tkp <=3){//2-3 Sulit
                                $min_value = 21;
                                $max_value = 30;
                            }else{
                                $min_value = 21;
                                $max_value = 30;
                            }

                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => $min_value,
                                'max_value'     => $max_value
                            ];
                            break;

                        default:
                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => 0,
                                'max_value'     => 0
                            ];
                            break;
                    }

                    $trans = Tr_Criteria::create($trans);
                }
             }
        }

        $current_criteria = Tr_Criteria::select('criteria_code')
                                        ->groupBy('criteria_code')
                                        ->get();

        $new_criteria = Criteria::whereNotIn('criteria_code',$current_criteria)->get();

        if(!empty($new_criteria[0])){
             //create data tr_criteria
             foreach ($po as $k) {
                foreach ($new_criteria as $cri) {

                    switch ($cri->criteria_code) {
                        case 'JMS-01':
                            $count = Tr_shoes::select(DB::Raw('COUNT(*) as jms'))
                                             ->where('id_po',$k->id_po)
                                             ->get()->first();

                            $jms = $count->jms;

                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => $jms,
                                'max_value'     => $jms
                            ];

                            break;
                        case 'JS-01':
                            $js = Detail::select(DB::raw('COUNT(id_do) as js'))
                            ->leftJoin('tr_shoes_models as t','detail_orders.id_tr_shoes','=','t.id_tr_shoes')
                            ->where('t.id_po',$k->id_po)
                            ->first();
                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => $js->js,
                                'max_value'     => $js->js
                            ];
                            break;
                        case 'WP-01':
                            $deadline = Purchase_order::select(DB::raw('DATEDIFF(purchase_orders.deadline,CURDATE()) as deadline'))
                                                        ->where('purchase_orders.id_po',$k->id_po)
                                                        ->get()
                                                        ->first();

                            $deadline = $deadline->deadline;

                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => $deadline,
                                'max_value'     => $deadline
                            ];
                            break;
                        case 'TKP-01':
                        $js = Detail::select(DB::raw('COUNT(id_do) as js'))
                                    ->leftJoin('tr_shoes_models as t','detail_orders.id_tr_shoes','=','t.id_tr_shoes')
                                    ->where('t.id_po',$k->id_po)
                                    ->first();

                        $current_shoes = DB::table('tr_shoes_models as t')
                                            ->select('*','t.id_tr_shoes as id_tr',DB::raw('COUNT(d.id_do) as unit'))
                                            ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
                                            ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                                            ->groupBy('t.id_tr_shoes')
                                            ->orderBy('t.created_at','desc')
                                            ->where('id_po',$k->id_po)
                                            ->get();

                        $amount_shoes = count($current_shoes);

                        $tkp = 0;
                        //count total tingkat kesulitan berdasarkan model sepatu
                        foreach ($current_shoes as $row) {
                            $tkp = $tkp + ($row->difficulty * $row->unit);
                        }

                        $tkp = $tkp/$js->js;

                        //RULES TINGKAT KESULITAN
                        if($tkp > 0 && $tkp <= 1){ // 0-1 Mudah
                            $min_value = 1;
                            $max_value = 10;
                        }elseif($tkp >1 && $tkp <= 2){//1-2 Sedang
                            $min_value = 11;
                            $max_value = 20;
                        }elseif($tkp > 2 && $tkp <=3){//2-3 Sulit
                            $min_value = 21;
                            $max_value = 30;
                        }else{
                            $min_value = 21;
                            $max_value = 30;
                        }

                        $trans = [
                            'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                            'id_po'         => $k->id_po,
                            'criteria_code' => $cri->criteria_code,
                            'min_value'     => $min_value,
                            'max_value'     => $max_value
                        ];
                        break;

                        default:
                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => 0,
                                'max_value'     => 0
                            ];
                            break;
                    }

                    $trans = Tr_Criteria::create($trans);
                }
             }

        }

        $tr = $raw->where('status',0)
            // ->orWhere('status',1)
            //  ->where('status_verification',1)
             ->orderBy('c.id_criteria','asc')->get();
        // dd($tr);

        //check options
        foreach ($criterias as $cek) {

           $flag = json_decode($cek->options);

           if(empty($flag)){
                 return response()->json(['success'=>'Tidak dapat membuka halaman ini, karena options pada kriteria memiliki format yang salah.'],422);
            }
        }

         if (!empty($consistency[0]->consistency) && empty($weight_value[0]->count)) {
            if($consistency[0]->consistency == "REJECTED"){
                return response()->json(['success'=>'Tidak dapat membuka halaman ini, karena nilai konsistensi tidak dapat diterima.'],422);
            }
            else{

                return view('judgement.judge',['tr'=>$tr,'criterias'=>$criterias,'po'=>$po]);

            }
         }else{
            return response()->json(['success'=>'Tidak dapat membuka halaman ini, karena nilai konsistensi tidak dapat diterima.'],422);
         }

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $current_data = Purchase_order::select('*','purchase_orders.id_po as id')
                                    ->where('purchase_orders.id_po',$id)
                                    ->leftJoin('customers as c','purchase_orders.id_customer','=','c.id_customer')
                                    ->get();

        $customers = Customers::get();

        $current_shoes = DB::table('tr_shoes_models as t')
                ->select('*','t.id_tr_shoes as id_tr',DB::raw('COUNT(d.id_do) as amount'))
                ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
                ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                ->groupBy('t.id_tr_shoes')
                ->orderBy('t.created_at','desc')
                ->where('id_po',$id)
                ->get();

        $shoes = Shoes_model::get();

        $additionals = Additional::select('*')
                ->where('id_po',$id)
                ->get();

        return view('purchase.po_view',['customers' => $customers,'shoes' => $shoes,'additionals' => $additionals,'current_data' => $current_data,'current_shoes' => $current_shoes]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $r)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\  $r
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $split = explode("_",$id);

        $id_po = $split[1];

        $criteria_code = $split[2];

        $value = explode('-',$request->judgement_value);

        $rules = [
            'id_po'                     =>  'required',
            'criteria_code'             =>  'required',
            'min_value'                 =>  'required|numeric|regex:/^[0-9]+(\.[0-9][0-9]?)?$/|not_in:0',
            'max_value'                 =>  'required|numeric|regex:/^[0-9]+(\.[0-9][0-9]?)?$/|not_in:0|gte:min_value'
        ];

        if($criteria_code=='WP-01'){
            $deadline = Purchase_order::select(DB::raw('DATEDIFF(purchase_orders.deadline,CURDATE()) as deadline'))
                                                        ->where('purchase_orders.id_po',$id_po)
                                                        ->get()
                                                        ->first();

           $deadline = $deadline->deadline;

           $add = "|max:".$deadline;

           $rules['max_value'] = $rules['max_value'].$add;
        }

        if(!empty($value[1])){
            $data = [
                'id_po' => $id_po,
                'criteria_code' => $criteria_code,
                'min_value'     => $value[0],
                'max_value'     => $value[1]
            ];
        }else{
            $data = [
                'id_po' => $id_po,
                'criteria_code' => $criteria_code,
                'min_value'     => $value[0],
                'max_value'     => $value[0]
            ];
        }

        $validator = Validator::make($data, $rules);



        if ($validator->fails()) {
            return  response()->json(['success'=> $validator->errors()->first(),'data'=> $request->all(),'errors'=>$validator->errors()],422);
        }

        $data = Tr_Criteria::where('id_po',$id_po)->where('criteria_code',$criteria_code)->update($data);

        $tr = Tr_Criteria::all();

        if($data){

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Active judgement alt.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return response()->json(['success'=>'Data is successfully updated','data'=> $request->all(),'all_data'=>$tr]);
        }else{
            return  response()->json(['success'=>'Failed to add data','data'=> $request->all()],422);
        }
    }

    public function detail_view($id){

        $shoes_model = Tr_shoes::select('*','tr_shoes_models.id_tr_shoes as id_tr')
                                ->leftJoin('shoes_models','tr_shoes_models.id_shoes_model','=','shoes_models.id_shoes_model')
                                ->where('tr_shoes_models.id_tr_shoes',$id)
                                ->get();

        $detail = Detail::select("*","detail_orders.detail_description as description","detail_orders.id_do as id")
                          ->leftJoin('tr_shoes_models as t', 'detail_orders.id_tr_shoes', '=', 't.id_tr_shoes')
                          ->leftJoin('shoes_models', 't.id_shoes_model', '=', 'shoes_models.id_shoes_model')
                          ->where('detail_orders.id_tr_shoes',$id)
                          ->get();

        return view('detail.detail_view',['detail' => $detail,'shoes_model' => $shoes_model]);

    }

}
