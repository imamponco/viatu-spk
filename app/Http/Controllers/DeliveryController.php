<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Haruncpi\LaravelIdGenerator\IdGenerator;

use App\Purchase_order;
use App\Detail;
use App\Customers;
use App\Additional;
use App\Shoes_model;
use App\Tr_shoes;
use App\Activity_log;
use App\HistoryDss;

class DeliveryController extends Controller
{
    //
    public function index(){
        $now = NOW()->format('Y-m-d');
        $po = Purchase_order::select('*','users.id_user as id_user','users.name as username','customers.customer_name as customer_name','purchase_orders.id_po as id')
        ->leftJoin('customers', 'purchase_orders.id_customer', '=', 'customers.id_customer')
        ->leftJoin('users', 'purchase_orders.id_user', '=', 'users.id_user')
        // ->leftJoin('history_dss', 'purchase_orders.id_po', '=', 'history_dss.id_po')
        ->where('purchase_orders.status','=',1)
        // ->where('purchase_orders.status_verification','=',1)
        ->whereRaw('CURDATE() <= purchase_orders.date_production')
        ->orderBy('purchase_orders.updated_at','asc')
        ->get();

        return view('delivery.delivery',['po' => $po]);
    }

    public function show($id){

        //
        $current_data = Purchase_order::select('*','purchase_orders.id_po as id')
        ->where('purchase_orders.id_po',$id)
        ->leftJoin('customers as c','purchase_orders.id_customer','=','c.id_customer')
        ->get();

       $customers = Customers::get();

       $current_shoes = DB::table('tr_shoes_models as t')
       ->select('*','t.id_tr_shoes as id_tr',DB::raw('COUNT(d.id_do) as amount'))
       ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
       ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
       ->groupBy('t.id_tr_shoes')
       ->orderBy('t.created_at','desc')
       ->where('id_po',$id)
       ->get();

       $shoes = Shoes_model::get();

       $additionals = Additional::select('*')
       ->where('id_po',$id)
       ->get();

       return view('purchase.po_view',['customers' => $customers,'shoes' => $shoes,'additionals' => $additionals,'current_data' => $current_data,'current_shoes' => $current_shoes]);

   }

   public function edit($id){

       $po = Purchase_order::select('id_po','resi_code')->where('id_po',$id)->get()->first();

       if($po){
            return response()->json(['success'=>'Data is successfully','all_data'=>$po]);
        }else{
            return response()->json(['success'=>'Failed to get data','all_data'=>$po]);
        }

   }

   public function update(Request $request,$id){

    $validator = Validator::make($request->all(), [
        'resicode'                   =>  'required'
    ]);


    if ($validator->fails()) {
        return  response()->json(['success'=>'Failed to add data','data'=> $request->all(),'errors'=>$validator->errors()],422);
    }

    $data = [
        'resi_code'   => $request->resicode,
        'status'      => 2,
        'date_finish' => NOW()->format('Y-m-d')
    ];

    $data = Purchase_order::where('id_po',$id)->update($data);

    $po = Purchase_order::all();

    if($data){

        $log = [
            'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
            'id_user'       => session('id_user'),
            'activity_name' => 'Purchase order done.',
            'ip_address'    => $request->ip()
        ];

        Activity_log::create($log);

        return response()->json(['success'=>'Data is successfully updated','data'=> $request->all(),'all_data'=>$po]);
    }else{
        return response()->json(['success'=>'Data is failed to update','data'=> $request->all(),'all_data'=>$po]);
    }

   }

   public function cancel(Request $request,$id){

    $data = [
        'resi_code' => null,
        'status' => 1
    ];

    $data = Purchase_order::where('id_po',$id)->update($data);

    if($data){
        $log = [
            'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
            'id_user'       => session('id_user'),
            'activity_name' => 'Cancel done purchase order.',
            'ip_address'    => $request->ip()
        ];

        Activity_log::create($log);
        return redirect('do')->with('alert-success','Berhasil mengubah data.');
    }else{
        return redirect('do')->with('alert','Gagal mengubah data.');
    }

    }

    public function update_resicode(Request $request,$id){

        $validator = Validator::make($request->all(), [
            'resicode'                   =>  'required'
        ]);


        if ($validator->fails()) {
            return  response()->json(['success'=>'Failed to add data','data'=> $request->all(),'errors'=>$validator->errors()],422);
        }

        $data = [
            'resi_code' => $request->resicode,
        ];

        $data = Purchase_order::where('id_po',$id)->update($data);

        $po = Purchase_order::all();

        if($data){
            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Po resicode update.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return response()->json(['success'=>'Data is successfully updated','data'=> $request->all(),'all_data'=>$po]);
        }else{
            return response()->json(['success'=>'Data is failed to update','data'=> $request->all(),'all_data'=>$po]);
        }

    }
}
