<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Haruncpi\LaravelIdGenerator\IdGenerator;


use App\User;
use App\Activity_log;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

     public function do_login(Request $request){
        $email = $request->email;
        $password = $request->password;

        $data = User::where('email',$email)->orWhere('phone_number',$email)->first();

        if($data){ //apakah email tersebut ada atau tidak
            if(Hash::check($password,$data->password)){
                
                if(auth()->attempt(array('email' => $data->email, 'password' => $request->password)))
                {
                    Session::put('id_user',$data->id_user);
                    Session::put('role',$data->role);
                    Session::put('name',$data->name);
                    Session::put('email',$email);
                    Session::put('login',TRUE);

                    $id = IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']);
                 
                    $log = [
                            'id_log'        => $id,
                            'id_user'       => $data->id_user,
                            'activity_name' => 'Login into the systems.',
                            'ip_address'    => $request->ip()
                    ];

                    Activity_log::create($log);

                    return redirect()->route('dashboard');

                }else{

                    return redirect()->route('/')->with('error','Email atau password, Salah !');

                }

            }else{
                    return redirect('/')->with('alert','Email atau password, Salah !');
                }
        }
        else{
            return redirect('/')->with('alert','Email atau password, Salah!');
        }

    }
}
