<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Haruncpi\LaravelIdGenerator\IdGenerator;

use Carbon\Carbon;

use App\Purchase_order;
use App\Customers;
use App\Shoes_model;
use App\Tr_shoes;
use App\Additional;
use App\Image_file;
use App\Activity_log;
use App\Detail;
use App\Criteria;
use App\Tr_Criteria;
use App\HistoryDss;

class PurchaseOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $po = Purchase_order::select('purchase_orders.id_po as id','customers.customer_name','purchase_orders.date_order','purchase_orders.deadline','purchase_orders.status','users.id_user as id_user','users.name as username')
                ->leftJoin('customers', 'purchase_orders.id_customer', '=', 'customers.id_customer')
                ->leftJoin('users', 'purchase_orders.id_user', '=', 'users.id_user')
                ->where('purchase_orders.status','=',0)
                ->orderBy('purchase_orders.created_at','desc')
                ->get();

        return view('purchase.po',['po' => $po]);
    }

    public function get_po($parameter){

        $split = explode('_',$parameter);

        $start_date = "$split[0]";

        $end_date = "$split[1]";

        $status = (integer) $split[2];

        $delivery = (integer) $split[3];

        // $status_verification = (integer) $split[3];

        $po = Purchase_order::select('purchase_orders.id_po as id_po','customers.customer_name as customer_name','purchase_orders.date_order','purchase_orders.deadline','purchase_orders.status','users.id_user as id_user','users.name as username','purchase_orders.resi_code')
                                ->leftJoin('customers', 'purchase_orders.id_customer', '=', 'customers.id_customer')
                                ->leftJoin('users', 'purchase_orders.id_user', '=', 'users.id_user');
                                // ->leftJoin('history_dss', 'purchase_orders.id_po', '=', 'history_dss.id_po');
      
        if(!empty($start_date) && !empty($end_date)){
            $po->whereBetween('purchase_orders.date_order', array($start_date,$end_date));
        }

        if($status==1 || $status===0 || $status==2){

            $po->where('purchase_orders.status','=',$status);

        }
        if($delivery == 1 && $status == 1){
            // $po->whereRaw('CURDATE() >= purchase_orders.date_production');
            $po = $po->orderBy('purchase_orders.updated_at','asc')->get();
        }else{
            $po = $po->orderBy('purchase_orders.created_at','desc')->get();
        }
        // dd($delivery);

        // if($status_verification==1 || $status_verification===0 || $status_verification==2){

        //     $po->where('purchase_orders.status_verification','=',$status_verification);

        // }else{

        // 
        $check = [];

        $no = 1;

        foreach ($po as $row) {

            $all_check = false;

            $current = DB::table('tr_shoes_models as t')
                        ->select(DB::raw('COUNT(d.id_do) as not_check'))
                        ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
                        ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                        ->groupBy('t.id_tr_shoes')
                        ->orderBy('t.created_at','desc')
                        ->where('t.id_po',$row->id_po)
                        ->where('d.quality_status',0)
                        ->get()
                        ->toArray();

            if(empty($current[0]->not_check)){

                $all_check = true;

                $check += [
                    'id' => $row->id_po,
                    'all_check' => $all_check
                ];

            }

            $no++;
        }
       
        if($po){
            return response()->json(['success'=>'Data is successfully get','all_data'=>$po,'check'=>$check]);
        }else{
            return  response()->json(['success'=>'Failed to update data'],422);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $customers = Customers::get();

        $shoes = DB::table('tr_shoes_models as t')
                ->select('*','t.id_tr_shoes as id_tr',DB::raw('COUNT(d.id_do) as unit'))
                ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
                ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                ->groupBy('t.id_tr_shoes')
                ->orderBy('t.created_at','desc')
                ->whereNull('id_po')
                ->get();

        $additionals = Additional::select('*')
                ->whereNull('id_po')
                ->orderBy('created_at','desc')
                ->get();
        $images = Image_file::get()->whereNull('id_shoes_model');

        //current id po
        $current_shoes = DB::table('tr_shoes_models as t')
                            ->select('*','t.id_tr_shoes as id_tr',DB::raw('COUNT(d.id_do) as unit'))
                            ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
                            ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                            ->groupBy('t.id_tr_shoes')
                            ->orderBy('t.created_at','desc')
                            ->whereNull('id_po')
                            ->get();

        //empty parameter
        $empty = 0;
        $amount_model = count($current_shoes);

        if($amount_model==0){
            $empty = 1;
        }
        //checking detail
        for ($i=0; $i < $amount_model; $i++) {
            $detail_data[$i] = Detail::select('*',DB::raw('COUNT(size) as amount_size'),'detail_orders.detail_description as description')
                                        ->leftJoin('tr_shoes_models as t','detail_orders.id_tr_shoes','=','t.id_tr_shoes')
                                        ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                                        ->where('t.id_tr_shoes',$current_shoes[$i]->id_tr)
                                        ->where('t.id_shoes_model',$current_shoes[$i]->id_shoes_model)
                                        ->groupBy('detail_orders.id_do')
                                        ->orderBy('size','asc')
                                        ->get();

            if(empty($detail_data[$i][0]->id_tr_shoes)){
                $empty++;
            }

        }

        return view('purchase.po_add',['customers' => $customers,'shoes' => $shoes,'additionals' => $additionals,'images' => $images,'amount_model'=>$amount_model,'param_detail'=>$empty]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'id_customer'     => 'required',
            'date_order'      => 'required|date',
            'deadline'        => 'required|date|after:date_order'
        ]);
        //get current shoes
        $current_shoes = DB::table('tr_shoes_models as t')
                ->select('*','t.id_tr_shoes as id_tr',DB::raw('COUNT(d.id_do) as unit'))
                ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
                ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                ->groupBy('t.id_tr_shoes')
                ->orderBy('t.created_at','desc')
                ->whereNull('id_po')
                ->get();

        //empty parameter
        $empty = 0;
        $amount_model = count($current_shoes);

        if($amount_model<1){
            return redirect('po/create')->with('alert','Shoes model tidak boleh kosong.')->withInput();;
        }

        //checking detail
        for ($i=0; $i < $amount_model; $i++) {
            $detail_data[$i] = Detail::select('*',DB::raw('COUNT(size) as amount_size'),'detail_orders.detail_description as description')
                                        ->leftJoin('tr_shoes_models as t','detail_orders.id_tr_shoes','=','t.id_tr_shoes')
                                        ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                                        ->where('t.id_tr_shoes',$current_shoes[$i]->id_tr)
                                        ->where('t.id_shoes_model',$current_shoes[$i]->id_shoes_model)
                                        ->groupBy('detail_orders.id_do')
                                        ->orderBy('size','asc')
                                        ->get();

            if(empty($detail_data[$i][0]->id_tr_shoes)){
                $empty++;
            }

        }

        if($empty>0){
            return redirect('po/create')->with('alert','Detail data tidak boleh kosong.')->withInput();;
        }

        if ($validator->fails()) {
            return  redirect()->back()->withErrors($validator)->withInput();
        }

        $data = [
            'id_po'                       => IdGenerator::generate(['table' => 'purchase_orders','field'=>'id_po', 'length' => 7, 'prefix' =>'PO','reset_on_prefix_change'=>'true']),
            // 'po_code'                     => IdGenerator::generate(['table' => 'purchase_orders','field'=>'po_code', 'length' => 8, 'prefix' =>'PO','reset_on_prefix_change'=>'true']),
            'id_user'                     => session('id_user'),
            'id_customer'                 => $request->id_customer,
            'date_order'                  => $request->date_order,
            'deadline'                    => $request->deadline,
            'status'                      => 0,
            // 'status_verification'         => 0
        ];

        $data = Purchase_order::create($data);

        if($data){

            $update = [
                'id_po' => $data->id_po
            ];

            $update1 = Tr_shoes::whereNull('id_po')->update($update);
            $update2 = Additional::whereNull('id_po')->update($update);

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'New po '.$data->id_po,
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);


            return redirect('po')->with('alert-success','Berhasil menambah data.');
        }else{
            return redirect('po/create')->with('alert','Gagal menambah data.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Purchase_order  $purchase_order
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase_order $purchase_order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purchase_order  $purchase_order
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase_order $purchase_order,$id)
    {
        //
        $current_data = Purchase_order::select('*','purchase_orders.id_po as id')
                                    ->leftJoin('customers as c','purchase_orders.id_customer','=','c.id_customer')
                                    ->where('purchase_orders.id_po',$id)
                                    ->get();

        $customers = Customers::get();

        $current_shoes = DB::table('tr_shoes_models as t')
                ->select('*','t.id_tr_shoes as id_tr',DB::raw('COUNT(d.id_do) as amount'))
                ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
                ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                ->groupBy('t.id_tr_shoes')
                ->orderBy('t.created_at','desc')
                ->where('id_po',$id)
                ->get();
      
        $shoes = Shoes_model::get();

        $additionals = Additional::select('*')
                ->where('id_po',$id)
                ->orderBy('created_at','desc')
                ->get();

         //empty parameter
         $empty = 0;
         $amount_model = count($current_shoes);

         if($amount_model==0){
            $empty = 1;
         }

         //checking detail
         for ($i=0; $i < $amount_model; $i++) {
             $detail_data[$i] = Detail::select('*',DB::raw('COUNT(size) as amount_size'),'detail_orders.detail_description as description')
                                         ->leftJoin('tr_shoes_models as t','detail_orders.id_tr_shoes','=','t.id_tr_shoes')
                                         ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                                         ->where('t.id_tr_shoes',$current_shoes[$i]->id_tr)
                                         ->where('t.id_shoes_model',$current_shoes[$i]->id_shoes_model)
                                         ->groupBy('detail_orders.id_do')
                                         ->orderBy('size','asc')
                                         ->get();

             if(empty($detail_data[$i][0]->id_tr_shoes)){
                 $empty++;
             }

         }


        return view('purchase.po_edit',['customers' => $customers,'shoes' => $shoes,'additionals' => $additionals,'current_data' => $current_data,'current_shoes' => $current_shoes,'amount_model'=>$amount_model,'param_detail'=>$empty]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Purchase_order  $purchase_order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase_order $purchase_order,$id)
    {
        $validator = Validator::make($request->all(), [
            'id_customer'     => 'required',
            'date_order'      => 'required|date',
            'deadline'        => 'required|date|after:date_order'
        ]);

          //get current shoes
          $current_shoes = DB::table('tr_shoes_models as t')
          ->select('*','t.id_tr_shoes as id_tr',DB::raw('COUNT(d.id_do) as unit'))
          ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
          ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
          ->groupBy('t.id_tr_shoes')
          ->orderBy('t.created_at','desc')
          ->where('id_po',$id)
          ->get();

        //empty parameter
        $empty = 0;
        $amount_model = count($current_shoes);

        if($amount_model<1){
            return redirect('po/'.$id.'/edit')->with('alert','Shoes model tidak boleh kosong.')->withInput();;
        }

        //checking detail
        for ($i=0; $i < $amount_model; $i++) {
            $detail_data[$i] = Detail::select('*',DB::raw('COUNT(size) as amount_size'),'detail_orders.detail_description as description')
                                        ->leftJoin('tr_shoes_models as t','detail_orders.id_tr_shoes','=','t.id_tr_shoes')
                                        ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                                        ->where('t.id_tr_shoes',$current_shoes[$i]->id_tr)
                                        ->where('t.id_shoes_model',$current_shoes[$i]->id_shoes_model)
                                        ->groupBy('detail_orders.id_do')
                                        ->orderBy('size','asc')
                                        ->get();

            if(empty($detail_data[$i][0]->id_tr_shoes)){
                $empty++;
            }

        }

        if($empty>0){
            return redirect('po/'.$id.'/edit')->with('alert','Detail data tidak boleh kosong.')->withInput();;
        }

        if ($validator->fails()) {
            return  redirect()->back()->withErrors($validator)->withInput();
        }

        $data = [
            'id_user'             => session('id_user'),
            'id_customer'         => $request->id_customer,
            'date_order'          => $request->date_order,
            'deadline'            => $request->deadline,
            // 'status_verification' => 0
        ];

        $data = Purchase_order::where('id_po',$id)->update($data);

        $data2 = Tr_shoes::where('id_po',$id)->update(['updated_at' => now()]);

        $criterias = Criteria::get();

        if($data && $data2){

            if(!empty($criterias[0])){
                foreach($criterias as $cri){
                    $tr = [
                        'id_po'         => $id,
                        'criteria_code' => $cri->criteria_code,
                        'min_value'     => 0,
                        'max_value'     => 0
                        ];

                    $data = Tr_Criteria::where('id_po',$id)->where('criteria_code',$cri->criteria_code)->update($tr);
                }
            }

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Managing po '.$id,
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return redirect('po')->with('alert-success','Berhasil menambah data.');
        }else{
            return redirect('po/'.$id.'/edit')->with('alert','Gagal menambah data.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Purchase_order  $purchase_order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        $data = Purchase_order::where('id_po',$id)->get()->first();

        if($data){

            $data->delete();

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Delete purchase orders.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

             // redirect
            return response()->json(['success'=>'Data has been deleted']);
        }else{
            return  response()->json(['failed'=>'Failed to deleted data','data'=> $data],422);
        }
    }


    public function verify_index()
    {
        //
        $po = Purchase_order::select('purchase_orders.id_po as id','customers.customer_name as customer_name','purchase_orders.date_order','purchase_orders.deadline','purchase_orders.status','users.id_user as id_user','users.name as username')
                ->leftJoin('customers', 'purchase_orders.id_customer', '=', 'customers.id_customer')
                ->leftJoin('users', 'purchase_orders.id_user', '=', 'users.id_user')
                ->where('purchase_orders.status','=',0)
                // ->where('purchase_orders.status_verification','=',0)
                ->orderBy('purchase_orders.created_at','desc')
                ->get();

        return view('verify.verify',['po' => $po]);
    }

    public function verify(Request $request,$id)
    {
        $data = [
            'status_verification' => 1
        ];

        $data = Purchase_order::where('id_po',$id)->update($data);

        if($data){

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Verified po '.$id,
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return redirect('verify')->with('alert-success','Berhasil mengubah data.');
        }else{
            return redirect('verify')->with('alert','Gagal mengubah data.');
        }
    }

    public function cancel_verify(Request $request,$id)
    {

        $data = [
            'status_verification' => 0
        ];

        $data = Purchase_order::where('id_po',$id)->update($data);

        if($data){

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Cancel verified po.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return redirect('verify')->with('alert-success','Berhasil mengubah data.');
        }else{
            return redirect('verify')->with('alert','Gagal mengubah data.');
        }
    }

    public function reject_po(Request $request,$id)
    {

        $data = [
            'status_verification' => 2
        ];

        $data = Purchase_order::where('id_po',$id)->update($data);

        if($data){

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Reject verified po.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return redirect('verify')->with('alert-success','Berhasil mengubah data.');
        }else{
            return redirect('verify')->with('alert','Gagal mengubah data.');
        }
    }

    public function is_doing(Request $request,$id)
    {
        $date_production = Carbon::now()->addDays(1);

        $data = [
            'status' => 1,
            'date_production'   => $date_production,
            'date_finish'       => null
        ];

        $data = Purchase_order::where('id_po',$id)->update($data);

        if($data){
            
            $dss = [
                'id_dss'            => IdGenerator::generate(['table' => 'history_dss','field'=>'id_dss', 'length' => 7, 'prefix' =>'DSS','reset_on_prefix_change'=>'true']),
                'id_po'             => $id,
                'id_user'           => session('id_user'),
                'description_dss'   => 'Purchase Orders will be doing at '.$date_production->format('Y-m-d'),
            ];

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Is doing po '.$id,
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);
            HistoryDss::create($dss);

            return redirect('result')->with('alert-success','Berhasil mengubah data.');
        }else{
            return redirect('result')->with('alert','Gagal mengubah data.');
        }
    }

    public function po_view($id){

         //
        $current_data = Purchase_order::select('*','purchase_orders.id_po as id')
         ->where('purchase_orders.id_po',$id)
         ->leftJoin('customers as c','purchase_orders.id_customer','=','c.id_customer')
         ->get();

        $customers = Customers::get();

        $current_shoes = DB::table('tr_shoes_models as t')
        ->select('*','t.id_tr_shoes as id_tr',DB::raw('COUNT(d.id_do) as amount'))
        ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
        ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
        ->groupBy('t.id_tr_shoes')
        ->orderBy('t.created_at','desc')
        ->where('id_po',$id)
        ->get();

        $shoes = Shoes_model::get();

        $additionals = Additional::select('*')
        ->where('id_po',$id)
        ->get();

        return view('purchase.po_view',['customers' => $customers,'shoes' => $shoes,'additionals' => $additionals,'current_data' => $current_data,'current_shoes' => $current_shoes]);

    }

    public function po_detail($id){

        $shoes_model = Tr_shoes::select('*','tr_shoes_models.id_tr_shoes as id_tr')
                                ->leftJoin('shoes_models','tr_shoes_models.id_shoes_model','=','shoes_models.id_shoes_model')
                                ->where('tr_shoes_models.id_tr_shoes',$id)
                                ->get();

        $detail = Detail::select("*","detail_orders.detail_description as description","detail_orders.id_do as id")
                          ->leftJoin('tr_shoes_models as t', 'detail_orders.id_tr_shoes', '=', 't.id_tr_shoes')
                          ->leftJoin('shoes_models', 't.id_shoes_model', '=', 'shoes_models.id_shoes_model')
                          ->where('detail_orders.id_tr_shoes',$id)
                          ->get();

        return view('detail.detail_view',['detail' => $detail,'shoes_model' => $shoes_model]);

    }
}
