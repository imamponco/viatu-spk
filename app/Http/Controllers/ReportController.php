<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Purchase_order;
use App\Customers;
use App\Shoes_model;
use App\Tr_shoes;
use App\Additional;
use App\Detail;

use PDF;
use Dompdf\Dompdf;
use Dompdf\Options;


class ReportController extends Controller
{
    //
    public function cetak_pdf($id){
        $options = new Options();
        $options->set('isRemoteEnabled', TRUE);
        $dompdf = new Dompdf($options);
        $contxt = stream_context_create([
            'ssl' => [
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed'=> TRUE
            ]
        ]);
        $dompdf->setHttpContext($contxt);

        $current_header = Purchase_order::select('*','purchase_orders.id_po as id','u.name as username','c.customer_name as customer_name')
                                    ->where('purchase_orders.id_po',$id)
                                    ->leftJoin('customers as c','purchase_orders.id_customer','=','c.id_customer')
                                    ->leftJoin('users as u','purchase_orders.id_user','=','u.id_user')
                                    ->get();


        $current_shoes = DB::table('tr_shoes_models as t')
                ->select('*','t.id_tr_shoes as id_tr',DB::raw('COUNT(d.id_do) as unit'))
                ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
                ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                ->groupBy('t.id_tr_shoes')
                ->orderBy('t.created_at','desc')
                ->where('id_po',$id)
                ->get();


        $current_additional = Additional::select('*')
                ->where('id_po',$id)
                ->get();

        $amount_model = count($current_shoes);

        for ($i=0; $i < $amount_model; $i++) {
            $detail_data[$i] = Detail::select('*',DB::raw('COUNT(size) as amount_size'),'detail_orders.detail_description as description')
                                        ->where('t.id_tr_shoes',$current_shoes[$i]->id_tr)
                                        ->where('t.id_shoes_model',$current_shoes[$i]->id_shoes_model)
                                        ->leftJoin('tr_shoes_models as t','detail_orders.id_tr_shoes','=','t.id_tr_shoes')
                                        ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                                        ->groupBy('detail_orders.id_do')
                                        ->orderBy('size','asc')
                                        ->get();
            $id = $detail_data[$i][0]->id_tr_shoes;
            $size[$id] = Detail::select('size',DB::raw('COUNT(id_do) as amount'))->where('id_tr_shoes',$detail_data[$i][0]->id_tr_shoes)->groupBy('size')->get();
        }
        // dd($size,$detail_data);
    	$pdf = PDF::loadview('report.quotation',['current_header'=>$current_header,'current_shoes'=>$current_shoes,'current_additional'=>$current_additional,'detail_data'=>$detail_data,'amount_model'=>$amount_model,'size'=>$size]);
        return $pdf->stream();
        // $date = date('d-m-Y');
        // return $pdf->download('quotation-'.$current_header[0]->name.'-'.$date.'.pdf');
    }
}
