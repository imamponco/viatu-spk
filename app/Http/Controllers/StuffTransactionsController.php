<?php

namespace App\Http\Controllers;

use App\Stuff_transactions;
use App\Inventory;
use App\Activity_log;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Haruncpi\LaravelIdGenerator\IdGenerator;

use Image;


class StuffTransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $stuff = DB::table('stuff_transactions as s')
                    ->select('i.stuff_name','i.stuff_code','s.id_tr','s.stuff_unit','s.inout','s.created_at','s.updated_at','u.name as username')
                    ->leftJoin('inventories as i','s.stuff_code','=','i.stuff_code')
                    ->leftJoin('users as u','s.id_user','=','u.id_user')
                    ->orderBy('s.created_at','desc')
                    ->get();

        return view('stuff.stuff_view',['stuff'=>$stuff]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $inventory = DB::table('inventories as i')
                        ->select('i.stuff_name','i.stuff_code','m.total','i.id_inv','i.stuff_description')
                        ->leftJoin(DB::raw('(SELECT id_tr,stuff_code,SUM(stuff_unit) as total FROM stuff_transactions GROUP BY stuff_code) as m'),'i.stuff_code','=','m.stuff_code')
                        ->orderBy('created_at','desc')
                        ->groupBy('i.stuff_code')
                        ->where('i.stuff_code',$request->stuff_code)
                        ->first();
        //
        $rules = [
            'stuff_code'   => 'required',
            'inout'        => 'required|numeric'
        ];

        $data = [
            'id_tr'          => IdGenerator::generate(['table' => 'stuff_transactions','field'=>'id_tr', 'length' => 7, 'prefix' =>'ST','reset_on_prefix_change'=>'true']),
            'id_user'        => session('id_user'),
            'stuff_code'     => $request->stuff_code,
            'inout'          => $request->inout
        ];

        if($request->inout==1){
            $data += [ 'stuff_unit' => ($request->unit)];
            $rules += [  'unit'         => 'required|integer|not_in:0|regex:/^[1-9]+([0-9][0-9]?)?$/'];
        }else{
            $data += [ 'stuff_unit' => -($request->unit)];
            $rules += [  'unit'         => 'required|integer|not_in:0|regex:/^[1-9]+([0-9][0-9]?)?$/|max:'.$inventory->total.""];
        }

        $validator = Validator::make($request->all(),$rules);
        
        if ($validator->fails()) {
            return  response()->json(['success'=>'Failed to add data','errors'=>$validator->errors()],422);
        }
            
        $data = Stuff_transactions::create($data);      
        
        if($data){

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Create new stuff transaction.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return response()->json(['success'=>'Data is successfully added','data'=> $data]);
        }else{
            return  response()->json(['success'=>'Failed to add data','data'=> $data],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\stuff_transactions  $stuff_transactions
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $stuff = DB::table('stuff_transactions as s')
        ->select('i.stuff_name','i.stuff_code','s.id_tr','s.stuff_unit','s.inout','s.created_at','s.updated_at','u.name as username')
        ->leftJoin('inventories as i','s.stuff_code','=','i.stuff_code')
        ->leftJoin('users as u','s.id_user','=','u.id_user')
        ->where('s.stuff_code',$id)
        ->orderBy('s.created_at','desc')
        ->get();

        return view('stuff.stuff',['stuff'=>$stuff]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\stuff_transactions  $stuff_transactions
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stuff = DB::table('stuff_transactions as s')
        ->select('i.stuff_name','i.stuff_code','s.id_tr','s.stuff_unit','s.inout','s.created_at','s.updated_at','u.name as username')
        ->leftJoin('inventories as i','s.stuff_code','=','i.stuff_code')
        ->leftJoin('users as u','s.id_user','=','u.id_user')
        ->where('s.id_tr',$id)
        ->orderBy('s.created_at','desc')
        ->get()
        ->first();

        if($stuff){
            return response()->json(['success'=>'Data is successfully get','all_data'=> $stuff]);
        }else{
            return  response()->json(['success'=>'Failed to get data','all_data'=> $stuff],422);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\stuff_transactions  $stuff_transactions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $inventory = DB::table('inventories as i')
                        ->select('i.stuff_name','i.stuff_code','m.total','i.id_inv','i.stuff_description','m.id_tr')
                        ->leftJoin(DB::raw('(SELECT id_tr,stuff_code,SUM(stuff_unit) as total FROM stuff_transactions GROUP BY stuff_code) as m'),'i.stuff_code','=','m.stuff_code')
                        ->orderBy('created_at','desc')
                        ->groupBy('i.stuff_code')
                        ->where('i.stuff_code',$request->stuff_code)
                        ->first();

        $current_stuff = Stuff_transactions::where('id_tr',$id)->first();

        $rules = [
            'inout'         => 'required|numeric'

        ];
        
        $data = [
            'id_user'        => session('id_user'),
            'inout'          => $request->inout
        ];
    
        if($request->inout==1){
            if($inventory->total==0){
                $rules += [  'unit'     => 'required|integer|not_in:0|regex:/^[1-9]+([0-9][0-9]?)?$/|min:'.$current_stuff->stuff_unit.""];
            }else{
                $rules += [  'unit'         => 'required|integer|not_in:0|regex:/^[1-9]+([0-9][0-9]?)?$/|min:'.(($inventory->total - $current_stuff->stuff_unit)* -1).""];
            }
            $data += [ 'stuff_unit' => ($request->unit)];
        }else{
            if($inventory->total==0){
               $rules += [  'unit'         => 'required|integer|not_in:0|regex:/^[1-9]+([0-9][0-9]?)?$/|max:'.(-1 * $current_stuff->stuff_unit).""];
            }else{
               $rules += [  'unit'         => 'required|integer|not_in:0|regex:/^[1-9]+([0-9][0-9]?)?$/|max:'.((-1 * $current_stuff->stuff_unit) + $inventory->total).""];
            }
            $data += [ 'stuff_unit' => -($request->unit)];
        }

        

        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return  response()->json(['success'=>'Failed to update data','data'=> $request->all(),'errors'=>$validator->errors()],422);
        }
       
        $data = Stuff_transactions::where('id_tr',$id)->update($data);
      
        if($data){

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Update stuff transaction.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

            return response()->json(['success'=>'Data is successfully updated','data'=> $data]);
        }else{
            return  response()->json(['success'=>'Failed to update data','data'=> $request->all()],422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\stuff_transactions  $stuff_transactions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $data = Stuff_transactions::where('id_tr',$id)->get()->first();

        if($data){

            $data->delete();

            $log = [
                'id_log'        => IdGenerator::generate(['table' => 'activity_logs','field'=>'id_log', 'length' => 7, 'prefix' =>'AL','reset_on_prefix_change'=>'true']),
                'id_user'       => session('id_user'),
                'activity_name' => 'Delete stuff transaction.',
                'ip_address'    => $request->ip()
            ];

            Activity_log::create($log);

             // redirect
            return response()->json(['success'=>'Data has been deleted']);
        }else{
            return  response()->json(['failed'=>'Failed to deleted data','data'=> $data],422);
        }
    }
}
