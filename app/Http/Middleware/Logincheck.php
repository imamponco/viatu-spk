<?php

namespace App\Http\Middleware;

use Closure;

class Logincheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!empty(session('id_user'))) {
            return redirect('dashboard');
        }

        return $next($request);
    }
}
