<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase_order extends Model
{
    protected $table = "purchase_orders";
    protected $primaryKey = "id_po";
    
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'id_po',
        'id_customer',
        'id_user',
        'po_code',
        'date_order',
        'deadline',
        'date_production',
        'date_finish',
        'status',
        'status_verification',
        'resi_code',
        'created_at',
        'updated_at'
    ];
}
