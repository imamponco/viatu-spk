<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image_file extends Model
{
    //
    protected $table = "images";
    protected $primaryKey = "id_images";
    
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'id_images',
        'id_shoes_model',
        'img_path',
        'created_at',
        'updated_at'
    ];
}
