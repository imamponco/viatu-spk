<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Additional extends Model
{
    //
    protected $table = "additional";
    protected $primaryKey = "id_add";
    
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'id_add',
        'id_po',
        'add_name',
        'add_price',
        'add_unit',
        'add_description', 
        'created_at',
        'updated_at'
    ];
}
