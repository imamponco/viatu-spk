<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryDss extends Model
{
    protected $table = "history_dss";
    protected $primaryKey = "id_dss";
    
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'id_dss',
        'id_user',
        'id_po',
        'description_dss',
        'created_at',
        'updated_at'
    ];
}
