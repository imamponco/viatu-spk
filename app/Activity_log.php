<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity_log extends Model
{
    //
    protected $table = "activity_logs";
    protected $primaryKey = "id_log";
    
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'id_log',
        'id_user',
        'activity_name',
        'ip_address',
        'created_at',
        'updated_at'
    ];
}
