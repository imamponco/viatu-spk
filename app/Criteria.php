<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Criteria extends Model
{
    //
    protected $table = "criterias";
    protected $primaryKey = "id_criteria";
    
    public $incrementing = false;
    public $timestamps = true;

    protected $casts = [
        'options' => 'array'
    ];

    protected $fillable = [
        'id_criteria',
        'criteria_code',
        'criteria_name',
        'optimization_direction',
        'options',
        'total_value',
        'eigen_value',
        'total_multiple',
        'result',
        'created_at',
        'updated_at'
    ];

    public function setOptionsAttribute($value)
    {
        
        $this->attributes['options'] = json_encode($value);
    }

}
