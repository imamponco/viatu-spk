<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shoes_model extends Model
{
    //
    protected $table = "shoes_models";
    protected $primaryKey = "id_shoes_model";

    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'id_shoes_model',
        'id_po',
        'model_name',
        'model_code',
        'unit_price',
        'difficulty',
        'shoes_description',
        'deleted_at',
        'created_at',
        'updated_at'
    ];
}
