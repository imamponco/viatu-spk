<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class Db_Backup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This laravel command is used to backup db with csv.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $db_connection = ENV('DB_CONNECTION');
        switch ($db_connection) {
            case 'mysql':
                if ($this->confirm('Data csv before will be replaced when you run this command. Do you wish to continue backup csv?', true)) {
                    //
                    $db_name = ENV('DB_DATABASE');

                    $this->info('Database backup csv by imamponco V.01 db: '.$db_connection.' db_name: '.$db_name);

                    //GET ALL TABLE FROM DB
                    $sql = "SELECT `TABLE_NAME` FROM `INFORMATION_SCHEMA`.`TABLES` WHERE `TABLE_TYPE` = 'BASE TABLE' AND TABLE_SCHEMA='".$db_name."'";
                    $table_name = DB::select(DB::RAW($sql));
                    $path = $this->laravel->databasePath().'/csv';

                    if(!file_exists($path)){
                        mkdir($path, 777, true);
                    }

                    foreach ($table_name as $tb) {
                        //
                        $header = array();

                        $data = DB::select(DB::RAW('SELECT * FROM '.$tb->TABLE_NAME));

                        $data = array_map(function ($value) {
                                return (array)$value;
                            }, $data);

                        $column = " SELECT COLUMN_NAME FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE TABLE_NAME = '".$tb->TABLE_NAME."' AND TABLE_SCHEMA = '".$db_name."'";

                        $column_name = DB::select(DB::RAW($column));

                        foreach ($column_name as $head) {
                            if($head->COLUMN_NAME == 'created_at' || $head->COLUMN_NAME == 'updated_at'){

                            }else{
                                array_push($header,$head->COLUMN_NAME);
                            }
                        }

                        $fp = fopen($path.'/'.$tb->TABLE_NAME.'.csv', 'w');
                        fputcsv($fp,$header);
                        for ($i=0; $i < count($data) ; $i++) {
                                fputcsv($fp, $data[$i]);
                            }
                        fclose($fp);

                        unset($header);

                        $this->info('Table '.$tb->TABLE_NAME.' has been backup!');
                    }
                    $this->info('All data has been saved in. '.$path);
                }
                break;

            default:
                $this->line('Database backup csv for db: '.$db_connection.'not available for now');
                break;
        }

    }
}
