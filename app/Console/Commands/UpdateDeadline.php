<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\Tr_Criteria;
use App\Purchase_order;

class UpdateDeadline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:deadline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This laravel cronjob is used to update value alternative criteria deadline.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $tr = Tr_Criteria::where('criteria_code','WP-01')->get();

        foreach ($tr as $row) {
            if($row->max_value <= 0 && $row->min_value <= 0){
                $update = [
                        'status' => 2,
                ];

                $trans = Tr_Criteria::where('id_trc',$row->id_trc)->delete();
                $po = Purchase_order::where('id_po',$row->id_po)->update($update);
            }else{
                $deadline = Purchase_order::select(DB::raw('DATEDIFF(purchase_orders.deadline,CURDATE()) as deadline'))
                                            ->where('purchase_orders.id_po',$row->id_po)
                                            ->get()
                                            ->first();

                $deadline = $deadline->deadline;

                $this->line('Deadline for PO '.$row->id_po.' is '.$deadline);

                $trans = [
                'min_value'     => $deadline,
                'max_value'     => $deadline
                ];

                $trans = Tr_Criteria::where('id_trc',$row->id_trc)->update($trans);
            }
        }
    }
}
