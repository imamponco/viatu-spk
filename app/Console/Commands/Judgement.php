<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Haruncpi\LaravelIdGenerator\IdGenerator;

use App\Tr_Criteria;
use App\VwConsistency;
use App\Purchase_order;
use App\Criteria;
use App\Customers;
use App\Shoes_model;
use App\Tr_shoes;
use App\Additional;
use App\Image_file;
use App\Activity_log;
use App\Detail;

use Carbon;

class Judgement extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'judgement:do';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Judgement Alternative';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         //
         $consistency = VwConsistency::get();

         $raw          = Tr_Criteria::select('p.id_po as id_po','tr_criterias.id_trc as id','c.criteria_code','c.options',
                                    'p.po_code','tr_criterias.min_value','tr_criterias.max_value','p.status')
                                    ->leftJoin('purchase_orders as p','tr_criterias.id_po','=','p.id_po')
                                    ->leftJoin('criterias as c','tr_criterias.criteria_code','=','c.criteria_code');

        //clear tr
        $clear = Tr_Criteria::select('p.id_po as id_po','tr_criterias.id_trc as id','c.criteria_code','c.options',
                                    'p.po_code','tr_criterias.min_value','tr_criterias.max_value','p.status')
                                    ->leftJoin('purchase_orders as p','tr_criterias.id_po','=','p.id_po')
                                    ->leftJoin('criterias as c','tr_criterias.criteria_code','=','c.criteria_code')
                                    ->where('status','!=',0)
                                    ->delete();

        //truncate tr
        $truncate = Tr_Criteria::where('criteria_code','WP-01')
                                ->orWhere('criteria_code','JMS-01')
                                ->orWhere('criteria_code','JS-01')
                                ->orWhere('criteria_code','TKP-01')
                                ->delete();

        $criterias = Criteria::orderBy('id_criteria','asc')->get();

        $po = Purchase_order::where('status',0)
                            // ->orWhere('status',1)
                            // ->where('status_verification',1)
                            ->orderBy('created_at','asc')->get();


         $current_po = Tr_Criteria::select('id_po')
                            ->groupBy('id_po')
                            ->get();

         $new_po = Purchase_order::whereNotIn('id_po',$current_po)->where('status',0)
                                // ->where('status_verification',1)
                                ->get();

         if(!empty($new_po[0])){
            //create data tr_criteria
            foreach ($new_po as $k) {
                foreach ($criterias as $cri) {

                    switch ($cri->criteria_code) {
                        case 'JMS-01':
                            $count = Tr_shoes::select(DB::Raw('COUNT(*) as jms'))
                                             ->where('id_po',$k->id_po)
                                             ->get()->first();

                            $jms = $count->jms;

                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => $jms,
                                'max_value'     => $jms
                            ];

                            break;
                        case 'JS-01':
                            $js = Detail::select(DB::raw('COUNT(id_do) as js'))
                                          ->leftJoin('tr_shoes_models as t','detail_orders.id_tr_shoes','=','t.id_tr_shoes')
                                          ->where('t.id_po',$k->id_po)
                                          ->first();
                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => $js->js,
                                'max_value'     => $js->js
                            ];
                            break;
                        case 'WP-01':
                            $deadline = Purchase_order::select(DB::raw('DATEDIFF(purchase_orders.deadline,CURDATE()) as deadline'))
                                                        ->where('purchase_orders.id_po',$k->id_po)
                                                        ->get()
                                                        ->first();

                            $deadline = $deadline->deadline;

                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => $deadline,
                                'max_value'     => $deadline
                            ];
                            break;
                        case 'TKP-01':
                            $js = Detail::select(DB::raw('COUNT(id_do) as js'))
                                        ->leftJoin('tr_shoes_models as t','detail_orders.id_tr_shoes','=','t.id_tr_shoes')
                                        ->where('t.id_po',$k->id_po)
                                        ->first();

                            $current_shoes = DB::table('tr_shoes_models as t')
                                                ->select('*','t.id_tr_shoes as id_tr',DB::raw('COUNT(d.id_do) as unit'))
                                                ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
                                                ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                                                ->groupBy('t.id_tr_shoes')
                                                ->orderBy('t.created_at','desc')
                                                ->where('id_po',$k->id_po)
                                                ->get();

                            $amount_shoes = count($current_shoes);

                            $tkp = 0;
                            //count total tingkat kesulitan berdasarkan model sepatu
                            foreach ($current_shoes as $row) {
                                $tkp = $tkp + ($row->difficulty * $row->unit);
                            }

                            if(!empty($js->js)){
                                $tkp = $tkp/$js->js;
                            }else{
                                $tkp = 1;
                            }

                            //RULES TINGKAT KESULITAN

                            if($tkp > 0 && $tkp <= 1){ // 0-1 Mudah
                                $min_value = 1;
                                $max_value = 10;
                            }elseif($tkp >1 && $tkp <= 2){//1-2 Sedang
                                $min_value = 11;
                                $max_value = 20;
                            }elseif($tkp > 2 && $tkp <=3){//2-3 Sulit
                                $min_value = 21;
                                $max_value = 30;
                            }else{
                                $min_value = 21;
                                $max_value = 30;
                            }

                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => $min_value,
                                'max_value'     => $max_value
                            ];
                            break;

                        default:
                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => 0,
                                'max_value'     => 0
                            ];
                            break;
                    }

                    $trans = Tr_Criteria::create($trans);
                }
             }
        }

        $current_criteria = Tr_Criteria::select('criteria_code')
                                        ->groupBy('criteria_code')
                                        ->get();

        $new_criteria = Criteria::whereNotIn('criteria_code',$current_criteria)->get();

        if(!empty($new_criteria[0])){
             //create data tr_criteria
             foreach ($po as $k) {
                foreach ($new_criteria as $cri) {

                    switch ($cri->criteria_code) {
                        case 'JMS-01':
                            $count = Tr_shoes::select(DB::Raw('COUNT(*) as jms'))
                                             ->where('id_po',$k->id_po)
                                             ->get()->first();

                            $jms = $count->jms;

                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => $jms,
                                'max_value'     => $jms
                            ];

                            break;
                        case 'JS-01':
                            $js = Detail::select(DB::raw('COUNT(id_do) as js'))
                            ->leftJoin('tr_shoes_models as t','detail_orders.id_tr_shoes','=','t.id_tr_shoes')
                            ->where('t.id_po',$k->id_po)
                            ->first();
                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => $js->js,
                                'max_value'     => $js->js
                            ];
                            break;
                        case 'WP-01':
                            $deadline = Purchase_order::select(DB::raw('DATEDIFF(purchase_orders.deadline,CURDATE()) as deadline'))
                                                        ->where('purchase_orders.id_po',$k->id_po)
                                                        ->get()
                                                        ->first();

                            $deadline = $deadline->deadline;

                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => $deadline,
                                'max_value'     => $deadline
                            ];
                            break;
                        case 'TKP-01':
                        $js = Detail::select(DB::raw('COUNT(id_do) as js'))
                                    ->leftJoin('tr_shoes_models as t','detail_orders.id_tr_shoes','=','t.id_tr_shoes')
                                    ->where('t.id_po',$k->id_po)
                                    ->first();

                        $current_shoes = DB::table('tr_shoes_models as t')
                                            ->select('*','t.id_tr_shoes as id_tr',DB::raw('COUNT(d.id_do) as unit'))
                                            ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
                                            ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                                            ->groupBy('t.id_tr_shoes')
                                            ->orderBy('t.created_at','desc')
                                            ->where('id_po',$k->id_po)
                                            ->get();

                        $amount_shoes = count($current_shoes);

                        $tkp = 0;
                        //count total tingkat kesulitan berdasarkan model sepatu
                        foreach ($current_shoes as $row) {
                            $tkp = $tkp + ($row->difficulty * $row->unit);
                        }

                        $tkp = $tkp/$js->js;

                        //RULES TINGKAT KESULITAN
                        if($tkp > 0 && $tkp <= 1){ // 0-1 Mudah
                            $min_value = 1;
                            $max_value = 10;
                        }elseif($tkp >1 && $tkp <= 2){//1-2 Sedang
                            $min_value = 11;
                            $max_value = 20;
                        }elseif($tkp > 2 && $tkp <=3){//2-3 Sulit
                            $min_value = 21;
                            $max_value = 30;
                        }else{
                            $min_value = 21;
                            $max_value = 30;
                        }

                        $trans = [
                            'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                            'id_po'         => $k->id_po,
                            'criteria_code' => $cri->criteria_code,
                            'min_value'     => $min_value,
                            'max_value'     => $max_value
                        ];
                        break;

                        default:
                            $trans = [
                                'id_trc'        => IdGenerator::generate(['table' => 'tr_criterias','field'=>'id_trc', 'length' => 7, 'prefix' =>'TRC','reset_on_prefix_change'=>'true']),
                                'id_po'         => $k->id_po,
                                'criteria_code' => $cri->criteria_code,
                                'min_value'     => 0,
                                'max_value'     => 0
                            ];
                            break;
                    }

                    $trans = Tr_Criteria::create($trans);
                }
             }

        }

        $tr = $raw->where('status',0)
            // ->orWhere('status',1)
            //  ->where('status_verification',1)
             ->orderBy('c.id_criteria','asc')->get();
    }
}
