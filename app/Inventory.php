<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    //
    protected $table = "inventories";
    protected $primaryKey = "id_inv";
    
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'id_inv',
        'id_user',
        'stuff_name',
        'stuff_code',
        'stuff_description'
    ];
}
