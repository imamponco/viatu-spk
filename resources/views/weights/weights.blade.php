@extends('backend.dashboard')
@section('title','Viatushop - AHP')
@section('content')
          
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">@yield('title')</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered display" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    
                    <tr>
                      <th>No</th>
                      <th>Kriteria</th>
                      @foreach($criterias as $row)
                      <th>{{ $row->criteria_name }}</th>
                      @endforeach
                    </tr>
                  </thead>
                  <tbody>
                   <?php $count=1; $limit = count($criterias); $jmlr=0;?>
                    @foreach($criterias as $row)
                      <tr id="row-{{ $row->id_criteria }}">
                        <th>{{ $count }} </th>
                        <th>{{ $row->criteria_name }}</th>
                      </tr>
                      <?php $count++; ?>
                    @endforeach
                  </tbody>
                 
                </table>
              </div>
            </div>
          </div>
          
          <!-- Matrix Nilai Kriteria -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Matrix Nilai Kriteria </h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered display" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                
                    <tr>
                      <th>No</th>
                      <th>Kriteria</th>
                      @foreach($criterias as $row)
                      <th>{{ $row->criteria_name }}</th>
                      @endforeach
                      <th>Jumlah</th>
                      <th>Prioritas</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php 
                    $count=1; 
                    $sum=0;
                    $bagi=0;
                    $total_eig=0;
                    $semua = array_sum($matrix);
                    $semua =  number_format($semua, 2, '.', '');
                    $limit = count($criterias); 
                    $total = count($weights);
                   ?>

                    @foreach($criterias as $row)
                  
                      <tr id="mtx-{{ $count }}">
                        <th>{{ $count }} </th>
                        <th>{{ $row->criteria_name }}</th>
                      </tr>
  
                    <?php $count++; ?>
                    @endforeach
                  </tbody>
                    <tr id="last">
                      <th>TOTAL</th>
                      <td></td>

                    </tr>
                 
                </table>
              </div>
            </div>
          </div>

          <!-- Matrix Penjumlahan Tiap Baris -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Matrix Penjumlahan Tiap Baris </h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered display" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                
                    <tr>
                      <th>No</th>
                      <th>Kriteria</th>
                      @foreach($criterias as $row)
                      <th>{{ $row->criteria_name }}</th>
                      @endforeach
                      <th>Prioritas</th>
                      <th>Jumlah Per Baris</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php
                        $count=1;
                    @endphp
                    @foreach($criterias as $row)
                      <tr id="mpt-{{ $row->id_criteria }}">
                        <th>{{ $count }} </th>
                        <th>{{ $row->criteria_name }}</th>
                      </tr>
                    @php
                        $count++;
                    @endphp
                    @endforeach
                  </tbody>
                 
                </table>
              </div>
            </div>
          </div>

          <!-- Matrix Hitung Ratio Konsistensi  -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Hitung Ratio Konsistensi</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered display" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                
                    <tr>
                      <th>No</th>
                      <th>Kriteria</th>
                      <th>Prioritas</th>
                      <th>Jumlah Per Baris</th>
                      <th>Hasil</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php
                      $count=1;
                    @endphp
                    @foreach($criterias as $row)

                      <tr id="hrk-{{ $row->id_criteria }}">
                        <th>{{ $count }}</th>
                        <th>{{ $row->criteria_name }}</th>
                        <td>{{ $row->eigen_value }}</td>
                        <td>{{ $row->total_multiple }}</td>
                      </tr>
                    
                    @php
                      $count++;
                    @endphp
                    @endforeach

                  </tbody>
                    <tr id="last-2">
                      <th>JUMLAH RATIO</th>
                    </tr>
                 
                </table>
              </div>
            </div>
          </div>

          <!-- Matrix Index Random Table  -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Index Random Table</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered"  width="100%" cellspacing="0">
                  <tbody>
                  <tr id="index">
                
                  </tr>
                  <tr id="nilai">
                    
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <!-- Matrix Perhitungan Akhir Kriteria  -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Perhitungan Akhir Kriteria </h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered"  width="100%" cellspacing="0">
                  <tbody>
                  <tr id="jmlr">
                   <th>Jumlah Ratio</th>
                  </tr>
                  <tr>
                    <th>n Kriteria</th>
                    <td>{{ $limit }}</td>
                  </tr>
                  <tr id="lamda-max">
                    <th>Lamda Max (Jumlah Ratio / n Kriteria)</th>
                  </tr>
                  <tr id="ci">
                    <th>CI (Lamda Max - n Kriteria / n Kriteria - 1)</th>
                  
                  </tr>
                  <tr id="cr">
                    <th>CR (CI/IR) (* IF CR < 0,1 Consistency Acceptted )</th>
                  </tr>
                  <tr id="consistency">
                    <th>Consistency Status</th>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <script>
              
              //Show weight value
              @foreach($weights as $col)

                @if($col->criteria_col > $col->criteria_row)
                  var content = "<td><input type='text' onchange='update_weight(this.id)' id='weight-{{ $col->id_weight }}-{{ $col->criteria_row }}-{{ $col->criteria_col }}' value='{{ $col->weight_value }}'></td>"
                @else
                  var content = "<td><input type='text' disabled='disabled' id='weight-{{ $col->id_weight }}-{{ $col->criteria_row }}-{{ $col->criteria_col }}' value='{{ $col->weight_value }}'></td>"
                @endif
          
                
                $('#row-'+'{{ $col->criteria_row }}').append(content)
              
              @endforeach

              <?php $count = 1; //dd($matrix); ?>
              // Show Matrix Nilai Kriteria
              @for($i=0; $i<$total; $i++)
                  var content = "<td>{{ $matrix[$i] }}</td>"

                      $('#mtx-'+{{ $count }}).append(content)
                      <?php $sum += $matrix[$i];?>

                    <?php

                    $check = $i % $limit;
                    if($check == ($limit-1) && $i != 0){
                      $sum = number_format($sum, 2, '.', '');
                      $eig  = (float) $sum / $semua;
                      $eig = number_format($eig, 3, '.', '');
                      $total_eig = (float) $total_eig + $eig;
                      $total_eig = number_format($total_eig, 3, '.', '');
                    ?>
                    
                    var sum = "<td id='sum-{{$count}}'>{{ $sum }}</td>";
                    var eig = "<td id='eig-{{$count}}'>{{ $eig }}</td>"
                    var semua = "<td>{{ $semua }}</td>";
                    var td = "<td></td>"
                    var total_eig = "<td>{{ $total_eig }}</td>";
                   
                      $('#mtx-'+{{ $count }}).append(sum)
                      $('#mtx-'+{{ $count }}).append(eig)
                      $('#last').append(td);
        
                    <?php
                      $sum=0;
                      $count++;
                    }
                    ?> 
              @endfor
              $('#last').append(semua);
              $('#last').append(total_eig);
            
            // Show Matrix Penjumlahan Tiap Baris
            <?php $hitung=1; $hitung2=0; $total_multiple=0; $limit=count($criterias); ?>
              @foreach($weights as $col)
            
              var content = "<td>{{ $col->weight_value }} * {{ $criterias[($hitung-1)]->eigen_value }}</td>"
              var priority = "<td>{{ $criterias[($hitung2)]->eigen_value }}</td>"
            
              <?php $total_multiple += ($col->weight_value * $criterias[($hitung-1)]->eigen_value); ?>
               
                $('#mpt-'+'{{ $col->criteria_row }}').append(content)
                
                @php                    
                   if($hitung==$limit){
                        $hitung=0;
                    }
                   $check = $hitung % $limit;
                   
                   if($check == 0 && $hitung != 1){
                   $total_multiple = round($total_multiple,3);
                @endphp

                    var total   = "<td>{{ $total_multiple }}</td>"
                    
                  
                    
                    $('#mpt-'+'{{ $col->criteria_row }}').append(priority)
                    $('#mpt-'+'{{ $col->criteria_row }}').append(total)
                
               @php
                  $total_multiple = 0;
                  $hitung2++;
                  }
                  $hitung++;
                @endphp
              @endforeach

              //Show Hitung Ratio Konsistensi
              <?php $count=1; ?>
              var td     = "<td></td><td></td><td></td>";
              @foreach($criterias as $row)
                var result = "<td>{{ $row->result }}</td>";
               
                <?php $jmlr += $row->result; ?>
                var jmlr   = "<td>{{ $jmlr }}</td>";

                  $('#hrk-'+'{{ $row->id_criteria }}').append(result)
                  
                 
                @if($count < count($criterias)-1)
                  
                @endif
                <?php $count++; ?>
        
              @endforeach
              $('#last-2').append(td)
              $('#last-2').append(jmlr)

              //show
              var ir  = [0.00, 0.00, 0.52, 0.89, 1.11, 1.25,1.35,1.40,1.45,1.49,1.52,1.54,1.56,1.58,1.59];
              for (let a = 0; a < ir.length; a++) {
                var index = "<th>"+(a+1)+"</th>";
                var nilai = "<td>"+ir[a]+"</td>";
                $('#index').append(index);
                $('#nilai').append(nilai);
              }
              <?php $limit = count($criterias); ?>
              //show perhitungan akhir criteria
              var jml = {{ $jmlr }};
              var limit = {{ $limit }};
              var lamda = jml / limit;
              var ci = (lamda-limit)/(limit-1);
              var cr = (ci/ir[limit-1]);
              var check = {{ (empty($weight_value[0]->count) ? 'true' : 'false' ) }};
            
              if (cr < 0.1 && check) {
                var status = "Accepted";
              }else{
                var cr = 'Undifined';
                var status = "Declined";
              }
              
              var consistency = "<td>"+status+"</td>";
              var content_lamda = "<td>"+lamda+"</td>";
              var content_ci = "<td>"+ci+"</td>";
              var content_cr = "<td>"+cr+"</td>";

              $('#jmlr').append(jmlr)
              $('#lamda-max').append(content_lamda)
              $('#ci').append(content_ci)
              $('#cr').append(content_cr)
              $('#consistency').append(consistency)
              
             

          </script>
      @endsection