  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@yield('title')</title>

  <!-- Favicon -->
  <link rel="icon" type="image/png" href="{{ asset('images/viatu.png') }}"/>

  <!-- Custom fonts for this template -->
  <link href="{{ asset(url('admin/vendor/fontawesome-free/css/all.min.css')) }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{ asset(url('admin/css/sb-admin-2.min.css')) }}" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="{{ asset(url('admin/vendor/datatables/dataTables.bootstrap4.min.css')) }}" rel="stylesheet">

  <!-- CK EDITOR -->
  <script src="{{ asset(url('ckeditor/ckeditor.js')) }}"></script>
  
  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset(url('admin/vendor/jquery/jquery.min.js')) }}"></script>
  <script src="{{ asset(url('admin/vendor/bootstrap/js/bootstrap.bundle.min.js')) }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset(url('admin/vendor/jquery-easing/jquery.easing.min.js')) }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset(url('admin/js/sb-admin-2.min.js')) }}"></script>

  <!-- Page level plugins -->
  <script src="{{ asset(url('admin/vendor/datatables/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(url('admin/vendor/datatables/dataTables.bootstrap4.min.js')) }}"></script>

  <!-- Page level custom scripts -->
  <script src="{{ asset(url('admin/js/demo/datatables-demo.js')) }}"></script>
  <!-- Lodash -->
  <script src='https://cdn.jsdelivr.net/g/lodash@4(lodash.min.js+lodash.fp.min.js)'></script>
  <!-- Page level plugins -->
  <script src="{{ asset('admin/vendor/chart.js/Chart.min.js') }}"></script>