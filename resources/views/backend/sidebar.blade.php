  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('dashboard') }}">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-shoe-prints"></i>
        </div>
        <div class="sidebar-brand-text mx-3"> Viatu <sup>Shop</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="{{ url('dashboard') }}">
          <i class="fas fa-fw fa-tachometer-alt "></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Control
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-table"></i>
          <span>Table</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">All Data Table:</h6>

            @switch(session('role'))

              @case('Manager Production')
                <a class="collapse-item" href="{{ url('criteria') }}">Criteria</a>
                @break

              {{-- @case('Manager Marketing')
                <a class="collapse-item" href="{{ url('verify') }}">PO Verification</a>
                @break --}}

              @case('Marketing')
                <a class="collapse-item" href="{{ url('customers') }}">Customers</a>
                <a class="collapse-item" href="{{ url('shoe') }}">Shoes Model</a>
                @break

              @case('Administrator')
                <a class="collapse-item" href="{{ url('quality') }}">Quality Report</a>
                <a class="collapse-item" href="{{ url('do') }}">Delivery Order</a>
                @break

              @case('Warehouse')
                <a class="collapse-item" href="{{ url('inventory') }}">Inventory</a>
                @break

            @endswitch
          </div>
        </div>
      </li>

      @section('transactions')
      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Transactions</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">All Transaction Table:</h6>
        @endsection

            @switch(session('role'))

            @case('Manager Production')
            @yield('transactions')
            <span class="collapse-item" id="weight">Weights</span>
            <span class="collapse-item" id="judgement" onClick="judgement()">Judgement Alternative</span>
            <span class="collapse-item" id="priority">Priority Scale</span>
                  </div>
                </div>
              </li>
              @break

            @case('Marketing')
            @yield('transactions')
              <a class="collapse-item" href="{{ url('po') }}">Purchase Order</a>
                  </div>
                </div>
              </li>
              @break

            @case('Warehouse')
              @yield('transactions')
              <a class="collapse-item" href="{{ url('stuff') }}">Stuff Transactions</a>
                  </div>
                </div>
              </li>
              @break

          @endswitch

          @if(session('role')=='Manager Production')
          <!-- Nav Item - Utilities Collapse Menu -->
          <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              <i class="fas fa-fw fa-wrench"></i>
              <span>Settings</span>
            </a>
            <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
              <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Settings:</h6>
                <a class="collapse-item" href="{{ url('users') }}">User</a>
              </div>
            </div>
          </li>
          @endif

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
