<!DOCTYPE html>
<html lang="en">
<head>
    @include('backend.header')
</head>
    <body id="page-top">
        @include('backend.sidebar')
        @include('backend.nav')
    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">

         <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Dashboard</h1>

            @yield('content')

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->
    @include('backend.footer')

    @include('js')
    </body>
</html>