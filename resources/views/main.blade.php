 @extends('backend.dashboard')
 @section('title','Viatushop Indonesia')
 @section('content')
@php
  use App\Purchase_order;
  use App\Tr_Criteria;
  use App\Criteria;
  use App\Activity_log;

  use Illuminate\Support\Facades\DB;

//   if(session('role')=="Manager Marketing"){
//     $new_po = Activity_log::select('*')->where('read',0)->where('activity_name','like','%Managing po%')->groupBy('activity_name')->get();
//   }

  if(session('role')=="Manager Production"){
    $new_verified = Activity_log::select('*')->where('read',0)->where('activity_name','like','%New po%')->groupBy('activity_name')->get();
  }

  $current_po = Purchase_order::select(DB::raw('COUNT(id_po) as jumlah'))->where('status',1)->get();
  
  $done = Purchase_order::select(DB::raw('COUNT(id_do) as jumlah'))
                          ->leftJoin('tr_shoes_models as tr','purchase_orders.id_po','=','tr.id_po')
                          ->leftJoin('detail_orders as d','tr.id_tr_shoes','=','d.id_tr_shoes')
                          ->where('status',1)
                        //   ->where('status_verification',1)
                          ->get();

  $criteria = Criteria::select(DB::raw('COUNT(id_criteria) as jumlah'))->get();

  $tr = Tr_Criteria::select('tr_criterias.id_po')
                    ->groupBy('tr_criterias.id_po')
                    ->get();
@endphp


        <!-- Page Heading -->

        <!-- Content Row -->
        <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Purchase Order</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">{{ ($current_po[0]->jumlah == null ? "0" : $current_po[0]->jumlah) }}</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Unit Handling</div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{ ($done[0]->jumlah == null ? "0" : $done[0]->jumlah) }}</div>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fa fa-tasks fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Alternative</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">{{ (count($tr) == 0 ? "0" : count($tr)) }}</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>


            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Criteria</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">{{ ($criteria[0]->jumlah == null ? "0" : $criteria[0]->jumlah) }}</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-user-check fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-12 mb-4">
            <!-- Illustrations -->
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Introduction</h6>
              </div>
              <div class="card-body">
                <div class="text-center">
                  <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="{{ asset('admin/img/undraw_business_decisions_gjwy.svg') }}" alt="">
                </div>
                <p>Selamat datang di Sistem Informasi Penentuan Skala Prioritas Produksi Sepatu Viatu Shop Indonesia!
                Sistem informasi ini akan membantu anda dalam menentukan skala prioritas produksi sepatu dengan menggunakan metode Analytical Hierarchy Processing (AHP)
                dan Complex Proportional Assessment of alternatives with Grey relations (COPRAS-G).</p>
              </div>
            </div>
          </div>
@endsection
