@extends('backend.dashboard')
@section('title','Viatushop - Users')
@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-6">Form Data Users</h1>
              </div>
              @if(\Session::has('alert-success'))
                  <div class="alert alert-success" style="padding: 2px;">
                      <div>{{Session::get('alert-success')}}</div>
                  </div>
              @endif
              <form class="user" enctype="multipart/form-data" action="{{ url('users') }}" method="post">
                @csrf
                 <div class="form-group">
                  <label for="name_label">Name</label>
                  @if($errors->has('name'))
                      <div class="text-left" style="padding: 2px; color:red;">{{ $errors->first('name') }}</div>
                  @endif
                  <input id="name_label" type="text" name="name" value="{{ old('name') }}" class="form-control"  placeholder="Enter name">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  @if($errors->has('email'))
                      <div class="text-left" style="padding: 2px; color:red;">{{ $errors->first('email') }}</div>
                  @endif
                  <input type="text" name="email" value="{{ old('email') }}" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                 <div class="form-group">
                  <label for="name_label">Phone</label>
                  @if($errors->has('phone_number'))
                      <div class="text-left" style="padding: 2px; color:red;">{{ $errors->first('phone_number') }}</div>
                  @endif
                  <input id="name_label" type="text" name="phone_number" value="{{ old('phone_number') }}" class="form-control"  placeholder="Enter phone number">
                </div>
                <div class="form-group row">

                  <div class="col-6">
                    <label for="exampleInputPassword1">Password</label>
                    @if($errors->has('password'))
                      <div class="text-left" style="padding: 2px; color:red;">{{ $errors->first('password') }}</div>
                    @endif
                    <input type="password" name="password" class="form-control" placeholder="Password">
                  </div>
                  <div class="col-6">
                    <label for="exampleInputPassword1">Re-Password</label>
                    @if($errors->has('repassword'))
                      <div class="text-left" style="padding: 2px; color:red;">{{ $errors->first('repassword') }}</div>
                    @endif
                    <input type="password" name="repassword" class="form-control" placeholder="Re-Password">
                  </div>
                </div>
                 <div class="form-group">
                  <div class="input-group-prepend">
                    <label for="inputGroupSelect01">Role</label>
                  </div>
                  @if($errors->has('role'))
                    <div class="text-left" style="padding: 2px; color:red;">{{ $errors->first('role') }}</div>
                  @endif
                  <select class="custom-select" id="inputGroupSelect01" name="role">
                    <option value="" selected>Choose...</option>
                    <option value="Manager Production">Manager Production</option>
                    {{-- <option value="Manager Marketing">Manager Marketing</option> --}}
                    <option value="Administrator">Administrator</option>
                    <option value="Marketing">Marketing</option>
                    <option value="Warehouse">Warehouse</option>
                  </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
