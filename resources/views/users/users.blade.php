@extends('backend.dashboard')
@section('title','Viatushop - Users')
@section('content')
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">@yield('title')</h6>
            </div>
            <div class="card-body">
               <a href="{{ url('users/create') }}" class="btn btn-success btn-square" style="margin-bottom: 20px;">
                  <i class="fas fa-plus"></i>
               </a>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                
                    <tr>
                      <th>No</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Role</th> 
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
                    @foreach($users as $row)
                    <tr>
                    
                      <td>{{ $no }}</td>
                      <td>{{ $row->name }}</td>
                      <td>{{ $row->email }}</td>
                      <td>{{ $row->phone_number }}</td>
                      <td>{{ $row->role }}</td>
                      <td>
                        <a href="{{ URL::to('users/' . $row->id_user . '/edit') }}" class="btn btn-info btn-circle">
                          <i class="fas fa-edit"></i>
                        </a>
                        @if(session('id_user') != $row->id)
                        <form action="{{ URL::to('users/'. $row->id_user) }}" method="post" style="float: right;">
                          @method('DELETE')
                          @csrf                   
                          <button class="btn btn-danger btn-circle" onclick="return confirm('Are you sure want to delete this data?')"><i class=" fas fa-trash"></i></button>
                        </form>
                        @endif
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                  </tbody>
                 
                </table>
              </div>
            </div>
          </div>
      @endsection