@extends('backend.dashboard')
@section('title','Viatushop - Stuff Transactions')
@section('content')

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">@yield('title') {{ (!empty($stuff[0]->stuff_name) == true ? $stuff[0]->stuff_name  : "" ) }}</h6>
            </div>
            <div class="card-body">
               <a href="#" class="btn btn-success btn-square" data-toggle="modal" data-target="#stuffTr" data-whatever="#" style="margin-bottom: 20px;">
                  <i class="fas fa-plus"></i>
               </a>
               <div class="d-sm-flex align-items-center justify-content-between mb-4 float-right">
                <a href="{{ url()->previous() }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa fa-arrow-left fa-sm text-white"></i></a>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                
                    <tr>
                      <th>No</th>
                      <th>Stuff Name</th>
                      <th>Stuff Code</th>
                      <th>Unit</th>
                      <th>Inout</th>
                      <th>Handler</th>
                      <th>Created At</th>
                      <th>Updated At</th>
                      <th width="120.05px">Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php $no = 1; ?>
                    @foreach($stuff as $row)
                    <tr>
                    
                      <td>{{ $no }}</td>
                      <td>{{ $row->stuff_name }}</td>
                      <td>{{ $row->stuff_code }}</td>
                      <td>{{ $row->stuff_unit }}</td>
                      <td>{{ ($row->inout == 1 ? "In" : "Out") }}</td>
                      <td>{{ $row->username }}</td>
                      <td>{{ $row->created_at }}</td>
                      <td>{{ $row->updated_at }}</td>
                      <td>
                      <div class="row">
                          <a href="#" class="btn btn-info btn-circle ml-4" id="id-stuff_{{ $row->id_tr }}_{{ Request()->stuff }}" onclick="editStuff(this.id)" data-toggle="modal" data-target="#editStuff" data-whatever="#">
                            <i class="fas fa-edit"></i>
                          </a>                  
                          <a href="#" id="{{ $row->id_tr }}" name="stuff" class="btn btn-danger btn-circle ml-2" onclick="delete_data(this.id,this.name)">
                            <i class=" fas fa-trash"></i>
                          </a>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                  </tbody>
                 
                </table>
              </div>
            </div>
          </div>

          <!-- Add Modal -->
          <div class="modal fade bd-example-modal-lg" id="stuffTr" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Add Stuff Transactions</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form class="add" action="#" id="stuff_form">
                    <div class="form-group">
                      <label for="name" class="col-form-label">Inout:</label>
                      <div class="text-left" id="error_inout" style="padding: 2px; color:red;"></div>
                      <select class="custom-select" id="inout" name="inout">
                        <option selected value="">Choose...</option>
                        <option value="1">In</option>
                        <option value="0">Out</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="name" class="col-form-label">Unit:</label>
                      <div class="text-left" id="error_unit" style="padding: 2px; color:red;"></div>
                      <input type="text" class="form-control" id="unit">
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="stuffSave" onclick="stuff_save(this.name)" name="{{ Request()->stuff }}">Save</button>
                </div>
              </div>
            </div>
          </div>

          <!-- Edit Modal -->
          <div class="modal fade bd-example-modal-lg" id="editStuff" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Edit Stuff Transactions</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                <form class="add" action="#" id="stuff_form">
                  <input type="hidden" value="" id="stuff_code_edit">
                    <div class="form-group">
                      <label for="name" class="col-form-label">Inout:</label>
                      <div class="text-left" id="error_u_inout" style="padding: 2px; color:red;"></div>
                      <select class="custom-select" id="u_inout" name="inout">
                        <option selected value="">Choose...</option>
                        <option value="1">In</option>
                        <option value="0">Out</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="name" class="col-form-label">Unit:</label>
                      <div class="text-left" id="error_u_unit" style="padding: 2px; color:red;"></div>
                      <input type="text" class="form-control" id="u_unit" value="">
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="stuffUpdate" onclick="stuff_update(this.name)">Update</button>
                </div>
              </div>
            </div>
          </div>

      @endsection