@extends('backend.dashboard')
@section('title','Viatushop - Stuff Transactions')
@section('content')

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">@yield('title') </h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
        
            <tr>
              <th>No</th>
              <th>Stuff Name</th>
              <th>Stuff Code</th>
              <th>Unit</th>
              <th>Inout</th>
              <th>Handler</th>
              <th>Created At</th>
              <th>Updated At</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>

            <?php $no = 1; ?>
            @foreach($stuff as $row)
            <tr>
            
              <td>{{ $no }}</td>
              <td>{{ $row->stuff_name }}</td>
              <td>{{ $row->stuff_code }}</td>
              <td>{{ $row->stuff_unit }}</td>
              <td>{{ ($row->inout == 1 ? "In" : "Out") }}</td>
              <td>{{ $row->username }}</td>
              <td>{{ $row->created_at }}</td>
              <td>{{ $row->updated_at }}</td>
              <td>
              <div class="row">
                  <a href="{{ url('stuff/'.$row->stuff_code) }}" class="btn btn-info btn-circle ml-4 mr-2">
                    <i class="fas fa-eye"></i>
                  </a>                  
              </td>
            </tr>
            <?php $no++; ?>
            @endforeach
          </tbody>
         
        </table>
      </div>
    </div>
  </div>
  
@endsection