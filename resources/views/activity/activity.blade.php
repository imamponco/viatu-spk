@extends('backend.dashboard')
@section('title','Viatushop - Activity Log')
@section('content')
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">@yield('title')</h6>
            </div>
            <div class="card-body">
              <div class="float-left">
                <a href="#" id="delete_all" class="btn btn-danger btn-icon-split " style="margin-bottom: 20px;">
                  <span class="icon text-white-50">
                    <i class="fas fa-trash"></i>
                  </span>
                  <span class="text">Clear Activity Log</span>
                </a>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                
                    <tr>
                      <th>No</th>
                      <th>Name</th>
                      <th>Activity Log</th>
                      <th>Ip Address</th>
                      <th>Date</th> 
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
                    @foreach($activity as $row)
                    <tr>
                    
                      <td>{{ $no }}</td>
                      <td>{{ $row->name }}</td>
                      <td>{{ $row->activity_name }}</td>
                      <td>{{ $row->ip_address }}</td>
                      <td>{{ $row->created_at }}</td>
                      <td>
                        <a href="#" id="{{ $row->id }}" name="activity" class="btn btn-danger btn-circle" onclick="delete_data(this.id,this.name)">
                          <i class=" fas fa-trash"></i>
                        </a>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                  </tbody>
                 
                </table>
              </div>
            </div>
          </div>

      <script> 
       
      </script>
      @endsection