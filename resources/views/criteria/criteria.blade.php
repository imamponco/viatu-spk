@extends('backend.dashboard')
@section('title','Viatushop - Criteria')
@section('content')
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">@yield('title')</h6>
            </div>
            <div class="card-body">
               <a href="#" class="btn btn-success btn-square" data-toggle="modal" data-target="#addCriteria" data-whatever="#" style="margin-bottom: 20px;">
                  <i class="fas fa-plus"></i>
               </a>
              
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                
                    <tr>
                      <th>No</th>
                      <th>Criteria Name</th>
                      <th>Criteria Code</th>
                      <th>Optimization Direction</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="newdata">

                    <?php $no = 1; ?>
                    @foreach($criterias as $row)
                    <tr>
                      <td>{{ $no }}</td>
                      <td>{{ $row->criteria_name }}</td>
                      <td>{{ $row->criteria_code }}</td>
                      <td>{{ ($row->optimization_direction == 1 ? "Max" : "Min" )  }}</td>
                      <td>
                      <div class="row">
                        <a href="#" class="btn btn-info btn-circle ml-1 mr-1" id="id_criteria-{{ $row->id_criteria }}" onclick="editCriteria(this.id)" data-toggle="modal" data-target="#editCriteria" data-whatever="#">
                          <i class="fas fa-edit"></i>
                        </a>
                        <form action="{{ URL::to('criteria/'. $row->id_criteria) }}" method="post" style="float: left;">
                          @method('DELETE')
                          @csrf                   
                          <button class="btn btn-danger btn-circle" onclick="return confirm('Are you sure want to delete this data?')"><i class=" fas fa-trash"></i></button>
                        </form>
                        </div>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                  </tbody>
                 
                </table>
              </div>
            </div>
          </div>

          <!-- Add Modal -->
          <div class="modal fade bd-example-modal-lg" id="addCriteria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Add Criteria</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form class="add" action="#">
                    @method('POST}')
                    @csrf 
                    <div class="form-group">
                      <label for="criteria_name" class="col-form-label">Criteria Name:</label>
                      <div class="text-left" id="error_criteria_name" style="padding: 2px; color:red;"></div>
                      <input type="text" class="form-control" id="criteria_name">
                    </div>
                    <div class="form-group">
                      <label for="criteria_code" class="col-form-label">Criteria Code:</label>
                      <div class="text-left" id="error_criteria_code" style="padding: 2px; color:red;"></div>
                      <input type="text" class="form-control" id="criteria_code">
                    </div>
                    <div class="form-group">
                      <label for="opt" class="col-form-label">Optimization Direction:</label>
                      <div class="text-left" id="error_opt" style="padding: 2px; color:red;"></div>
                      <select class="custom-select" id="opt" name="opt">
                        <option selected value="{{ old('opt') }}">{{ !empty(old('opt')) ? (old('opt') == 1 ? "Max" : "Min" ) : "Choose..." }}</option>
                        <option value="1">Max</option>
                        <option value="0">Min</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="description" class="col-form-label">Options:</label>
                      <div class="text-left" id="error_options" style="padding: 2px; color:red;"></div>
                        <select class="custom-select" id="options" name="options">
                          <option selected value="">Choose</option>
                          <option value="1">Text</option>
                          <option value="2">Number</option>
                          <option value="3">Select Box</option>
                        </select>
                    </div>
                    <div class="form-group" id="load_option" hidden="true">
                      <button id="tambah_option" class="btn btn-primary">Add option</button><br/><br/>
                      <div id="select_option"></div>
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="criteriaSave">Save</button>
                </div>
              </div>
            </div>
          </div>

          <!-- Edit Modal -->
          <div class="modal fade bd-example-modal-lg" id="editCriteria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Edit Criteria</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form action="#">
                     <div class="form-group">
                      <label for="criteria_name" class="col-form-label">Criteria Name:</label>
                      <div class="text-left" id="error_u_criteria_name" style="padding: 2px; color:red;"></div>
                      <input type="text" name="criteria_name" class="form-control" id="criteria_name_e" value="">
                    </div>
                    <div class="form-group">
                      <label for="criteria_code" class="col-form-label">Criteria Code:</label>
                      <div class="text-left" id="error_u_criteria_code" style="padding: 2px; color:red;"></div>
                      <input type="text" name="criteria_code" class="form-control" id="criteria_code_e" value="">
                    </div>
                    <div class="form-group">
                      <div class="text-left" id="error_u_opt" style="padding: 2px; color:red;"></div>
                      <label for="opt" class="col-form-label">Optimization Direction:</label>
                      <select class="custom-select" id="opt_e" name="opt">
                        <option selected value="">Choose</option>
                        <option value="1">Max</option>
                        <option value="0">Min</option>
                      </select>
                    </div>
                    <div class="form-group">
                        <div class="text-left" id="error_u_options" style="padding: 2px; color:red;"></div>
                        <select class="custom-select" id="options_e" name="options">
                          <option selected value="">Choose...</option>
                          <option value="1">Text</option>
                          <option value="2">Number</option>
                          <option value="3">Select Box</option>
                        </select>
                    </div>
                    <div class="form-group" id="load_option_e" hidden="true">
                      <button id="tambah_option_e" class="btn btn-primary">Add option</button><br/><br/>
                      <div id="select_option_e"></div>
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="form-edit-criteria" name="" onclick="do_update(this.name)">Update</button>
                </div>
              </div>
            </div>
          </div>
      @endsection