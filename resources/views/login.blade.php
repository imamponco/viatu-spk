<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Viatushop Dashboard Login</title>

  <!-- Custom fonts for this template-->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{ asset(url('admin/css/sb-admin-2.min.css')) }}" rel="stylesheet">

</head>

<body class="bg-gradient-primary">
<style>
background-image: {{ asset('images/background.jpg')}};
</style>
  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <img src="{{ asset('images/viatu.png') }}" class="col-lg-6 d-none d-lg-block">
                <div class="col-lg-6 p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                  </div>
                  <form class="user" action="{{ url('do_login') }}" method="post">
                    {{ csrf_field() }}
                    	@if(\Session::has('alert'))
                        <div class="alert alert-danger text-center" style="padding: 2px;">
                            <div>{{Session::get('alert')}}</div>
                        </div>
                      @endif
                      {{-- @if(\Session::has('alert-success'))
                          <div class="alert alert-success text-center" style="padding: 2px;">
                              <div>{{Session::get('alert-success')}}</div>
                          </div>
                      @endif --}}
                    <div class="form-group">
                      <input name="email" type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Email/Phone...">
                    </div>
                    <div class="form-group">
                      <input name="password" type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                    </div>
                   
                    <button class="btn btn-primary btn-user btn-block">
                      Login
                    </button>
                  
                  </form>
                  <hr>
                  <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Viatushop 2020</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset(url('admin/vendor/jquery/jquery.min.js')) }}"></script>
  <script src="{{ asset(url('admin/vendor/bootstrap/js/bootstrap.bundle.min.js')) }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset(url('admin/vendor/jquery-easing/jquery.easing.min.js')) }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset(url('admin/js/sb-admin-2.min.js')) }}"></script>

</body>

</html>
