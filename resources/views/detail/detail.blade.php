@extends('backend.dashboard')
@section('title','Viatushop - Detail Order')
@section('content')

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">@yield('title') {{ $shoes_model[0]->model_name }}</h6>
            </div>
            <div class="card-body">
              <div class="row">
                <a href="#" class="btn btn-success btn-square" data-toggle="modal" data-target="#add_Detail" data-whatever="#" style="margin-bottom: 20px;">
                  <i class="fas fa-plus"></i>
                </a>
                <a href="#" id="delete_detail" name="{{ ($shoes_model[0]->id_tr == null ? 'null': $shoes_model[0]->id_tr) }}" class="btn btn-danger btn-icon-split ml-4" style="margin-bottom: 20px;">
                  <span class="icon text-white-50">
                    <i class="fas fa-trash"></i>
                  </span>
                  <span class="text">Clear Detail Order</span>
                </a>
              </div>
             
              <div class="d-sm-flex align-items-center justify-content-between mb-4 float-right">
                <a href="{{ url()->previous() }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa fa-arrow-left fa-sm text-white"></i></a>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                
                    <tr>
                      <th>No</th>
                      <th>Name</th>
                      <th>Colour</th>
                      <th>Size</th>
                      <th>Width</th>
                      <th>Length</th>
                      <th>Description</th>
                      <th>Quality Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php $no = 1; ?>
                    @foreach($detail as $row)
                    <tr>
                    
                      <td>{{ $no }}</td>
                      <td>{{ $row->detail_name }}</td>
                      <td>{{ $row->colour }}</td>
                      <td>{{ $row->size }}</td>
                      <td>{{ $row->width }}</td>
                      <td>{{ $row->length }}</td>
                      <td>{{ $row->description }}</td>
                      <td>
                        @switch($row->quality_status)
                            @case(0)
                              Not checked
                              @break
                            @case(1)
                              Ready
                              @break
                        @endswitch
                      </td>
                      <td>
                      <div class="row">
                          <a href="#" class="btn btn-info btn-circle ml-4 mr-2" id="id_detail-{{ $row->id }}" onclick="editDetail(this.id)" data-toggle="modal" data-target="#editDetail data-whatever="#">
                            <i class="fas fa-edit"></i>
                          </a>                  
                          <a href="#" id="{{ $row->id }}" name="detail" class="btn btn-danger btn-circle ml-4" onclick="delete_data(this.id,this.name)">
                            <i class=" fas fa-trash"></i>
                          </a>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                  </tbody>
                 
                </table>
              </div>
            </div>
          </div>

          <!-- Add Modal -->
          <div class="modal fade bd-example-modal-lg" id="add_Detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Add Detail Order</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <button type="button" class="btn btn-primary" id="common">Common</button>
                  <button type="button" class="btn btn-primary" id="costume">Costume</button>
                  
                  <form class="add" action="#" id="form_detail_add">
                    <input type="hidden" class="form-control" id="type_form" value="common">
                    <div class="form-group">
                      <label for="size" class="col-form-label">Size:</label>
                      <div class="text-left" id="error_size" style="padding: 2px; color:red;"></div>
                      <input type="text" class="form-control" id="size">
                    </div>
                    <div class="form-group">
                      <label for="colour" class="col-form-label">Colour:</label>
                      <div class="text-left" id="error_colour" style="padding: 2px; color:red;"></div>
                      <input type="text" class="form-control" id="colour">
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col">
                          <div class="row">
                            <div class="col">
                              <label for="width" class="col-form-label">Width:</label>
                                <div class="text-left" id="error_width" style="padding: 2px; color:red;"></div>
                              <input type="text" class="form-control" id="width">
                            </div>
                            <div class="col-md-2">
                              <label for="p" class="col-form-label"></label>
                              <p class="mt-4">Cm</p>
                            </div>
                          </div>
                        </div>
                        
                        <div class="col">
                          <div class="row">
                            <div class="col">
                              <label for="length" class="col-form-label">Length:</label>
                                <div class="text-left" id="error_length" style="padding: 2px; color:red;"></div>
                              <input type="text" class="form-control" id="length"> 
                            </div>
                            <div class="col-md-2">
                              <label for="p" class="col-form-label"></label>
                              <p class="mt-4">Cm</p>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                    <div class="form-group">
                      <label for="total" class="col-form-label">Total:</label>
                      <div class="text-left" id="error_total" style="padding: 2px; color:red;"></div>
                      <input type="text" class="form-control" id="total"> 
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="detailSave" onclick="detail_save(this.name)" name="{{ $shoes_model[0]->id_tr }}">Save</button>
                </div>
              </div>
            </div>
          </div>

          <!-- Edit Modal -->
          <div class="modal fade bd-example-modal-lg" id="edit_Detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Edit Detail Order</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form class="add" action="#">
                    <div class="form-group">
                      <label for="name" class="col-form-label">Name:</label>
                      <div class="text-left" id="error_u_name" style="padding: 2px; color:red;"></div>
                      <input type="text" class="form-control" id="e-name">
                    </div>
                    <div class="form-group">
                      <label for="colour" class="col-form-label">Colour:</label>
                      <div class="text-left" id="error_u_colour" style="padding: 2px; color:red;"></div>
                      <input type="text" class="form-control" id="e-colour">
                    </div>
                    <div class="form-group">
                      <label for="size" class="col-form-label">Size:</label>
                      <div class="text-left" id="error_u_size" style="padding: 2px; color:red;"></div>
                      <input type="text" class="form-control" id="e-size">
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col">
                          <div class="row">
                            <div class="col">
                              <label for="width" class="col-form-label">Width:</label>
                                <div class="text-left" id="error_u_width" style="padding: 2px; color:red;"></div>
                              <input type="text" class="form-control" id="e-width">
                            </div>
                            <div class="col-md-2">
                              <label for="p" class="col-form-label"></label>
                              <p class="mt-4">Cm</p>
                            </div>
                          </div>
                        </div>
                        
                        <div class="col">
                          <div class="row">
                            <div class="col">
                              <label for="length" class="col-form-label">Length:</label>
                                <div class="text-left" id="error_u_length" style="padding: 2px; color:red;"></div>
                              <input type="text" class="form-control" id="e-length"> 
                            </div>
                            <div class="col-md-2">
                              <label for="p" class="col-form-label"></label>
                              <p class="mt-4">Cm</p>
                            </div>
                          </div>
                        </div>
                      
                      </div>
                  </div>
                    <div class="form-group">
                      <label for="description" class="col-form-label">Description:</label>
                      <div class="text-left" id="error_u_description" style="padding: 2px; color:red;"></div>
                      <textarea class="form-control" id="e-description"></textarea>
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="detailUpdate" onclick="detail_update(this.name)">Update</button>
                </div>
              </div>
            </div>
          </div>

      @endsection