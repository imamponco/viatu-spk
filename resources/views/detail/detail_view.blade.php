@extends('backend.dashboard')
@section('title','Viatushop - Detail Order')
@section('content')
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">@yield('title') {{ $shoes_model[0]->model_name }}</h6>
            </div>
            <div class="card-body">
               <div class="d-sm-flex align-items-center justify-content-between mb-4 float-right">
                <a href="{{ url()->previous() }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa fa-arrow-left fa-sm text-white"></i></a>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>

                    <tr>
                      <th>No</th>
                      <th>Name</th>
                      <th>Colour</th>
                      <th>Size</th>
                      <th>Width</th>
                      <th>Length</th>
                      <th>Description</th>
                      <th>Quality Status</th>

                    </tr>
                  </thead>
                  <tbody>

                    <?php $no = 1; ?>
                    @foreach($detail as $row)
                    <tr>
                      <td>{{ $no }}</td>
                      <td>{{ $row->detail_name }}</td>
                      <td>{{ $row->colour }}</td>
                      <td>{{ $row->size }}</td>
                      <td>{{ $row->width }}</td>
                      <td>{{ $row->length }}</td>
                      <td>{{ $row->detail_description }}</td>
                      <td>
                        @if (session('role')=='Administrator')

                              @if($row->quality_status==0)
                                <button class="btn btn-success btn-circle ml-4 mr-3" style="margin-bottom: 20px;" id="{{ $row->id_tr_shoes."-".$row->id_po."-".$row->id }}" name="quality" onclick="update_data(this.id,this.name)">
                                  <i class="fas fa-check-circle"></i>
                                </button>
                              @else
                                <button class="btn btn-danger btn-circle ml-4 mr-3" style="margin-bottom: 20px;" id="{{ $row->id_tr_shoes."-".$row->id_po."-".$row->id }}" name="quality_cancel" onclick="update_data(this.id,this.name)">
                                  <i class="fas fa-window-close"></i>
                                </button>
                              @endif
                        @else
                          @switch($row->quality_status)
                              @case(0)
                                Not checked
                                @break
                              @case(1)
                                Ready
                                @break
                          @endswitch
                        @endif

                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                  </tbody>

                </table>
              </div>
            </div>
          </div>


      @endsection
