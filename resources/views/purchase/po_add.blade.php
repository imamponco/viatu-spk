@extends('backend.dashboard')
@section('title','Viatushop - Purchase Order')
@section('content')
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-6">Form Data Purchase Order</h1>
              </div>
              @if(\Session::has('alert-success'))
                  <div class="alert alert-success text-center" style="padding: 2px;">
                      <div>{{Session::get('alert-success')}}</div>
                  </div>
              @endif

              @if(\Session::has('alert'))
                    <div class="alert alert-danger text-center" style="padding: 2px;">
                        <div>{{Session::get('alert')}}</div>
                    </div>
              @endif
              
              <form class="user" action="{{ url('po') }}" method="post">
                    @csrf
                    <div class="form-group">
                      <div class="input-group-prepend">
                        <label for="inputGroupSelect01">Customers Name</label>
                      </div>
                      @if($errors->has('id_customer'))
                        <div class="text-left" style="padding: 2px; color:red;">{{ $errors->first('id_customer') }}</div>
                      @endif
                      <select class="custom-select" id="inputGroupSelect01" name="id_customer" {{ ($param_detail > 0  ? "disabled" : "" ) }}>
                        <option selected value="{{ old('id_customer') }}">Choose...</option>
                        @foreach($customers as $cst)
                        <option value="{{ $cst->id_customer }}">{{ $cst->customer_name }}</option>
                        @endforeach
                      </select>
                    </div>
                   
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Date Order</label>
                          @if($errors->has('date_order'))
                              <div class="text-left" style="padding: 2px; color:red;">{{ $errors->first('date_order') }}</div>
                          @endif
                          <input type="date" name="date_order" value="{{ old('date_order') }}" class="form-control" {{ ($param_detail > 0  ? "disabled" : "" ) }}>
                        </div>
                      </div>

                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="name_label">Deadline</label>
                          @if($errors->has('deadline'))
                              <div class="text-left" style="padding: 2px; color:red;">{{ $errors->first('deadline') }}</div>
                          @endif
                          <input id="name_label" type="date" name="deadline" value="{{ old('deadline') }}" class="form-control" {{ ($param_detail > 0  ? "disabled" : "" ) }}>
                        </div>
                      </div>
                    </div>

                 
                  <div class="form-group">
                    <div class="card shadow mb-4">
                      <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Shoes Model</h6>
                      </div>
                    <div class="card-body">
                      <a href="#" onclick="get_shoes_model(null)" class="btn btn-success btn-square" data-toggle="modal" data-target="#getShoesModel" data-whatever="#" style="margin-bottom: 20px;">
                        <i class="fas fa-plus"></i>
                      </a>
                        
                      <div class="table-responsive">
                        <table class="table table-bordered display" width="100%" cellspacing="0">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Model Name</th>
                              <th>Model Code</th>
                              <th>Unit Price</th>
                              <th>Unit</th> 
                              <th>Description</th>
                              <th>Total</th>
                              <th style="width:120px;">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $no = 1; ?>
                            @foreach($shoes as $row)
                            <tr>
                              <td>{{ $no }}</td>
                              <td>{{ $row->model_name }}</td>
                              <td>{{ $row->model_code }}</td>
                              <td>{{ $row->current_price  }}</td>
                              <td>{{ $row->unit }}</td>
                              <td>{{ $row->shoes_description }}</td>
                              <td>{{ ($row->unit * $row->current_price) }}</td>
                              <td>
                                <div class="row">
                                  <a href="{{ url('detail/'.$row->id_tr) }}" class="btn btn-success btn-circle ml-2 mr-2" style="margin-bottom: 20px;">
                                    <i class="fas fa-info-circle"></i>
                                  </a>
                                  <a href="#" onclick="showShoesModel(this.id)" id="{{ $row->id_shoes_model }}" class="btn btn-info btn-circle mr-2">
                                    <i class="fas fa-eye"></i>
                                  </a>
                                  <a href="#" id="{{ $row->id_tr }}" name="tr_shoe" class="btn btn-danger btn-circle" onclick="delete_data(this.id,this.name)">
                                    <i class=" fas fa-trash"></i>
                                  </a>
                              </div>
                              </td>
                            </tr>
                            <?php $no++; ?>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="card shadow mb-4">
                      <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Additional</h6>
                      </div>
                    <div class="card-body">
                      <a href="#" onclick="" class="btn btn-success btn-square" data-toggle="modal" data-target="#additionalModal" data-whatever="#" style="margin-bottom: 20px;">
                        <i class="fas fa-plus"></i>
                      </a>
                        
                      <div class="table-responsive">
                        <table class="table table-bordered display" id="dataTable-Additional" width="100%" cellspacing="0">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Name</th>
                              <th>Unit Price</th>
                              <th>Unit</th>
                              <th>Description</th>
                              <th>Total</th> 
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $no = 1; ?>
                            @foreach($additionals as $add)
                            <tr>
                              <td>{{ $no }}</td>
                              <td>{{ $add->add_name }}</td>
                              <td>{{ $add->add_price }}</td>
                              <td>{{ $add->add_unit }}</td>
                              <td>{{ $add->add_description }}</td>
                              <td>{{ ($add->add_price * $add->add_unit) }}</td>
                              <td>
                                <div class="row">
                                  <a href="#" class="btn btn-info btn-circle ml-4 mr-2" id="id_additional-{{ $add->id_add }}" onclick="editAdditional(this.id)" data-toggle="modal" data-target="#editAdditional" data-whatever="#">
                                    <i class="fas fa-edit"></i>
                                  </a>                  
                                  <a href="#" id="{{ $add->id_add }}" name="additional" class="btn btn-danger btn-circle ml-4" onclick="delete_data(this.id,this.name)">
                                    <i class=" fas fa-trash"></i>
                                  </a>
                                </div>
                              </td>
                            </tr>
                            <?php $no++; ?>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>

                <button type="submit" class="btn btn-primary col-lg-12">Save Order</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Shoes Modal -->
      <div class="modal fade bd-example-modal-lg" id="getshoesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Shoes Model</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="input-group">
                  <select class="custom-select" id="shoes-model-select">
                    <option selected value="">Choose...</option>
                  </select>
                </div>
                <div class="input-group">
                  <div class="float-right mt-2">
                    <p class="">Not found the model? Add new model <a href="{{ url('shoe') }}">here.</a></p>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="model_po_save" class="btn btn-primary">Save</button>
              </div>
          </div>
        </div>
      </div>
    </div>

        <!-- edit shoes modal -->
        <div class="modal fade bd-example-modal-lg" id="showShoesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Shoes Model</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <form id="update_model_form">
                    <label class="model-current-image" for="uploaded">Current Image</label>
                    <div class="model-current-image" id="model-current-image">
                    </div>
                      <div class="form-group">
                        <div class="text-left" id="error_model_name_u" style="padding: 2px; color:red;"></div>
                        <label for="model-name" class="col-form-label">Model Name:</label>
                        <input type="text" class="form-control" id="current-model-name" disabled>
                      </div>
                      <div class="form-group">
                        <div class="text-left" id="error_model_code_u" style="padding: 2px; color:red;"></div>
                        <label for="model-code" class="col-form-label">Model Code:</label>
                        <input type="text" class="form-control" id="current-model-code" disabled>
                      </div>
                      <div class="form-group">
                      <div class="text-left" id="error_unit_price_u" style="padding: 2px; color:red;"></div>
                        <label for="unit-price" class="col-form-label">Unit Price:</label>
                        <input type="text" class="form-control" min="1" id="current-unit-price" disabled>
                      </div>
                      <div class="form-group">
                        <div class="text-left" id="error_model_description_u" style="padding: 2px; color:red;"></div>
                        <label for="description" class="col-form-label">Description:</label>
                        <textarea class="form-control" id="current-description-shoes" disabled></textarea>
                  </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Additional Modal -->
        <div class="modal fade bd-example-modal-lg" id="additionalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Add Additional</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                      <input type="hidden" id="id_po" value="">
                      <label for="model-name" class="col-form-label">Additional Name:</label>
                      <div class="text-left" id="error_add_name" style="padding: 2px; color:red;"></div>
                      <input type="text" class="form-control" id="additional_name">
                    </div>
                    <div class="form-group">
                      <label for="unit-price" class="col-form-label">Unit Price:</label>
                      <div class="text-left" id="error_unit_price" style="padding: 2px; color:red;"></div>
                      <input type="text" class="form-control" min="1" id="unit_price-ad">
                    </div>
                    <div class="form-group">
                      <label for="unit" class="col-form-label">Unit:</label>
                      <div class="text-left" id="error_unit" style="padding: 2px; color:red;"></div>
                      <input type="text" class="form-control" min="1" id="unit-ad">
                    </div>
                    <div class="form-group">
                      <label for="description" class="col-form-label">Description:</label>
                      <div class="text-left" id="error_description" style="padding: 2px; color:red;"></div>
                      <textarea class="form-control" id="description-ad"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="additional_save">Save</button>
                </div>
              </div>
            </div>
        </div>

      <!-- Additional Edit Modal -->
      <div class="modal fade bd-example-modal-lg" id="editAdditional" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Additional</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <div class="form-group">
                    <label for="model-name" class="col-form-label">Additional Name:</label>
                    <div class="text-left" id="error_u_add_name" style="padding: 2px; color:red;"></div>
                    <input type="text" class="form-control" id="additional_name-e">
                  </div>
                  <div class="form-group">
                    <label for="unit-price" class="col-form-label">Unit Price:</label>
                    <div class="text-left" id="error_u_add_price" style="padding: 2px; color:red;"></div>
                    <input type="text" class="form-control" min="1" id="unit_price-ad-e">
                  </div>
                  <div class="form-group">
                    <label for="unit" class="col-form-label">Unit:</label>
                    <div class="text-left" id="error_u_add_unit" style="padding: 2px; color:red;"></div>
                    <input type="text" class="form-control" min="1" id="unit-ad-e">
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-form-label">Description:</label>
                    <div class="text-left" id="error_u_add_description" style="padding: 2px; color:red;"></div>
                    <textarea class="form-control" id="description-ad-e"></textarea>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="additional_edit_save" name="" onclick="do_update_add(this.name)">Update</button>
              </div>
            </div>
          </div>
        </div>

        <!-- =========================== CAROUSEL =========================== -->
        <div class="modal fade bd-example-modal-lg" id="carousel_thumbnail" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Preview</h5>
              <button type="button" class="close" onclick="close_slideshow();" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <div id="mdb-lightbox-ui"></div>
              <div id="carousel_indicators" class="carousel slide" data-ride="carousel">
                <ol id="thumbnail_indicators" class="carousel-indicators">
                  <li data-target="#carousel_indicators" data-slide-to="0" class="active"></li>
                </ol>
                <div id="thumbnail_item" class="carousel-inner">
                  <div class="carousel-item active">
                    <img class="d-block w-100" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17299ff5b6f%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17299ff5b6f%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="First slide">
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carousel_indicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel_indicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" onclick="close_slideshow();">Close</button>
            </div>
          </div>
        </div>
      </div>
      
      <?php $limit = count($images); $count = 0; ?>
      <div class="modal fade bd-example-modal-lg" id="carousel_uploaded" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Uploaded Image</h5>
              <button type="button" class="close" onclick="close_uploaded_thumbnail();" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <div id="mdb-lightbox-ui"></div>
              <div id="carousel_uploaded_indicators" class="carousel slide" data-ride="carousel">
                <ol id="uploaded_indicators" class="carousel-indicators">
                  @foreach($images as $img)
                    @if($count == 0)
                    <li data-target="#carousel_uploaded_indicators" data-slide-to="{{ $count }}" class="active"></li>
                    @else
                      <li data-target="#carousel_uploaded_indicators" data-slide-to="{{ $count }}" ></li>
                    @endif
                    <?php $count++; ?>
                  @endforeach
                  <?php $count=0; ?>
                </ol>
                <div id="uploaded_item" class="carousel-inner">
                @foreach($images as $img)
                  @if($count==0)
                    <div class="carousel-item active">
                      <img id="uploaded-model-{{ $count }}" class="d-block w-100" src="{{ asset(url('assets/images/'.$img->img_path)) }}" alt="First slide">
                    </div>
                  @else
                    <div class="carousel-item">
                      <img id="uploaded-model-{{ $count }}" class="d-block w-100" src="{{ asset(url('assets/images/'.$img->img_path)) }}" alt="Slide">
                    </div>
                  @endif
                  <?php $count++; ?>
                @endforeach
                </div>
                <a class="carousel-control-prev" href="#carousel_uploaded_indicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel_uploaded_indicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" onclick="close_uploaded_thumbnail();">Close</button>
            </div>
          </div>
        </div>
      </div>
          
@endsection