@extends('backend.dashboard')
@section('title','Viatushop - Purchase Order')
@section('content')

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">@yield('title')</h6>

            </div>
            <div class="card-body">
              <div class="row mb-4">
                <div class="col-2">
                  <a href="{{ url('po/create') }}" class="btn btn-success btn-square" style="margin-bottom: 20px;">
                    <i class="fas fa-plus"></i>
                 </a>
                </div>
                <div class="col-2">
                  <label for="first_date">Start Date:</label><br>
                  <input class="form-control" type="date" id="start_date">
                </div>
                <div class="col-2">
                  <label for="second_date">End Date:</label><br>
                  <input class="form-control" type="date" id="end_date">
                </div>
                <div class="col-2">
                  <label for="status">Status:</label>
                  <select class="form-control" id="status">
                    <option selected value="10">Choose...</option>
                    <option value="0">Waiting</option>
                    <option value="1">Is Doing</option>
                    <option value="2">Done</option>
                </select>
                </div>
                {{-- <div class="col-2">
                  <label for="status">Status Verification:</label>
                  <select class="form-control" id="status_verification">
                    <option selected value="10">Choose...</option>
                    <option value="0">Unverified</option>
                    <option value="1">Verified</option>
                </select>
                </div> --}}
                <div class="col-2">
                  <label for="action">Action:</label><br>
                  <button class="btn btn-success btn-square" id="go">Go</>
                </div>
              </div>

              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Customer Name</th>
                      <th>Po Code</th>
                      <th>Date Order</th>
                      <th>Deadline</th>
                      <th>Status</th>
                      {{-- <th>Status Verified</th> --}}
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
                    @foreach($po as $row)
                    <tr>
                     
                      <td>{{ $no }}</td>
                      <td>{{ $row->customer_name }}</td>
                      <td>{{ $row->id }}</td>
                      <td>{{ $row->date_order }}</td>
                      <td>{{ $row->deadline }}</td>
                      <td>
                        @switch($row->status)
                          @case(0)
                            Waiting
                            @break
                          @case(1)
                            Is Doing
                            @break
                          @case(2)
                            Done
                            @break
                        @endswitch
                      </td>
                      {{-- <td>
                        @switch($row->status_verification)
                          @case(0)
                            Unverified
                            @break
                          @case(1)
                            Verified
                            @break
                          @case(2)
                            Rejected
                            @break
                        @endswitch
                      </td> --}}
                      <td>
                      <div class="row">
                        @if ($row->status==0)
                          <a href="{{ url('make_report/'.$row->id) }}" class="btn btn-primary btn-circle ml-2">
                            <i class="fas fa-download"></i>
                          </a>
                        @endif
                        <a href="{{ URL::to('po/' . $row->id . '/edit') }}" class="btn btn-info btn-circle ml-2">
                          <i class="fas fa-edit"></i>
                        </a>

                        <a href="#" id="{{ $row->id }}" name="po" class="btn btn-danger btn-circle ml-2" onclick="delete_data(this.id,this.name)">
                          <i class=" fas fa-trash"></i>
                        </a>
                        </div>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                  </tbody>

                </table>
              </div>
            </div>
          </div>
      @endsection
