@extends('backend.dashboard')
@section('title','Viatushop - Quality Report')
@section('content')

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">@yield('title')</h6>
              
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                
                    <tr>
                      <th>No</th>
                      <th>Customer Name</th>
                      <th>Po Code</th>
                      <th>Date Order</th>
                      <th>Deadline</th>
                      <th>Status</th>
                      <th>Handler</th> 
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
                    @foreach($po as $row)
                    <tr>
                    
                      <td>{{ $no }}</td>
                      <td>{{ $row->customer_name }}</td>
                      <td>{{ $row->id_po }}</td>
                      <td>{{ $row->date_order }}</td>
                      <td>{{ $row->deadline }}</td>
                      <td>@switch($row->status)
                            @case(0)
                              Waiting
                              @break
                            @case(1)
                              Is Doing
                              @break
                            @case(2)
                              Done
                              @break
                        @endswitch</td>
                      <td>{{ $row->username }}</td>
                      <td>
                      <div class="row">
                        <a href="{{ URL::to('quality/' . $row->id ) }}" class="btn btn-info btn-circle ml-3">
                          <i class="fas fa-eye"></i>
                        </a>
                        </div>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                  </tbody>
                 
                </table>
              </div>
            </div>
          </div>
      @endsection