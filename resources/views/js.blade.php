 <script>
	var last = 1;

	$(document).ready(function() {
		var no = 1;



		$('.display').DataTable();

		$('#options').change(function(){
			$('#tambah_option').click();
		});

		$('#options_e').change(function(){
			if($('#options_e').val() == 3){
				$('#load_option_e').attr('hidden',false);
			}else{
				$('#load_option_e').attr('hidden',true);
			}
		});

		$("#tambah_option").click(function(){

			var option = $("#options").find('option:selected').val();
			var content = "<div id='row_"+no+"'><label class='col-form-label'>Option:</label><br><input type='text' class='form-input' name='op_label' placeholder='Name'> <input type='text' class='form-input ml-2' name='op_value' placeholder='Value'>"+
			"<a href='#' class='form-input btn btn-danger ml-2' id='"+no+"' onClick='delete_option(this.id)'>Delete</a><br/></div>";

			if(option == 3){
				no++;
				$('#select_option').append(content);
				$('#load_option').attr('hidden',false);
			}else{
				$('#load_option').attr('hidden',true);
			}

			return false;
		});

		$("#tambah_option_e").click(function(){

			var option = $("#options_e").find('option:selected').val();
			var content = "<div id='row_"+last+"'><label class='col-form-label'>Option:</label><br><input type='text' class='form-input' name='op_label' placeholder='Name'> <input type='text' class='form-input ml-2' name='op_value' placeholder='Value'>"+
			"<a href='#' class='form-input btn btn-danger ml-2' id='"+last+"' onClick='delete_option(this.id)'>Delete</a><br/></div>";

			if(option == 3){
				$('#select_option_e').append(content);
				$('#load_option_e').attr('hidden',false);
				last++;
			}else{
				$('#load_option_e').attr('hidden',true);
			}

			return false;
		});

	} );


	function delete_option(id){
			var amount_option = $("input[name='op_value']").length;
			if(amount_option == 1){
				$('select option:contains("Choose...")').prop('selected',true);
				$('#load_option').attr('hidden',true);
				$('#load_option_e').attr('hidden',true);
				$('#row_'+id).remove();
			}else{
				$('#row_'+id).remove();
			}

			return false;
	}

	function LengthObj( object ) {
		var length = 0;
		for( var key in object ) {
			if( object.hasOwnProperty(key) ) {
				++length;
			}
		}
		return length;
	};

	@php
	if(!empty($new_verified)){
		$jml_production = count($new_verified);
		if($jml_production > 0){
		@endphp
			let new_verified = "{{ $jml_production }}";
			let yuhu = {
				success: "Terdapat "+new_verified+" alternatif baru. <a href='#' onClick='judgement()'>Lihat</a>",
				activity_name: "New po"
			}
			notification(yuhu);
		@php
		}

	}
	@endphp


	@php
	if(!empty($new_po)){
		$jml_marketing = count($new_po);
		if($jml_marketing > 0){
		@endphp
			let new_po = "{{ $jml_marketing }}";
			let yuhu = {
				success: "Terdapat "+new_po+" pesanan baru yang harus di verfikasi. <a href='{{ url('verify') }}'>Lihat</a>",
				activity_name: "Managing po"
			}
			notification(yuhu);
		@php
		}

	}
	@endphp

	function resetInput(){
		$('.form-control').val('');
	}

	function resetSomeInput(data){
		// reset some input form, data on paramaters will gonna form like this
		// data = { class:[],id:[] }

		try{

		if(data.id[0]!=""){
			// reset form all id
			for (let index = 0; index < data.id.length; index++) {
				$('#'+data.id[index]).val('');
			}
		}


		}catch(err){

		}

		try{

			if(data.class[0]!=""){
				//reset form all class
				for (let index = 0; index < data.class.length; index++) {
				$('.'+data.class[index]).val('');
				}
			}


		}catch(err){

		}


	}

	function read_notification(activity_name){

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': "{{ csrf_token() }}"
			}
        });

		<?php $prefix = url('/')."/read_notification";  ?>

		$.ajax({
			url: "{{ $prefix }}",
			method: 'patch',
			data: {
				activity_name: activity_name,
			},
			success: function(result){
				console.log(result);
			},
			error: function(data) {
				console.log(data);
    			try{

    			}catch(err){

    			}
			}

		});

	}

	function notification(data){
		$('#message-notification').html(data.success);
		$('#notificationModal').modal('show');
		read_notification(data.activity_name);
	}



	function refreshTable() {
		$('.dataTable').each(function() {
			dt = $(this).dataTable();
			dt.fnDraw();
		})
	}

	function refreshThumbnail(data,div){

		<?php $prefix = asset(url('assets/images'))."/"; ?>

		var all_data  = data.current_img;
		var thumbnail = "";
		var preview   = "<img src='data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17299ff5b6f%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17299ff5b6f%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E' class='rounded' alt='...'>";

		for (let index = 0; index < all_data.length; index++) {
			thumbnail += "<img onclick='uploaded_thumbnail();' src='{{ $prefix }}"+ all_data[index].img_path +"' class='img-fluid img-thumbnail m-3' style='width:200px; height:200px;' alt='...'><a href='#' onClick='delete_image(\""+all_data[index].id_images+"\",\"image\",\""+div+"\")' class='close' style='position:absolute; margin: 0 0 0 -15px;'>&times;</a>" ;
		}

		$('#'+div).html(thumbnail);
		$('#thumbnail-'+div).html(preview);

		if(thumbnail!=""){
			return true;
		}else{
			return false;
		}
	}

	function refreshSlider(data,div){

		var limit      = data.current_img.length;
		var all_data   = data.current_img;
		var indicators = "";
		var item 	   = "";

		$('#'+div+'_item').html(item);
		$('#'+div+'_indicators').html(indicators)

		for (let index = 0; index < limit; index++) {
			if(index == 0){
				indicators += "<li data-target='#carousel_uploaded_indicators' data-slide-to='"+index+"' class='active'></li>";
				item += "<div class='carousel-item active'><img onclick='uploaded_thumbnail();' id='uploaded-model-"+index+"' class='d-block w-100' src='{{ $prefix }}"+ all_data[index].img_path +"' alt='Picture no available'></div>";
			}else{
				indicators += "<li data-target='#carousel_uploaded_indicators' data-slide-to='"+index+"' class=''></li>";
				item += "<div class='carousel-item'><img onclick='uploaded_thumbnail();' id='uploaded-model-"+index+"' class='d-block w-100' src='{{ $prefix }}"+ all_data[index].img_path +"' alt='Picture no available'></div>";
			}
		}

		$('#'+div+'_item').html(item);
		$('#'+div+'_indicators').html(indicators)

		for (let a = 0; a < limit; a++) {
			$('#'+div+'-model-'+a).attr('src',all_data.img_path );
		}

		if(indicators!=""){
			return true;
		}else{
			return false;
		}
	}

	function update_data(id,url){

		let check = confirm('Are you sure want to change this data?');

		if(check){
			<?php $prefix = url('/')."/"; ?>
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});

			$.ajax({
				url: "{{ $prefix }}"+ url +"/"+ id,
				method: 'post',
				data: {
					_method: "PATCH",
					_token: "{{ csrf_token() }}",
				},
					success: function(result){
						setTimeout(function() {
							window.location.reload()
						}, (2 * 1000));	
					},
					error: function(data) {
					console.log("error")

					}
				});
		}else{
			return;
		}
	}

	function delete_data(id,url){

		let check = confirm('Are you sure want to delete this data?');

		if(check){
			<?php $prefix = url('/')."/"; ?>
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});

			$.ajax({
				url: "{{ $prefix }}"+ url +"/"+ id,
				method: 'post',
				data: {
					_method: "DELETE",
					_token: "{{ csrf_token() }}",
				},
					success: function(result){
						setTimeout(function() {
							refreshTable();
							window.location.reload()
						}, (2 * 1000));	
					},
					error: function(data) {
					    console.log("error")
					}
				});
		}else{
			return;
		}
	}

	function delete_image(id,url,div){
		let check = confirm('Are you sure want to delete this data?');
		console.log(div)
		if(check){

			<?php $prefix = url('/')."/"; ?>
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});

			$.ajax({
				url: "{{ $prefix }}"+ url +"/"+ id,
				method: 'post',
				data: {
					_method: "DELETE",
					_token: "{{ csrf_token() }}",
				},
					success: function(result){

						refreshThumbnail(result,div);
						refreshSlider(result,"uploaded");
					},
					error: function(data) {
					console.log("error")

					}
				});
		}else{
			return;
		}
	}

	$('body').on('hidden.bs.modal', function () {
		if($('.show').length > 1)
		{
			$('body').addClass('modal-open');
		}
	});

	function uploaded_thumbnail(){
		$('#carousel_uploaded').modal('toggle');
	}

	function close_uploaded_thumbnail(){
		$('#carousel_uploaded').modal('hide');
	}

	function slider_image(){
		$('#carousel_thumbnail').modal('toggle');
	}

	function close_slideshow(){
		$('#carousel_thumbnail').modal('hide');
	}


	function readURL(input,div) {
		// check extension files
		for (let i = 0; i < input.files.length; i++) {
			if(input.files[i].type[5] != '/'){
				var error = 1;
			}
		}

		if (error != 1) {
			let content = "";
			let indicators = "";
			let item = "";
			let limit = input.files.length;

			for (let index = 0; index < limit; index++) {
				content += "<img id='img-model-"+ index +"' onClick='slider_image();' class='img-fluid img-thumbnail m-3' style='width:200px; height:200px;' src='' alt='No preview available' data-size='1600x1067'>";
				if(index == 0){
					indicators += "<li data-target='#carousel_indicators' data-slide-to='"+index+"' class='active'></li>";
					item += "<div class='carousel-item active'><img id='item-model-"+index+"' class='d-block w-100' src='data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17299ff5b6f%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17299ff5b6f%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E' alt='Picture no available'></div>";
				}else{
					indicators += "<li data-target='#carousel_indicators' data-slide-to='"+index+"' class=''></li>";
					item += "<div class='carousel-item'><img id='item-model-"+index+"' class='d-block w-100' src='data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17299ff5b6f%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17299ff5b6f%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E' alt='Picture no available'></div>";
				}
			}

			$('#thumbnail-'+div).html(content);

			$('#thumbnail_item').html(item);
			$('#thumbnail_indicators').html(indicators)


			for (let a = 0; a < limit; a++) {
			var reader = new FileReader();

			 	reader.onload = function(e) {
				 	$('#img-model-'+a).attr('src', e.target.result);
					$('#item-model-'+a).attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[a]); // convert to base64 string
			}
		}else{
			$('#error_'+div).html('The file must be a file of type: jpeg, bmp, png, jpg')
		}
	}

	// ====================================== MODAL PURCHASE ORDER ========================================
	function upload_image(){
		var reset = {
					"class" : [
					    "custom-file-input",
					]
				}
		var formData = new FormData($('#shoes_model_form')[0]);

		$.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
               });

		$.ajax({
			url: "{{ url('upload_image') }}",
			method: 'post',
			processData: false,
    		contentType: false,
			data: formData,
			success: function(result){
				refreshThumbnail(result,"uploaded");
				refreshSlider(result,"uploaded");
				resetSomeInput(reset);
				$('#model-image').val('');
				$('#error_file_upload').html('');
			},
			error: function(data) {
    		    var data = data.responseJSON.errors;
                resetSomeInput(reset);
    			try{
    				error_handler(data,'s');
    			}catch(err){

    			}
			}

		});

	}

	function update_image(id){
		console.log(id)
	        var reset = {
					"class" : [
					    "custom-file-input",
					]
				}

		var formData = new FormData($('#update_model_form')[0]);

		$.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
               });
		<?php $prefix = url('/')."/update_image/";  ?>
		$.ajax({
			url: "{{ $prefix }}"+id,
			method: 'post',
			processData: false,
    		contentType: false,
			data: formData,
			success: function(result){
				refreshThumbnail(result,"model-current-image");
				refreshSlider(result,"uploaded");
				resetSomeInput(reset);
				$('#update-image').val('');
				$('#error_update_upload').html('');
			},
			error: function(data) {

    			var data = data.responseJSON.errors;
                resetSomeInput(reset);
    			try{
    				error_handler(data,'u');
    			}catch(err){

    			}
			}

		});

	}
	// shoes model PO
	function get_shoes_model(id){

		<?php $prefix = url('/')."/get_shoes/";  ?>

		var url = "{{ $prefix }}"+id

		$('#getshoesModal').modal('toggle');
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': "{{ csrf_token() }}"
			}
		});

		$.ajax({
			url: url,
			method: 'get',
			success: function(result){
			set_shoes_model(result)
			},
			error: function(data) {
			try{

			}catch(err){

			}
			}

		});
	}

	$('#model_po_save').on('click',function(e){
				e.preventDefault();
				var reset = {
					"id" : [ "shoes-model-select"]
				}

               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
               });

               $.ajax({
                  url: "{{ url('tr_shoe') }}",
                  method: 'post',
                  data: {
					id		: $('#shoes-model-select').val(),
					id_po	: $('#id_po').val()
                  },
                  success: function(result){
					refreshTable();
					resetSomeInput();
					$('#getshoesModal').modal('hide');
					window.location.reload();
                  },
                  error: function(data) {
					var data = data.responseJSON.errors;

                  	try{

						error_handler(data);

                  	}catch(err){

                  	}
                  }

              	});
	});

	function set_shoes_model(data){

		var limit = data.all_data.length;

		var content = "<option selected value=\"\">Choose...</option>";

		for (let i = 0; i < limit; i++) {
			content += "<option value=\""+ data.all_data[i].id_shoes_model +"\">"+data.all_data[i].model_name+"</option>";
		}

		$('#shoes-model-select').html(content);
	}

	function showShoesModel(id){
		$('#showShoesModal').modal('toggle');
			event.preventDefault();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});
			<?php $prefix = url('/')."/shoe/";  $sufix="/edit"; ?>
			$.ajax({
				url: "{{ $prefix }}"+ id + "{{ $sufix }}",
				method: 'get',
				success: function(result){

					var current = result;

					$('#update_image').attr('onclick',"update_image("+current.all_data.id_shoes_model+")")
					$('#model_shoes_update').attr('onclick',"update_shoes("+current.all_data.id_shoes_model+")")
					$('#current-model-name').val(current.all_data.model_name);
					$('#current-model-code').val(current.all_data.model_code);
					$('#current-unit-price').val(current.all_data.unit_price);
					$('#current-description-shoes').val(current.all_data.shoes_description);
					refreshThumbnail(result,"model-current-image")
					var check = refreshSlider(result,"uploaded");

					if(check == true){
						$("a").remove(".close");
					}

				},
				error: function(data) {
				console.log(data.responseJSON.errors.model_code[0])
				try{
					$('#error_model_code').html(data.responseJSON.errors.model_code[0]);
					$('#error_model_name').html(data.responseJSON.errors.model_name[0]);
					$('#error_unit_price').html(data.responseJSON.errors.unit_price[0]);
					$('#error_model_description').html(data.responseJSON.errors.description[0]);
				}catch(err){
					$('#error_model_name').html('');
					$('#error_model_code').html('');
					$('#error_unit_price').html('');
					$('#error_model_description').html('');
				}
				}

			});

	}



	// shoes model CRUD
	// $('#shoesModal').on('show.bs.modal', function (event) {
		$('#model_shoes_save').click(function(e){
               e.preventDefault();
			   var reset = {
					"id" : [ "model-name",
							 "model-code",
							 "unit-price",
                             "difficulty",
							 "description-shoes"
					]
				}
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
               });

               $.ajax({
                  url: "{{ url('shoe') }}",
                  method: 'post',
                  data: {
					model_name	: $('#model-name').val(),
					model_code	: $('#model-code').val(),
					unit_price	: $('#unit-price').val(),
                    difficulty  : $('#difficulty').val(),
					description	: $('#description-shoes').val()
                  },
                  success: function(result){
					refreshTable();
					resetSomeInput(reset);
					$('#shoesModal').modal('hide');
					window.location.reload();
                  },
                  error: function(data) {

					var data = data.responseJSON.errors;

                  	try{
						error_handler(data,'s');
                  	}catch(err){

                  	}
                  }

              	});
		});
	// })

	function editShoesModel(id){
		$('#editShoesModal').modal('toggle');
			event.preventDefault();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});
			<?php $prefix = url('/')."/shoe/";  $sufix="/edit"; ?>
			$.ajax({
				url: "{{ $prefix }}"+ id + "{{ $sufix }}",
				method: 'get',
				success: function(result){

					var current = result;
                    console.log(current.all_data.difficulty)
					$('#update_image').attr('onclick',"update_image('"+current.all_data.id_shoes_model+"')")
					$('#model_shoes_update').attr('onclick',"update_shoes('"+current.all_data.id_shoes_model+"')")
					$('#current-model-name').val(current.all_data.model_name);
					$('#current-model-code').val(current.all_data.model_code);
                    $('#current-difficulty').find('option[value="'+current.all_data.difficulty+'"]').prop('selected',true);
					$('#current-unit-price').val(current.all_data.unit_price);
					$('#current-description-shoes').val(current.all_data.shoes_description);
					refreshThumbnail(result,"model-current-image")
					refreshSlider(result,"uploaded")
				},
				error: function(data) {
				console.log(data.responseJSON.errors.model_code[0])
				try{
					$('#error_model_code').html(data.responseJSON.errors.model_code[0]);
					$('#error_model_name').html(data.responseJSON.errors.model_name[0]);
					$('#error_unit_price').html(data.responseJSON.errors.unit_price[0]);
					$('#error_model_description').html(data.responseJSON.errors.description[0]);
				}catch(err){
					$('#error_model_name').html('');
					$('#error_model_code').html('');
					$('#error_unit_price').html('');
					$('#error_model_description').html('');
				}
				}

			});
	}

	$('#editShoesModal').on('hidden.bs.modal',function(event){
		var data = {
			"class" : ["custom-file-input"],
		}

		resetSomeInput(data);
	});

	function update_shoes(id){
			$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': "{{ csrf_token() }}"
					}
				});
				<?php $prefix = url('/')."/shoe/";?>
				$.ajax({
					url: "{{ $prefix }}"+ id,
					method: 'patch',
					data: {
						model_name	: $('#current-model-name').val(),
						model_code	: $('#current-model-code').val(),
						unit_price	: $('#current-unit-price').val(),
                        difficulty  : $('#current-difficulty').val(),
						description	: $('#current-description-shoes').val()
                  	},
					success: function(result){
						$('#editShoesModal').modal('hide');
						$('.text-left').html('');
						refreshTable();
						window.location.reload();
					},
					error: function(data) {
						var data = data.responseJSON.errors;

						try{
							error_handler(data,'u');
						}catch(err){

						}
					}

				});

	}


	$("#model-image").change(function() {
		readURL(this,"uploaded");
	});

	$("#update-image").change(function() {
		readURL(this,"model-current-image");
	});

	$("#stuff_image").change(function() {
		readURL(this,"stuff");
	});

	// ADD ADITIONAL
	$('#additionalModal').on('show.bs.modal', function (event) {
		$('#additional_save').click(function(e){
               e.preventDefault();

               var reset = {
					"id" : [ "additional_name",
							 "unit_price-ad",
							 "unit-ad",
							 "description-ad",
							 "id_po"
					]
				}

               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
               });

               $.ajax({
                  url: "{{ url('additional') }}",
                  method: 'post',
                  data: {
                     add_name: $('#additional_name').val(),
                     unit_price: $('#unit_price-ad').val(),
					 unit: $('#unit-ad').val(),
					 description: $('#description-ad').val(),
					 id_po: $('#id_po').val()
                  },
                  success: function(result){
					$('#additionalModal').modal('hide');
                  	$('.text-left').html('');
					refreshTable();
                    resetSomeInput(reset);
					window.location.reload();
                  },
                  error: function(data) {
					var data = data.responseJSON.errors;

                  	try{
						error_handler(data,'s');
                  	}catch(err){

                  	}
                  }

              	});
		});
	})

	//EDIT ADDTIONAL
	function editAdditional(id){
		$('#editAdditional').on('show.bs.modal', function (event) {
			var split = id.split('-');
        	var id_additional = split[1];

        	$.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
            });
			<?php $prefix = url('/')."/additional/";  $sufix="/edit"; ?>
               $.ajax({
                  url: "{{ $prefix }}"+ id_additional + "{{ $sufix }}",
                  method: 'get',

                  success: function(result){
					console.log(result.data.add_unit)
                  	$('#additional_name-e').val(result.data.add_name)
					$('#unit_price-ad-e').val(result.data.add_price)
					$('#unit-ad-e').val(result.data.add_unit)
					$('#description-ad-e').val(result.data.add_description)
                  	$('#additional_edit_save').attr('name',result.data.id_add)
                  },
                  error: function(data) {

          		  }
              	});
		});
	}

	//DO UPDATE
	function do_update_add(id){
		$.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
            });

               $.ajax({
                  url: "{{ $prefix }}"+ id,
                  method: 'patch',
              	  data: {
					add_name: 		 $('#additional_name-e').val(),
					add_price:  	 $('#unit_price-ad-e').val(),
					add_unit: 	 	 $('#unit-ad-e').val(),
					add_description: $('#description-ad-e').val()
                  },
                  success: function(result){
                  	$('#editAdditional').modal('hide');
                  	$('.text-left').html('');
					refreshTable();
					window.location.reload();
                  },
                  error: function(data) {
					var data = data.responseJSON.errors;

                  	try{
						error_handler(data,'u');
                  	}catch(err){

                  	}
          		  }
              	});
	}


	function rewriteTableAdditional(data){
		var newdata = data.all_data;
		var result = newdata.map(function(data,i) {
			var data = [
				(i+1),data.name,data.unit_price,data.unit,data.description,(data.unit_price * data.unit),"<div class=\"row\"> <a href=\"#\" id=\"id_additional-"+data.id+"\" onclick=\"editAdditional(this.id)\" class=\"btn btn-info btn-circle ml-4 mr-2\" data-toggle=\"modal\" data-target=\"#editAdditional\" data-whatever=\"#\">\n <i class=\"fas fa-edit\"></i>\n  </a>\n <form action=\"{{ url('/') }}/additional/"+data.id+"\" method=\"post\" style=\"float: left;\">\n                          <input type=\"hidden\" name=\"_method\" value=\"delete\">                          <input type=\"hidden\" name=\"_token\" value=\"\ {{ csrf_token() }} \">                   \n                          <button class=\"btn btn-danger btn-circle \" onclick=\"return confirm('Are you sure want to delete this data?')\"><i class=\" fas fa-trash\"></i></button>\n </form></div>"
			];
		  return data;
		});

		var table = $('#dataTable-Additional').DataTable();
 		table.clear();
 		table.rows.add(result).draw();
	}

	// Function to create the cookie
	function createCookie(name, value, days) {
		var expires;

		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toGMTString();
		}
		else {
			expires = "";
		}

		document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
	}

	// ====================================== MODAL DETAIL ORDER ==========================================
	$('#add_Detail').on('show.bs.modal', function (event) {

	});

	$('#common').on('click',function(event){
		let form = "<input type='hidden' class='form-control' id='type_form' value='common'><div class='form-group'><label for='size' class='col-form-label'>Size:</label><div class='text-left' id='error_size' style='padding: 2px; color:red;'></div><input type='text' class='form-control' id='size'></div><div class='form-group'><label for='colour' class='col-form-label'>Colour:</label><div class='text-left' id='error_colour' style='padding: 2px; color:red;'></div><input type='text' class='form-control' id='colour'></div><div class='form-group'><div class='row'><div class='col'><div class='row'><div class='col'><label for='width' class='col-form-label'>Width:</label><div class='text-left' id='error_width' style='padding: 2px; color:red;'></div><input type='text' class='form-control' id='width'></div><div class='col-md-2'><label for='p' class='col-form-label'></label><p class='mt-4'>Cm</p></div></div></div><div class='col'><div class='row'><div class='col'><label for='length' class='col-form-label'>Length:</label><div class='text-left' id='error_length' style='padding: 2px; color:red;'></div><input type='text' class='form-control' id='length'> </div><div class='col-md-2'><label for='p' class='col-form-label'></label><p class='mt-4'>Cm</p></div></div></div></div></div><div class='form-group'><label for='total' class='col-form-label'>Total:</label><div class='text-left' id='error_total' style='padding: 2px; color:red;'></div><input type='text' class='form-control' id='total'></div>";
		$('#form_detail_add').html(form);
	});

	$('#costume').on('click',function(event){
		let form = "<input type='hidden' class='form-control' id='type_form' value='costume'><div class='form-group'><label for='name' class='col-form-label'>Name:</label><div class='text-left' id='error_name' style='padding: 2px; color:red;'></div><input type='text' class='form-control' id='name'></div><div class='form-group'><label for='colour' class='col-form-label'>Colour:</label><div class='text-left' id='error_colour' style='padding: 2px; color:red;'></div><input type='text' class='form-control' id='colour'></div><div class='form-group'><label for='size' class='col-form-label'>Size:</label><div class='text-left' id='error_size' style='padding: 2px; color:red;'></div><input type='text' class='form-control' id='size'></div><div class='form-group'><div class='row'><div class='col'><div class='row'><div class='col'><label for='width' class='col-form-label'>Width:</label><div class='text-left' id='error_width' style='padding: 2px; color:red;'></div><input type='text' class='form-control' id='width'></div><div class='col-md-2'><label for='p' class='col-form-label'></label><p class='mt-4'>Cm</p></div></div></div><div class='col'><div class='row'><div class='col'><label for='length' class='col-form-label'>Length:</label><div class='text-left' id='error_length' style='padding: 2px; color:red;'></div><input type='text' class='form-control' id='length'> </div><div class='col-md-2'><label for='p' class='col-form-label'></label><p class='mt-4'>Cm</p></div></div></div></div></div><div class='form-group'><label for='description' class='col-form-label'>Description:</label><div class='text-left' id='error_description' style='padding: 2px; color:red;'></div><textarea class='form-control' id='description'></textarea></div>";

		$('#form_detail_add').html(form);
	});

	// SAVE DETAIL ORDER
	function detail_save(id){
			var reset = {
					"id" : [ "name",
							 "colour",
							 "size",
							 "length",
							 "width",
							 "description",
							 "total"
					]
				}

			$.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
               });

            <?php $prefix = url('/')."/detail/"?>
			$.ajax({
				url: "{{ $prefix }}"+ id,
                  method: 'post',
                  data: {
                     name: $('#name').val(),
                     colour: $('#colour').val(),
					 size: $('#size').val(),
					 length: $('#length').val(),
					 width: $('#width').val(),
					 description: $('#description').val(),
					 total: $('#total').val(),
					 type_form: $('#type_form').val()
                  },
                  success: function(result){
					$('#add_Detail').modal('hide');
                  	$('.text-left').html('');
					refreshTable();
                    resetSomeInput(reset);
					window.location.reload();
                  },
                  error: function(data) {

					var data = data.responseJSON.errors;

                  	try{
						error_handler(data,'s');
                  	}catch(err){

                  	}
                  }
			   });
		}

	//EDIT DETAIL
	function editDetail(id){
		var split = id.split('-');
        var id = split[1];

		$('#edit_Detail').modal('toggle');
			event.preventDefault();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});
			<?php $prefix = url('/')."/detail/";  $sufix="/edit"; ?>
			$.ajax({
				url: "{{ $prefix }}"+ id + "{{ $sufix }}",
				method: 'get',
				success: function(result){

					var current = result;

					$('#detailUpdate').attr('onclick',"detail_update('"+current.all_data.id_do+"')")
					$('#e-name').val(current.all_data.detail_name);
					$('#e-colour').val(current.all_data.colour);
					$('#e-size').val(current.all_data.size);
					$('#e-width').val(current.all_data.width);
					$('#e-length').val(current.all_data.length);
					$('#e-description').val(current.all_data.detail_description);
				},
				error: function(data) {
					try{

					}catch(err){

					}
				}

			});
	}

	//UPDATE DETAIL
	function detail_update(id){
			var reset = {
				"id" : [ "e-name",
						 "e-colour",
						 "e-size",
						 "e-length",
						 "e-width",
						 "e-description"
				]
			}

			$.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
               });

            <?php $prefix = url('/')."/detail/"?>
			$.ajax({
				url: "{{ $prefix }}"+ id,
                  method: 'patch',
                  data: {
                     name: $('#e-name').val(),
                     colour: $('#e-colour').val(),
					 size: $('#e-size').val(),
					 length: $('#e-length').val(),
					 width: $('#e-width').val(),
					 description: $('#e-description').val()
                  },
                  success: function(result){
					console.log(result)
					$('#editDetail').modal('hide');
                  	$('.text-left').html('');

					refreshTable();
                    resetSomeInput(reset);
					window.location.reload();
                  },
                  error: function(data) {
					var data = data.responseJSON.errors;

                  	try{
						error_handler(data,'u');
                  	}catch(err){

                  	}
                  }
			   });
		}

	// ====================================== MODAL INVENTORY ==============================================
	// save invetory
	function inventory_save(){
			var reset = {
					"id" : [ "stuff_name",
							 "stuff_code",
							 "stuff_description"
							]
				}

			$.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
               });

            <?php $prefix = url('/')."/inventory"?>
			$.ajax({
				url: "{{ $prefix }}",
                  method: 'post',
                  data: {
                     stuff_name: $('#stuff_name').val(),
                     stuff_code: $('#stuff_code').val(),
					 stuff_description: $('#stuff_description').val()
                  },
                  success: function(result){
					$('#Inventory').modal('hide');
                  	$('.text-left').html('');

                    resetSomeInput(reset);
					refreshTable();
					window.location.reload();
                  },
                  error: function(data) {

					var data = data.responseJSON.errors;

                  	try{
						error_handler(data,'s');
                  	}catch(err){

                  	}
                  }
			   });
		}

	// get inventory
	function editInventory(id){
		$('#editInventory').modal('toggle');
		$.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
               });

            <?php $prefix = url('/')."/inventory/"?>
			$.ajax({
				url: "{{ $prefix }}"+id,
                  method: 'get',
                  success: function(result){
					$('#u_stuff_name').val(result.all_data.stuff_name)
                  	$('#u_stuff_code').val(result.all_data.stuff_code)
					$('#u_stuff_description').val(result.all_data.stuff_description)
                  	$('#inventory_update').attr('onclick','update_inventory(\''+result.all_data.id_inv+'\')')
                  },
                  error: function(data) {

					try{


					}catch(err){

					}

                  }
			   });
	}

	// update inventory
	function update_inventory(id){

		$.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
        });

		<?php $prefix = url('/')."/inventory/"?>
		$.ajax({
			url: "{{ $prefix }}"+id,
				method: 'patch',
				data: {
					stuff_name: $('#u_stuff_name').val(),
					stuff_code: $('#u_stuff_code').val(),
					stuff_description: $('#u_stuff_description').val()
				},
				success: function(result){
				$('#editInventory').modal('hide');
				$('.text-left').html('');

				refreshTable();
				window.location.reload();
				},
				error: function(data) {

				var data = data.responseJSON.errors;

				try{

				error_handler(data,'u');

				}catch(err){

				}

				}
		});
	}

	// ====================================== Stuff Transactions ==============================================

	function stuff_save(id){
		var reset = {
			"id" : [
						"unit",
						"inout"
					]
		}

		const unit = $('#unit').val();
		const inout = $('#inout').val();
		const formData = new FormData($('#stuff_form')[0]);

		formData.append('unit', unit);
		formData.append('inout', inout);
		formData.append('stuff_code', id);


		$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});

		$.ajax({
			url: "{{ url('stuff') }}",
			method: 'post',
			processData: false,
			contentType: false,
			data: formData,
			success: function(result){
			$('#stuffTr').modal('hide');
			$('.text-left').html('');

			resetSomeInput(reset);
			refreshTable();
			window.location.reload();
			},
			error: function(data) {

			var data = data.responseJSON.errors;

			try{
				error_handler(data,'s');
			}catch(err){

			}

			}
		});
	}

	function editStuff(id){

		var split = id.split('_');

		var id = split[1];
		var stuff_code = split[2];

		$.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
               });

            <?php $prefix = url('/')."/stuff/"?>
			$.ajax({
				url: "{{ $prefix }}"+id+"/edit",
                  method: 'get',
                  success: function(result){
					$('#stuff_code_edit').val(stuff_code)
                  	$('#u_inout').val(result.all_data.inout)
					$('#u_unit').val((result.all_data.inout == 0 ? (-1*result.all_data.stuff_unit) :result.all_data.stuff_unit))
                  	$('#stuffUpdate').attr('onclick','update_stuff(\''+result.all_data.id_tr+'\')')
                  },
                  error: function(data) {

					try{


					}catch(err){

					}

                  }
			   });
	}

	function update_stuff(id){

		$.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
        });

		<?php $prefix = url('/')."/stuff/"?>
		$.ajax({
			url: "{{ $prefix }}"+id,
				method: 'patch',
				data: {
					unit: $('#u_unit').val(),
					inout: $('#u_inout').val(),
					stuff_code: $('#stuff_code_edit').val()
				},
				success: function(result){
				$('#editStuff').modal('hide');
				$('.text-left').html('');

				refreshTable();
				window.location.reload();
				},
				error: function(data) {

				var data = data.responseJSON.errors;

				try{

				error_handler(data,'u');

				}catch(err){

				}

				}
		});
	}

	// ====================================== RESI CODE ===================================================

	function addResicode(id){
		$('#resicodeSave').attr('name',id)
	}

	function resicodeSave(id){
		var reset = {
			"id" : [ "resicode" ]
		}

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': "{{ csrf_token() }}"
			}
        });

		<?php $prefix = url('/')."/do/"?>
		$.ajax({
			url: "{{ $prefix }}"+id,
				method: 'patch',
				data: {
					resicode: $('#resicode').val(),
				},
				success: function(result){
				$('#addResicode').modal('hide');
				$('.text-left').html('');

				resetSomeInput(reset);
				refreshTable();
				window.location.reload();
				},
				error: function(data) {

				var data = data.responseJSON.errors;

				try{

				error_handler(data,'s');

				}catch(err){

				}

				}
		});
	}

	function editResicode(id){

		$.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
               });

            <?php $prefix = url('/')."/do/"?>
			$.ajax({
				url: "{{ $prefix }}"+id+"/edit",
                  method: 'get',
                  success: function(result){
					$('#u_resicode').val(result.all_data.resi_code)
                  	$('#resicodeUpdate').attr('onclick','update_resicode(\''+result.all_data.id_po+'\')')
                  },
                  error: function(data) {

					try{


					}catch(err){

					}

                  }
		});
	}

	function update_resicode(id){

		$.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
        });

		<?php $prefix = url('/')."/update_resicode/"?>
		$.ajax({
			url: "{{ $prefix }}"+id,
				method: 'patch',
				data: {
					resicode: $('#u_resicode').val(),
				},
				success: function(result){
				$('#editResicode').modal('hide');
				$('.text-left').html('');

				refreshTable();
				window.location.reload();
				},
				error: function(data) {

				var data = data.responseJSON.errors;
				console.log(data);
				try{

				error_handler(data,'u');

				}catch(err){

				}

				}
		});
	}


	// ====================================== MODAL CRITERIA ==============================================

	$('#addCriteria').on('show.bs.modal', function (event) {

	});

	$('#editCriteria').on('hidden.bs.modal', function () {
		$('#select_option_e').html('');
		initial = '';
	});


	$('#criteriaSave').click(function(e){

			   e.preventDefault();

               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
               });

			   	var options = {};

				if($("#options").val() == 3){

					var op_value = [];
					var op_label = [];


					$("input[name='op_value']").each(function(){
						op_value.push($(this).val());

					});
					$("input[name='op_label']").each(function(){
						op_label.push($(this).val());

					});

					jresult = {};
					jresult['type_input'] = 'select_box';
					jresult['data_option'] = new Object();
					for(i=0; i<op_value.length; i++)
					{
						jresult['data_option']["op"+(i+1)] = {'name':op_label[i],'value':op_value[i]};
					}

					if(_.isEmpty(jresult.data_option.op1.name)){
						$('select option:contains("Choose...")').prop('selected',true);
						$('#load_option').attr('hidden',true);
						$('#select_option').html('');
					}else{
						options = jresult;
					}



				}else if($('#options').val() == 2){

					jresult = {};
					jresult['type_input'] = 'number';
					options = jresult;

				}else if($('#options').val() == 1){

					jresult = {};
					jresult['type_input'] = 'text';
					options = jresult;

				}else{
					options = "";
				}


               $.ajax({
                  url: "{{ url('criteria') }}",
                  method: 'post',
                  data: {
                     criteria_name: $('#criteria_name').val(),
                     criteria_code: $('#criteria_code').val(),
					 opt: $('#opt').val(),
					 options: options,
                  },
                  success: function(result){
                  	$('#addCriteria').modal('hide');
                  	$('.text-left').html('');

					window.location.reload();
                    resetInput();
                  },
                  error: function(data) {

					var data = data.responseJSON.errors;
					var lastkey = _.findLastKey(data);

					if(lastkey == 'options' || lastkey == 'opt' || lastkey == 'criteria_code' || lastkey == 'criteria_name'){

					}else{
						data.options = "Format invalid for option";
					}

                  	try{

						error_handler(data,'s');

                  	}catch(err){

                  	}

                  }

              	});

               });

	function editCriteria(id){


			var split = id.split('-');
        	var id_criteria = split[1];

        	$.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
            });

               $.ajax({
                  url: "criteria/"+ id_criteria +"/edit",
                  method: 'get',

                  success: function(result){

					result.data.options = JSON.parse(result.data.options);

					var options = result.data.options[0];
					var length = LengthObj(options.data_option);



                  	$('#criteria_code_e').val(result.data.criteria_code)
                  	$('#criteria_name_e').val(result.data.criteria_name)
					$('#opt_e').val(result.data.optimization_direction)
                  	$('#form-edit-criteria').attr('name',result.data.id_criteria)
					$('#options_e').val((options.type_input == "text" ? 1 : (options.type_input == "number" ? 2 : (options.type_input == 'select_box' ? 3 : 0 ))))

					if($('#options_e').val() == 3){


						var initial = "";

						for (const [key, value] of Object.entries(options.data_option)) {
							initial += "<div id='row_"+last+"'><label class='col-form-label'>Option:</label><br>"+
							"<input type='text' class='form-input' name='op_label' value='"+`${value.name}`+"' placeholder='Name'>"+
							"<input type='text' class='form-input ml-2' value='"+`${value.value}`+"' name='op_value' placeholder='Value'>"+
							"<a href='#' class='form-input btn btn-danger ml-2' id='"+last+"' onClick='delete_option(this.id)'>Delete</a><br/></div>"

							last++;
						}

						$('#select_option_e').append(initial);
						$('#load_option_e').attr('hidden',false);

					}else{
						initial = "";
						$('#load_option_e').attr('hidden',true);
					}


                  },
                  error: function(data) {

          		  }
              	});
	}

	function do_update(id){
		$.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
            });

			var options = {};

				if($("#options_e").val() == 3){

					var op_value = [];
					var op_label = [];


					$("input[name='op_value']").each(function(){
						op_value.push($(this).val());

					});
					$("input[name='op_label']").each(function(){
						op_label.push($(this).val());

					});

					jresult = {};
					jresult['type_input'] = 'select_box';
					jresult['data_option'] = new Object();
					for(i=0; i<op_value.length; i++)
					{
						jresult['data_option']["op"+(i+1)] = {'name':op_label[i],'value':op_value[i]};
					}

					if(_.isEmpty(jresult.data_option.op1.name)){
						$('select option:contains("Choose...")').prop('selected',true);
						$('#load_option').attr('hidden',true);
						$('#select_option').html('');
					}else{
						options = jresult;
					}



				}else if($('#options_e').val() == 2){
					jresult = {};
					jresult['type_input'] = 'number';
					options = jresult;

				}else if($('#options_e').val() == 1){

					jresult = {};
					jresult['type_input'] = 'text';
					options = jresult;

				}else{
					options = "";
				}

               $.ajax({
                  url: "criteria/"+ id,
                  method: 'patch',
              	  data: {
                     criteria_name: $('#criteria_name_e').val(),
                     criteria_code: $('#criteria_code_e').val(),
					 opt: $('#opt_e').val(),
					 options: options,
                  },
                  success: function(result){
                  	$('#editCriteria').modal('hide');
                  	$('.text-left').html('');

					window.location.reload();
                  },
                  error: function(data) {
					var data = data.responseJSON.errors;
					var lastkey = _.findLastKey(data);

					if(lastkey == 'options' || lastkey == 'opt' || lastkey == 'criteria_code' || lastkey == 'criteria_name'){

					}else{
						data.options = "Format invalid for option";
					}
      		  		try{
              			error_handler(data,'u')
                  	}catch(err){

                  	}

          		  }
              	});
	}

	function rewriteTableCriteria(data){
		var newdata = data.all_data;
		var result = newdata.map(function(data,i) {
			var data = [
				(i+1),data.criteria_name,data.criteria_code,data.options,"<div class=\"row\"> <a href=\"#\" id=\"id_criteria-"+data.id+"\" onclick=\"editCriteria(this.id)\" class=\"btn btn-info btn-circle ml-2 mr-2\" data-toggle=\"modal\" data-target=\"#editCriteria\" data-whatever=\"#\">\n <i class=\"fas fa-edit\"></i>\n  </a>\n <form action=\"{{ url('/') }}/criteria/"+data.id+"\" method=\"post\" style=\"float: left;\">\n                          <input type=\"hidden\" name=\"_method\" value=\"delete\">                          <input type=\"hidden\" name=\"_token\" value=\"\ {{ csrf_token() }} \">                   \n                          <button class=\"btn btn-danger btn-circle ml-2\" onclick=\"return confirm('Are you sure want to delete this data?')\"><i class=\" fas fa-trash\"></i></button>\n                        </form></div>"
			];
		  return data;
		});

		var table = $('#dataTable').DataTable();
 		table.clear();
 		table.rows.add(result).draw();
	}

   // =============================== Weight =====================================

	$('#weight').click(function(e){
	   e.preventDefault();

	   $.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': "{{ csrf_token() }}"
	      }
	   });

	   $.ajax({
	      url: "{{ url('weight') }}",
	      method: 'get',
	      success: function(result){
	      	window.location.href = "{{ url('weights') }}";
	      },
	      error: function(data) {

	      	try{

	      		notification(data.responseJSON);

          	}catch(err){

          	}

	      }
	  	});
	});

   function update_weight(data){

	var data = data.split('-');
	var id   = data[1];
	var criteria_row = data[2];
	var criteria_col = data[3];

	$.ajaxSetup({
		headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
		});

        $.ajax({
			url: "weight/"+ id,
			method: 'patch',
			data: {
				weight_value: $('#weight-'+id+'-'+criteria_row+'-'+criteria_col).val(),
				criteria_row: criteria_row,
				criteria_col: criteria_col,
			},
			success: function(result){
    		    window.location.reload(true);
			},
			error: function(data) {
				try{
					let result = {
						success : data.responseJSON.errors.weight_value[0]
					};

					notification(result);
				}catch(err){

				}

			}
		});
   }

   // ============================================== JUDGEMENT ALTERNATIVE ==============================
   function judgement(){
	   $.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': "{{ csrf_token() }}"
	      }
	   });

	   $.ajax({
	      url: "{{ url('judgement') }}",
	      method: 'get',

	      success: function(result){
	      	window.location.href = "{{ url('judgement') }}";
	      },
	      error: function(data) {

	      	try{

	      		notification(data.responseJSON);

          	}catch(err){

          	}

	      }
	  	});
	}

   function tr_criteria(id){
		$.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': "{{ csrf_token() }}"
	      }
	    });

	   <?php $prefix = url('/')."/judgement/"; ?>


	   $.ajax({
	      url: "{{ $prefix }}"+id,
	      method: 'patch',
		  data: {
				judgement_value: $('#'+id).val()
		  },
	      success: function(result){

	      },
	      error: function(data) {

	      	try{

	      		notification(data.responseJSON);

          	}catch(err){

          	}

	      }
	  	});
   }

   $('#priority').click(function(e){
	   e.preventDefault();

	   $.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': "{{ csrf_token() }}"
	      }
	   });

	   $.ajax({
	      url: "{{ url('result') }}",
	      method: 'get',
	      success: function(result){
	      	window.location.href = "{{ url('result') }}";
	      },
	      error: function(data) {

	      	try{

	      		notification(data.responseJSON);

          	}catch(err){

          	}

	      }
	  	});
	});

	$('#delete_all').click(function(e){
		let check = confirm('Are you sure want to delete this data?');

		if(check){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});

			$.ajax({
				url: "{{ url('delete_all') }}",
				method: 'post',
				data: {
					_method: "DELETE",
					_token: "{{ csrf_token() }}",
				},
					success: function(result){
						window.location.reload();
					},
					error: function(data) {


					}
				});
		}else{
			return;
		}
	});

	$('#delete_detail').click(function(e){

		let check = confirm('Are you sure want to delete this data?');

		if(check){
			var id_tr = $('#delete_detail').attr("name");

			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});

			$.ajax({
				url: "{{ url('delete_detail') }}"+"/"+id_tr,
				method: 'post',
				data: {
					_method: "DELETE",
					_token: "{{ csrf_token() }}",
				},
					success: function(result){
						window.location.reload();
					},
					error: function(data) {


					}
				});
		}else{
			return;
		}
	});

	function error_handler(data,status){
		var testing = 0;

		$('.text-left').html('');

		for (let [key, value] of Object.entries(data)) {
			if(status == 'u'){
				testing = `${key}`.split(".");
				console.log(testing[0])
			    if(testing[1]==0){
			        $('#error_u_'+testing[0]).html(`${value}`)
			    }else{
			       	$('#error_u_'+`${key}`).html(`${value}`)
			    }

			}else{

			    testing = `${key}`.split(".");
			    if(testing[1]==0){
			        $('#error_'+testing[0]).html(`${value}`)
			    }else{
			        $('#error_'+`${key}`).html(`${value}`)
			    }

			}
		}
	}

	$('#go_ver').on('click',function(){

		let parameter = "/"+$('#start_date').val()+"_"+$('#end_date').val()+"_"+$('#status').val();

		<?php $prefix = url('/')."/get_po";  ?>

		var url = "{{ $prefix }}"+parameter

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': "{{ csrf_token() }}"
			}
		});

		$.ajax({
			url: url,
			method: 'get',
			success: function(result){
				rewriteTablePurchaseVer(result)
			},
			error: function(data) {
				try{

				}catch(err){

				}
			}
		});
	});

	$('#go').on('click',function(){

      let parameter = "/"+$('#start_date').val()+"_"+$('#end_date').val()+"_"+$('#status').val()+"_";

	  <?php $prefix = url('/')."/get_po";  ?>

		var url = "{{ $prefix }}"+parameter

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': "{{ csrf_token() }}"
			}
		});

		$.ajax({
			url: url,
			method: 'get',
			success: function(result){
				rewriteTablePurchase(result)
			},
			error: function(data) {
				try{

				}catch(err){

				}
			}
		});
    });

	$('#go_delv').on('click',function(){

      let parameter = "/"+$('#start_date').val()+"_"+$('#end_date').val()+"_"+$('#status').val()+"_"+1+"_"+1+"/";

	  <?php $prefix = url('/')."/get_po";  ?>

		var url = "{{ $prefix }}"+parameter

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': "{{ csrf_token() }}"
			}
		});

		$.ajax({
			url: url,
			method: 'get',
			success: function(result){
				rewriteTableDelv(result)
			},
			error: function(data) {
				try{

				}catch(err){

				}
			}
		});
    });

	$('#download_history').on('click',function(){

	  <?php $prefix = url('/')."/download_history";  ?>

		var url = "{{ $prefix }}"

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': "{{ csrf_token() }}"
			}
		});

		$.ajax({
			url: url,
			method: 'get',
			success: function(result, status, xhr){
				window.location = '{{ $prefix }}';
			},
			error: function(data) {
				try{
					console.log(data);
				}catch(err){

				}
			}
		});
    });

	function rewriteTablePurchaseVer(data){
		var newdata = data.all_data;
		var result = newdata.map(function(data,i) {

			var data = [
				(i+1),data.customer_name,data.id_po,data.date_order,data.deadline,data.username,"<div class=\"row\"><a href=\"{{ url('/') }}/po_view/"+ data.id +"\" class=\"btn btn-info btn-circle ml-2\"><i class=\"fas fa-eye\"></i></a>"+(data.status == 0 ? "<button class=\"btn btn-success btn-circle ml-2\" style=\"margin-bottom: 20px;\" id=\""+data.id+"\" name=\"verify\" onclick=\"update_data(this.id,this.name)\"><i class=\"fas fa-check-circle\"></i></button><button class=\"btn btn-danger btn-circle ml-2\" style=\"margin-bottom: 20px;\" id=\""+data.id+"\" name=\"reject_po\" onclick=\"update_data(this.id,this.name)\"><i class=\"fa fa-ban\" aria-hidden=\"true\"></i></button>" : (data.status == 1 ? "<button class=\"btn btn-danger btn-circle ml-4 mr-3\" style=\"margin-bottom: 20px;\" id=\""+data.id+"\" name=\"verify_cancel\" onclick=\"update_data(this.id,this.name)\"><i class=\"fas fa-window-close\"></i></button>" : "")+(data.status == 2 ? "<button class=\"btn btn-danger btn-circle ml-4 mr-3\" style=\"margin-bottom: 20px;\" id=\""+data.id+"\" name=\"verify_cancel\" onclick=\"update_data(this.id,this.name)\"><i class=\"fas fa-window-close\"></i></button>" : "")+"</div>")
			];
		  return data;
		});

		var table = $('#dataTable').DataTable();
 		table.clear();
 		table.rows.add(result).draw();
	}

	function rewriteTablePurchase(data){
		var newdata = data.all_data;
		var result = newdata.map(function(data,i) {
			var data = [
				(i+1),data.customer_name,data.id_po,data.date_order,data.deadline,(data.status == 0 ? "Waiting" : (data.status == 1  ? "Is Doing":"Done")),"<div class=\"row\">"+(data.status == 0 || data.status==1 || data.status==2 ? "<a href=\"{{ url('/') }}/make_report/"+data.id+"\" class=\"btn btn-primary btn-circle ml-2\"> <i class=\"fas fa-download\"></i></a> " : " ")+" "+(data.status==0 || data.status==1 ? "<a href=\"{{ url('/') }}/po/"+data.id+"/edit\" class=\"btn btn-info btn-circle ml-2\"><i class=\"fas fa-edit\"></i></a>":"")+" <a href=\"#\" id=\""+data.id+"\" name=\"po\" class=\"btn btn-danger btn-circle ml-2\" onclick=\"delete_data(this.id,this.name)\"><i class=\" fas fa-trash\"></i></a></div>"
			];
		  return data;
		});

		var table = $('#dataTable').DataTable();
 		table.clear();
 		table.rows.add(result).draw();
	}

	function rewriteTableDelv(data){
		var check = data.check
		var newdata = data.all_data
		var result = newdata.map(function(data,i) {
			var data = [
				(i+1),data.customer_name,data.id_po,data.date_order,data.deadline,(data.status == 0 ? "Waiting" : (data.status == 1  ? "Is Doing":"Done")),(data.resi_code ? data.resi_code : "" ),"<div class=\"row\"><a href='{{ url('/') }}/do/"+data.id_po+"' class='btn btn-info btn-circle ml-2'><i class='fas fa-eye'></i></a>"+(data.status == 1 ?   (_.isMatch(check, { 'id': data.id_po }) ? "<a href='#' class='btn btn-success btn-circle ml-1' id='"+data.id_po+"' onclick='addResicode(this.id)' data-toggle='modal' data-target='#addResicode' data-whatever='#'><i class='fas fa-plus'></i></a>" : "") : "<a href='#' class='btn btn-info btn-circle ml-1' id='"+data.id_po+"' onclick='editResicode(this.id)' data-toggle='modal' data-target='#editResicode' data-whatever='#'><i class='fas fa-edit'></i></a><button class='btn btn-danger btn-circle ml-1' style='margin-bottom: 20px;' id='"+data.id_po+"' name='do_cancel' onclick='update_data(this.id,this.name)'><i class='fas fa-window-close'></i></button>")+"</div>"
			];
		  return data;
		});

		var table = $('#dataTable').DataTable();
 		table.clear();
 		table.rows.add(result).draw();
	}

	// Loading `lodash.fp.js` converts `_` to its fp variant.
	_.defaults({ 'a': 2, 'b': 2 })({ 'a': 1 });
	// ➜ { 'a': 1, 'b': 2 }

	// Use `noConflict` to restore the pre-fp variant.
	var fp = _.noConflict();

	_.defaults({ 'a': 1 }, { 'a': 2, 'b': 2 });
	// ➜ { 'a': 1, 'b': 2 }
	fp.defaults({ 'a': 2, 'b': 2 })({ 'a': 1 });
	// ➜ { 'a': 1, 'b': 2 }

</script>
