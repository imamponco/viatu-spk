@extends('backend.dashboard')
@section('title','Viatushop - Judgement Alternative')
@section('content')

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">@yield('title')</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered display" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Alternative</th>
                      @foreach($criterias as $row)
                      <th>{{ $row->criteria_name }}</th>
                      @endforeach
                    </tr>
                  </thead>
                  <tbody>
                   <?php $count=1; $limit = count($tr); $jmlr=0; $true=0;?>
                    @foreach($po as $row)
                    <tr id="row-{{ $row->id_po }}">
                      <th>{{ $count }} </th>
                      <td><a href="{{ url('judgement/'.$row->id_po) }}">{{ $row->id_po }}</a></td>
                        @foreach($tr as $t)
                            @if($t->id_po == $row->id_po)
                            @php
                            $t->options = json_decode($t->options);
                            $t->options = json_decode($t->options);
                            @endphp

                              @if (isset($t->options[0]->type_input))
                              {{-- {{ dd($t->options[0]->type_input) }} --}}
                                @switch($t->options[0]->type_input)
                                    @case('select_box')
                                        @php
                                        $options = (array)$t->options[0];
                                        $options["data_option"] = (array)$options["data_option"];
                                        // dd($options["data_option"]);
                                        @endphp
                                      <td>
                                        <div class="form-group">

                                        @if($t->criteria_code !='WP-01' && $t->criteria_code != 'JS-01' && $t->criteria_code != 'JMS-01' && $t->criteria_code !='TKP-01')
                                            <select class="custom-select" id="judgement_{{ $t->id_po  }}_{{ $t->criteria_code }}" onchange="tr_criteria(this.id)">
                                        @else
                                            <select class="custom-select" id="judgement_{{ $t->id_po  }}_{{ $t->criteria_code }}" onchange="#" disabled>
                                        @endif

                                          {{-- current --}}
                                          @for ($i = 1; $i < count($options["data_option"])+1; $i++)
                                            @if($t->min_value."-".$t->max_value == $options["data_option"]["op".$i]->value || $t->min_value == $options["data_option"]["op".$i]->value)
                                              <option selected="selected" value="{{ $t->min_value."-".$t->max_value }}">{{ $options["data_option"]["op".$i]->name }}</option>
                                              @php $true=2; @endphp
                                            @endif
                                          @endfor
                                          @if($true!=2)
                                          <option selected="selected" value="0-0">Choose...</option>
                                          @endif
                                          {{-- option --}}
                                          @for ($i = 1; $i < count($options["data_option"])+1; $i++)
                                            <option value="{{ $options["data_option"]["op".$i]->value  }}">{{ $options["data_option"]["op".$i]->name  }}</option>
                                          @endfor
                                          @php $true=0;  @endphp
                                          </select>
                                        </div>
                                      </td>
                                        @break
                                    @case('text')
                                        <td>
                                          <div class="form-group">

                                            @if($t->criteria_code !='WP-01' && $t->criteria_code != 'JS-01' && $t->criteria_code != 'JMS-01' && $t->criteria_code !='TKP-01')
                                                <input type="{{ $t->options[0]->type_input }}" class="form-control" id="judgement_{{ $t->id_po }}_{{ $t->criteria_code }}" value="{{ $t->min_value }}-{{ $t->max_value }}" onchange="tr_criteria(this.id)">
                                            @else
                                                <input type="{{ $t->options[0]->type_input }}" class="form-control" id="judgement_{{ $t->id_po }}_{{ $t->criteria_code }}" value="{{ $t->min_value }}-{{ $t->max_value }}" onchange="#" disabled>
                                            @endif

                                          </div>
                                        </td>
                                        @break
                                    @case('number')
                                      <td>
                                        <div class="form-group">
                                        {{-- {{dd($t->criteria_code)}} --}}
                                        @if($t->criteria_code !='WP-01' && $t->criteria_code != 'JS-01' && $t->criteria_code != 'JMS-01' && $t->criteria_code !='TKP-01')
                                            <input type="{{ $t->options[0]->type_input }}" class="form-control" id="judgement_{{ $t->id_po }}_{{ $t->criteria_code }}" value="{{ $t->min_value }}" onchange="tr_criteria(this.id)">
                                        @else
                                            <input type="{{ $t->options[0]->type_input }}" class="form-control" id="judgement_{{ $t->id_po }}_{{ $t->criteria_code }}" value="{{ $t->min_value }}" onchange="#" disabled>
                                        @endif

                                        </div>
                                       </td>
                                      @break

                                    @default
                                @endswitch
                              @endif
                            @endif
                          @endforeach
                        <?php $count++; ?>
                      </tr>
                    @endforeach
                  </tbody>

                </table>
              </div>
            </div>
          </div>

          <script>




          </script>
      @endsection
