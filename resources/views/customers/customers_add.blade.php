@extends('backend.dashboard')
@section('title','Viatushop - Customers')
@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-6">Form Data Customers</h1>
              </div>
             <!--  @if($errors->any())
                  {{ implode('', $errors->all('<div>:message</div>')) }}
              @endif -->
              @if(\Session::has('alert-success'))
                  <div class="alert alert-success" style="padding: 2px;">
                      <div>{{Session::get('alert-success')}}</div>
                  </div>
              @endif
              <form class="user" enctype="multipart/form-data" action="{{ url('customers') }}" method="post">
                @csrf
                 <div class="form-group">
                  <label for="name_label">Name</label>
                  @if($errors->has('name'))
                      <div class="text-left" style="padding: 2px; color:red;">{{ $errors->first('name') }}</div>
                  @endif
                  <input id="name_label" type="text" name="name" value="{{ old('name') }}" class="form-control"  placeholder="Enter your name">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  @if($errors->has('customer_email'))
                      <div class="text-left" style="padding: 2px; color:red;">{{ $errors->first('customer_email') }}</div>
                  @endif
                  <input type="text" name="customer_email" value="{{ old('customer_email') }}" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="form-group">
                  <label for="name_label">Phone</label>
                  @if($errors->has('customer_phone'))
                      <div class="text-left" style="padding: 2px; color:red;">{{ $errors->first('customer_phone') }}</div>
                  @endif
                  <input id="name_label" type="text" name="customer_phone" value="{{ old('customer_phone') }}" class="form-control"  placeholder="Enter phone number">
                </div>
                <div class="form-group">
                  <label for="name_label">Company</label>
                  @if($errors->has('company'))
                      <div class="text-left" style="padding: 2px; color:red;">{{ $errors->first('company') }}</div>
                  @endif
                  <input id="name_label" type="text" name="company" value="{{ old('company') }}" class="form-control"  placeholder="Enter company name">
                </div>
                 <div class="form-group">
                  <label for="exampleInputEmail1">Address</label>
                  @if($errors->has('address'))
                      <div class="text-left" style="padding: 2px; color:red;">{{ $errors->first('address') }}</div>
                  @endif
                  <textarea  name="address" class="form-control" id="validationTextarea" placeholder="Address">{{ old('address') }}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection