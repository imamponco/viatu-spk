@extends('backend.dashboard')
@section('title','Viatushop - Customers')
@section('content')
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">@yield('title')</h6>
              
            </div>
            <div class="card-body">
              <a href="{{ url('customers/create') }}" class="btn btn-success btn-square" style="margin-bottom: 20px;">
                  <i class="fas fa-plus"></i>
              </a>
        
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                
                    <tr>
                      <th>No</th>
                      <th>Name</th>
                      <th>Phone</th>
                      <th>Email</th>
                      <th>Company</th>
                      <th>Address</th> 
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
                    @foreach($customers as $row)
                    <tr>
                    
                      <td>{{ $no }}</td>
                      <td>{{ $row->customer_name }}</td>
                      <td>{{ $row->customer_phone }}</td>
                      <td>{{ $row->customer_email }}</td>
                      <td>{{ $row->company }}</td>
                      <td>{{ $row->address }}</td>
                      <td>
                        <div class="row">
                          <a href="{{ URL::to('customers/' . $row->id_customer . '/edit') }}" class="btn btn-info btn-circle ml-1 mr-1">
                            <i class="fas fa-edit"></i>
                          </a>
        
                          <form action="{{ URL::to('customers/'. $row->id_customer) }}" method="post">
                            @method('DELETE')
                            @csrf                   
                            <button class="btn btn-danger btn-circle" onclick="return confirm('Are you sure want to delete this data?')"><i class=" fas fa-trash"></i></button>
                          </form>
                        </div>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                  </tbody>
                 
                </table>
              </div>
            </div>
          </div>
      @endsection