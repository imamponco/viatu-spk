<!DOCTYPE html>
<html>
<head>
	<title>Viatu Quotation {{ $current_header[0]->customer_name }}</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <style>
    .height {
        min-height: 200px;
    }

    .icon {
        font-size: 47px;
        color: #5CB85C;
    }

    .iconbig {
        font-size: 77px;
        color: #5CB85C;
    }

    .table > tbody > tr > .emptyrow {
        border-top: none;
    }

    .table > thead > tr > .emptyrow {
        border-bottom: none;
    }

    .table > tbody > tr > .highrow {
        border-top: 3px solid;
    }

    .page-break {
        page-break-after: always;
    }
    </style>

</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-xs-12">
            <div class="text-left">
                <div class="row">
                    <div class="col-8">
                        <h1>Viatu Shop Indonesia</h1>
                        <h2>We Support Your Style</h2>
                        <p>Jl. Cipedak Raya Rt 009 Rw 009 No 33,<br> 
                        Srengseng Sawah Jagakarsa Jakarta Selatan 12640 <br>
                        Telp. 0856 915 38 762 Email : info@viatushop.com  </p>
                    </div>
                   
                    <div class="col-20">
                        <div class="ml-8 float-right text-right">
                           <img src="{{ public_path('images/viatu.png') }}" alt="logo" width="220px" height="170px"> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                     <div class="text-left">
                        <b>DATE        : </b> <br> 
                        <b>PO CODE   :</b> <br> 
                        <b>CUSTOMER ID :</b>
                     </div>
                    </div>
                    <div class="col">
                    <div class="float-right text-right">
                        <b>{{ $current_header[0]->date_order }}</b><br>
                        <b>#{{ $current_header[0]->id_po }}</b><br>
                        <b>{{ $current_header[0]->id_customer }}</b>
                     </div>
                    </div>
                </div>
            </div>
           
            <hr>
            <div class="row">
                <div class="col-xs-12 col-md-3 col-lg-3 pull-left">
                    <div class="panel panel-default height">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col">
                                    Name    : {{ $current_header[0]->customer_name }}<br>
                                    Company : {{ $current_header[0]->company }}<br>
                                    Address : <br>
                                    {{ $current_header[0]->address }}<br>
                                </div>
                                <div class="col">
                                    <div class="text-right">
                                        Quotation Valid Until :   <b>{{ $current_header[0]->deadline }} </b><br>
                                        Prepared by : <b>{{ $current_header[0]->username }}</b>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="text-center"><strong>Order Summary</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <td><strong>No</strong></td>
                                    <td><strong>Product Name</strong></td>
                                    <td class="text-center"><strong>Description</strong></td>
                                    <td class="text-center"><strong>Unit Price</strong></td>
                                    <td class="text-center"><strong>Unit</strong></td>
                                    <td class="text-right"><strong>Amount</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1; $total=0;?>
                                @foreach($current_shoes as $shoe)
                                    <?php $amount = ($shoe->current_price * $shoe->unit); ?>
                                    <tr>
                                        <td>{{ $no }}</td>
                                        <td>{{ $shoe->model_name }}</td>
                                        <td class="text-center">{{ $shoe->shoes_description }}</td>
                                        <td class="text-center">Rp.{{ number_format($shoe->current_price ,2, ".", ",") }}</td>
                                        <td class="text-center">{{ $shoe->unit }}</td>
                                        <td class="text-right">Rp.{{ number_format($amount ,2, ".", ",") }}</td>
                                        <?php $total += $amount; ?>
                                        <?php $no++; ?>
                                    </tr>
                                @endforeach
                                @foreach($current_additional as $add)
                                <?php $amount = ($add->add_price * $add->add_unit); ?>
                                    <tr>
                                        <td>{{ $no }}</td>
                                        <td>{{ $add->add_name }}</td>
                                        <td class="text-center">{{ $add->add_description }}</td>
                                        <td class="text-center">Rp.{{ number_format($add->add_price ,2, ".", ",")  }}</td>
                                        <td class="text-center">{{ $add->add_unit }}</td>
                                        <td class="text-right">Rp.{{ number_format($amount ,2, ".", ",") }}</td>
                                        <?php $total += $amount; ?>
                                        <?php $no++; ?>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td class="highrow"></td>
                                    <td class="highrow"></td>
                                    <td class="highrow"></td>
                                    <td class="highrow"></td>
                                    <td class="highrow text-center"><strong>Subtotal</strong></td>
                                    <td class="highrow text-right">Rp.{{ number_format($total ,2, ".", ",") }}</td>
                                </tr>
                                <tr>
                                    <td class="emptyrow"><i class="fa fa-barcode iconbig"></i></td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow text-center"><strong>Total</strong></td>
                                    <td class="emptyrow text-right">Rp.{{ number_format($total ,2, ".", ",") }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="blog-post">
                <div class="text-left">
                    Hormat kami,    <br><br>
                    <p><b>Viatu Shop Indonesia.</br></p>
                </div>
            </div><!-- /.blog-post -->
        </div>
    </div>

    <div class="page-break"></div>
    @for($i=0; $i < $amount_model; $i++)
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="text-center"><strong>Model {{ $detail_data[$i][0]->model_name }}</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                    <tr>
                                        <td><strong>No</strong></td>
                                        <td><strong>Name</strong></td>
                                        <td class="text-center"><strong>Colour</strong></td>
                                        <td class="text-center"><strong>Size</strong></td>
                                        <td class="text-right"><strong>Width</strong></td>
                                        <td class="text-right"><strong>Length</strong></td>
                                        <td class="text-center"><strong>Description</strong></td>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $no = 1; $total=0;?>
                                    @foreach($detail_data[$i] as $det)
                                        <?php $amount = (count($detail_data[$i])); ?>
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td class="text-center">{{ $det->detail_name }}</td>
                                            <td class="text-center">{{ $det->colour }}</td>
                                            <td class="text-center">{{ $det->size }}</td>
                                            <td class="text-center">{{ $det->width }}</td>
                                            <td class="text-center">{{ $det->length }}</td>
                                            <td class="text-right">{{  $det->description }}</td>
                                            <?php $total += $amount; ?>
                                            <?php $no++; ?>
                                        </tr>
                                    @endforeach
                                    <?php $id = $detail_data[$i][0]->id_tr_shoes;
                                          $amount_size = count($size[$id]); 
                                    ?>
                                    @for($a=0; $a<$amount_size; $a++)
                                        @if($a == 0)
                                            <tr>
                                                <td class="highrow text-center">Size {{ $size[$id][$a]->size }}</td>
                                                <td class="highrow"></td>
                                                <td class="highrow"></td>
                                                <td class="highrow"></td>
                                                <td class="highrow"></td>
                                                <td class="highrow"></td>
                                                <td class="highrow text-right">{{ $size[$id][$a]->amount }}</td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td class="text-center">Size {{ $size[$id][$a]->size }}</td>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="float-right text-right">{{ $size[$id][$a]->amount }}</td>
                                            </tr>
                                        @endif
                                    @endfor
                                    <tr>
                                        <td class="emptyrow text-center"><strong>Total</strong></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow text-right">{{ $amount }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $amount = 0; ?>
    <div class="page-break"></div>
    @endfor

    <div class="row">
        <div class="blog-post">
                <h2 class="text-center mb-2">Syarat dan Ketentuan</h2>
                    <p style="text-align:justify;">Berikut ini merupakan syarat dan ketentuan yang telah disepakati 
                    bersama dalam melakukan pemesanan jasa pembuatan sepatu di Viatu Shop Indonesia,
                    harap dibaca dan disepakati.
                    </p>
                    <ol>
                    <li>Pembayaran uang muka minimal 50% dapat dilakukan melalui pembayaran antar bank.</li>
                    <li>Barang yang sudah dipesan tidak dapat dikembalikan.</li>
                    <li>Uang muka yang telah diterima tidak bisa diminta kembali</li>
                    <li>Kami memberikan layanan perbaikan produk jika ditemukan cacat pada produk dalam jangka waktu 2 minggu setelah produk diterima.</li>
                    <li>Kami akan mengkonfirmasi pemesanan sebagai acuan dalam pembuatan produk, 
                    jika produk yang telah dibuat telah sesuai dengan pemesanan yang kami terima,
                     maka perbaikan produk dikenakan biaya</li>
                    </ol>
                    <p>Pembayaran melalui bank dapat dilakukan melalui:</p>
                    <ol>
                        {{-- <li>BCA NO.REK: 5865286144 A. N IMAM TAUFIQ PONCO UTOMO</li>
                        <li>MANDIRI NO. REK 127-00-0734463-1 A. N IMAM TAUFIQ PONCO</li>
                        <li>BSI NO. REK 7149109026 A. N IMAM TAUFIQ PONCO</li> --}}

                        <li>BCA NO.REK: 715.058.0875 an Abdul Haris Dwi</li>
                        <li>MANDIRI NO.REK: 124.00.0625896.7 a/n Abdul Haris Dwi</li>
                    </ol>
                    <p class="mb-10">Demikian syarat ketentuan pemesanan yang berlaku, mohon disepakati bersama.</p><br><br><br>

                    Hormat kami,    <br><br><br>
                    <p><b>Viatu Shop Indonesia.</br></p>
        </div><!-- /.blog-post -->
    </div>

</div>
 
</body>
</html>


