@extends('backend.dashboard')
@section('title','Viatushop - Shoes Model')
@section('content')

          <!-- DataTales Example -->

          <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Viatushop - Shoes Model</h6>
              </div>
            <div class="card-body">
              <a href="#" onclick="" class="btn btn-success btn-square" data-toggle="modal" data-target="#shoesModal" data-whatever="#" style="margin-bottom: 20px;">
                <i class="fas fa-plus"></i>
              </a>

              <div class="table-responsive">
                <table class="table table-bordered display" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Model Name</th>
                      <th>Model Code</th>
                      <th>Unit Price</th>
                      <th>Description</th>
                      <th style="width:120px;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
                    @foreach($shoes_model as $row)
                    <tr>
                      <td>{{ $no }}</td>
                      <td>{{ $row->model_name }}</td>
                      <td>{{ $row->model_code }}</td>
                      <td>{{ $row->unit_price  }}</td>
                      <td>{{ $row->shoes_description }}</td>
                      <td>
                        <div class="row">
                          <a href="#" onclick="editShoesModel(this.id)" id="{{ $row->id_shoes_model }}" class="btn btn-info btn-circle ml-2 mr-2">
                            <i class="fas fa-edit"></i>
                          </a>
                          <a href="#" id="{{ $row->id_shoes_model }}" name="shoe" class="btn btn-danger btn-circle" onclick="delete_data(this.id,this.name)">
                            <i class=" fas fa-trash"></i>
                          </a>
                      </div>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>

      <!-- Shoes Modal -->
      <div class="modal fade bd-example-modal-lg" id="shoesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Shoes Model</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <form id="shoes_model_form">
                    <label class="model-image-uploaded" for="uploaded">Uploaded Image</label>
                    <div class="model-image-uploaded" id="uploaded">
                      @foreach($images as $img)
                        @if(count($images) < 1)
                        <img src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17299ff5b6f%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17299ff5b6f%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" class="rounded" alt="...">
                        @else
                        <img onclick="uploaded_thumbnail();" src="{{ asset(url('assets/images/'.$img->img_path)) }}" class='img-fluid img-thumbnail m-3' style='width:200px; height:200px;' alt="...">
                        <a href="#" onClick="delete_image('{{ $img->id_images}}','image','model-image-uploaded')" class="close" style="position:absolute; margin: 0 0 0 -15px;">&times;</a>
                        @endif
                      @endforeach
                    </div>
                    <label class="model-image-thumbnail" for="thumbnail">Preview</label>
                    <div class="model-image-thumbnail" id="thumbnail-uploaded">
                      <img src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17299ff5b6f%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17299ff5b6f%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" class="rounded" alt="...">
                    </div>
                      <div class="form-group">
                        <label for="model-image" class="col-form-label">Model Images:</label>
                        <div class="text-left" id="error_file" style="padding: 2px; color:red;"></div>
                        <div class="input-group mb-3">
                          <div class="custom-file">
                            <input type="file" name="file[]" class="custom-file-input" id="model-image" multiple>
                            <label class="custom-file-label" for="model-image">Choose file</label>
                          </div>
                          <div class="input-group-append">
                            <span class="input-group-text" id="upload_image" onclick="upload_image();">Upload</span>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="text-left" id="error_model_name" style="padding: 2px; color:red;"></div>
                        <label for="model-name" class="col-form-label">Model Name:</label>
                        <input type="text" class="form-control" id="model-name">
                      </div>
                      <div class="form-group">
                        <div class="text-left" id="error_model_code" style="padding: 2px; color:red;"></div>
                        <label for="model-code" class="col-form-label">Model Code:</label>
                        <input type="text" class="form-control" id="model-code">
                      </div>
                      <div class="form-group">
                        <div class="text-left" id="error_difficulty" style="padding: 2px; color:red;"></div>
                        <label for="difficulty" class="col-form-label">Difficulty:</label>
                        <select class="custom-select" id="difficulty" name="difficulty">
                            <option value="" selected>Choose...</option>
                            <option value="1">Mudah</option>
                            <option value="2">Sedang</option>
                            <option value="3">Sulit</option>
                          </select>
                      </div>
                      <div class="form-group">
                      <div class="text-left" id="error_unit_price" style="padding: 2px; color:red;"></div>
                        <label for="unit-price" class="col-form-label">Unit Price:</label>
                        <input type="text" class="form-control" min="1" id="unit-price">
                      </div>
                      <div class="form-group">
                        <div class="text-left" id="error_description" style="padding: 2px; color:red;"></div>
                        <label for="description" class="col-form-label">Description:</label>
                        <textarea class="form-control" id="description-shoes"></textarea>
                  </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" id="model_shoes_save" class="btn btn-primary">Save</button>
                  </div>
              </div>
            </div>
          </div>
        </div>

        <!-- edit shoes modal -->
        <div class="modal fade bd-example-modal-lg" id="editShoesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Shoes Model</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <form id="update_model_form">
                    <label class="model-current-image" for="uploaded">Current Image</label>
                    <div class="model-current-image" id="model-current-image">
                    </div>
                    <label class="model-image-thumbnail" for="update-thumbnail">Preview</label>
                    <div class="model-image-thumbnail" id="thumbnail-model-current-image">
                      <img src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17299ff5b6f%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17299ff5b6f%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" class="rounded" alt="...">
                    </div>
                      <div class="form-group">
                        <label for="model-image" class="col-form-label">Model Image:</label>
                        <div class="text-left" id="error_u_update_file" style="padding: 2px; color:red;"></div>
                        <div class="input-group mb-3">
                          <div class="custom-file">
                            <input type="file" name="update_file[]" class="custom-file-input" id="update-image" multiple>
                            <label class="custom-file-label" for="model-image">Choose file</label>
                          </div>
                          <div class="input-group-append">
                            <span class="input-group-text" id="update_image" onclick="update_image();">Upload</span>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="text-left" id="error_u_model_name" style="padding: 2px; color:red;"></div>
                        <label for="current-model-name" class="col-form-label">Model Name:</label>
                        <input type="text" class="form-control" id="current-model-name">
                      </div>
                      <div class="form-group">
                        <div class="text-left" id="error_u_model_code" style="padding: 2px; color:red;"></div>
                        <label for="current-model-code" class="col-form-label">Model Code:</label>
                        <input type="text" class="form-control" id="current-model-code">
                      </div>
                      <div class="form-group">
                        <div class="text-left" id="error_u_difficulty" style="padding: 2px; color:red;"></div>
                        <label for="current-difficulty" class="col-form-label">Difficulty:</label>
                        <select class="custom-select" id="current-difficulty" name="current-difficulty">
                            <option value="" selected>Choose...</option>
                            <option value="1">Mudah</option>
                            <option value="2">Sedang</option>
                            <option value="3">Sulit</option>
                          </select>
                      </div>
                      <div class="form-group">
                      <div class="text-left" id="error_u_unit_price" style="padding: 2px; color:red;"></div>
                        <label for="unit-price" class="col-form-label">Unit Price:</label>
                        <input type="text" class="form-control" min="1" id="current-unit-price">
                      </div>
                      <div class="form-group">
                        <div class="text-left" id="error_u_description" style="padding: 2px; color:red;"></div>
                        <label for="description" class="col-form-label">Description:</label>
                        <textarea class="form-control" id="current-description-shoes"></textarea>
                  </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" id="model_shoes_update" onclick="update_shoes(this.id)" class="btn btn-primary">Update</button>
                  </div>
              </div>
            </div>
          </div>
        </div>

         <!-- =========================== CAROUSEL =========================== -->
        <div class="modal fade bd-example-modal-lg" id="carousel_thumbnail" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Preview</h5>
              <button type="button" class="close" onclick="close_slideshow();" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <div id="mdb-lightbox-ui"></div>
              <div id="carousel_indicators" class="carousel slide" data-ride="carousel">
                <ol id="thumbnail_indicators" class="carousel-indicators">
                  <li data-target="#carousel_indicators" data-slide-to="0" class="active"></li>
                </ol>
                <div id="thumbnail_item" class="carousel-inner">
                  <div class="carousel-item active">
                    <img class="d-block w-100" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17299ff5b6f%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17299ff5b6f%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="First slide">
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carousel_indicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel_indicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" onclick="close_slideshow();">Close</button>
            </div>
          </div>
        </div>
      </div>

      <?php $limit = count($images); $count = 0; ?>
      <div class="modal fade bd-example-modal-lg" id="carousel_uploaded" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Uploaded Image</h5>
              <button type="button" class="close" onclick="close_uploaded_thumbnail();" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <div id="mdb-lightbox-ui"></div>
              <div id="carousel_uploaded_indicators" class="carousel slide" data-ride="carousel">
                <ol id="uploaded_indicators" class="carousel-indicators">
                  @foreach($images as $img)
                    @if($count == 0)
                    <li data-target="#carousel_uploaded_indicators" data-slide-to="{{ $count }}" class="active"></li>
                    @else
                      <li data-target="#carousel_uploaded_indicators" data-slide-to="{{ $count }}" ></li>
                    @endif
                    <?php $count++; ?>
                  @endforeach
                  <?php $count=0; ?>
                </ol>
                <div id="uploaded_item" class="carousel-inner">
                @foreach($images as $img)
                  @if($count==0)
                    <div class="carousel-item active">
                      <img id="uploaded-model-{{ $count }}" class="d-block w-100" src="{{ asset(url('assets/images/'.$img->img_path)) }}" alt="First slide">
                    </div>
                  @else
                    <div class="carousel-item">
                      <img id="uploaded-model-{{ $count }}" class="d-block w-100" src="{{ asset(url('assets/images/'.$img->img_path)) }}" alt="Slide">
                    </div>
                  @endif
                  <?php $count++; ?>
                @endforeach
                </div>
                <a class="carousel-control-prev" href="#carousel_uploaded_indicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel_uploaded_indicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" onclick="close_uploaded_thumbnail();">Close</button>
            </div>
          </div>
        </div>
      </div>
      @endsection
