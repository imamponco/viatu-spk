@extends('backend.dashboard')
@section('title','Viatushop - Delivery Report')
@section('content')

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">@yield('title')</h6>
              
            </div>
            <div class="card-body">
              <div class="row mb-4">
                <div class="col-2">
                  <label for="first_date">Start Date:</label><br>
                  <input class="form-control" type="date" id="start_date">
                </div>
                <div class="col-2">
                  <label for="second_date">End Date:</label><br>
                  <input class="form-control" type="date" id="end_date">
                </div>
                <div class="col-2">
                  <label for="status">Status:</label>
                  <select class="form-control" id="status">
                    <option selected value="1">Choose...</option>
                    <option value="1">Is Doing</option>
                    <option value="2">Done</option>
                </select>
                </div>
                <div class="col-2">
                  <label for="action">Action:</label><br>
                  <button class="btn btn-success btn-square" id="go_delv">Go</>
                </div>
              </div>

              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                
                    <tr>
                      <th>No</th>
                      <th>Customer Name</th>
                      <th>Po Code</th>
                      <th>Date Order</th>
                      <th>Deadline</th>
                      <th>Status</th>
                      <th>Resi Code</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
                    @foreach($po as $row)
                    <tr>
                      <td>{{ $no }}</td>
                      <td>{{ $row->customer_name }}</td>
                      <td>{{ $row->id_po }}</td>
                      <td>{{ $row->date_order }}</td>
                      <td>{{ $row->deadline }}</td>
                      <td>@switch($row->status)
                        @case(0)
                          Waiting
                          @break
                        @case(1)
                          Is Doing
                          @break
                        @case(2)
                          Done
                          @break
                        @endswitch</td>
                      <td>{{ $row->resi_code }}</td>
                      <td>
                      <div class="row">
                        <a href="{{ URL::to('do/'. $row->id ) }}" class="btn btn-info btn-circle ml-2">
                          <i class="fas fa-eye"></i>
                        </a>
                        @if ($row->status==1)
                          @php 
                             $current = DB::table('tr_shoes_models as t')
                                                ->select(DB::raw('COUNT(d.id_do) as not_check'))
                                                ->leftJoin(DB::raw('(SELECT * FROM detail_orders) as d'),'t.id_tr_shoes','=','d.id_tr_shoes')
                                                ->leftJoin('shoes_models as s','t.id_shoes_model','=','s.id_shoes_model')
                                                ->groupBy('t.id_tr_shoes')
                                                ->orderBy('t.created_at','desc')
                                                ->where('t.id_po',$row->id)
                                                ->where('d.quality_status',0)
                                                ->get()
                                                ->toArray();
                          @endphp
                          @if(empty($current[0]->not_check))
                            <a href="#" class="btn btn-success btn-circle ml-1" id="{{ $row->id }}" onclick="addResicode(this.id)" data-toggle="modal" data-target="#addResicode" data-whatever="#">
                              <i class="fas fa-plus"></i>
                            </a>
                          @endif
                        @else
                          <a href="#" class="btn btn-info btn-circle ml-1" id="{{ $row->id }}" onclick="editResicode(this.id)" data-toggle="modal" data-target="#editResicode" data-whatever="#">
                            <i class="fas fa-edit"></i>
                          </a>
                            <button class="btn btn-danger btn-circle ml-1" style="margin-bottom: 20px;" id="{{ $row->id }}" name="do_cancel" onclick="update_data(this.id,this.name)">
                              <i class="fas fa-window-close"></i>
                            </button>
                        @endif
                        </div>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                  </tbody>
                 
                </table>
              </div>
            </div>
          </div>

           <!-- Add Modal -->
           <div class="modal fade bd-example-modal-lg" id="addResicode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Add Resi Code</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form class="add" action="#" id="resicode_form">        
                    <div class="form-group">
                      <label for="name" class="col-form-label">Resi Code:</label>
                      <div class="text-left" id="error_resicode" style="padding: 2px; color:red;"></div>
                      <input type="text" class="form-control" id="resicode">
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="resicodeSave" onclick="resicodeSave(this.name)" name="{{ "hello" }}">Save</button>
                </div>
              </div>
            </div>
          </div>

          <!-- Edit Modal -->
          <div class="modal fade bd-example-modal-lg" id="editResicode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Edit Resi Code</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                <form class="edit" action="#" id="resicode_form">
                  <div class="form-group">
                    <label for="name" class="col-form-label">Resi Code:</label>
                    <div class="text-left" id="error_u_resicode" style="padding: 2px; color:red;"></div>
                    <input type="text" class="form-control" id="u_resicode">
                  </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="resicodeUpdate" onclick="resicode_update(this.name)">Update</button>
                </div>
              </div>
            </div>
          </div>

      @endsection