@extends('backend.dashboard')
@section('title','Viatushop - Inventory')
@section('content')

          <!-- DataTales Example -->
         
          <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Viatushop - Inventory</h6>
              </div>
            <div class="card-body">
              <a href="#" onclick="" class="btn btn-success btn-square" data-toggle="modal" data-target="#Inventory" data-whatever="#" style="margin-bottom: 20px;">
                <i class="fas fa-plus"></i>
              </a>
                
              <div class="table-responsive">
                <table class="table table-bordered display" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Stuff Name</th>
                      <th>Stuff Code</th>
                      <th>Unit</th>
                      <th>Stuff Description</th>
                      <th style="width:120px;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
                    @foreach($inventory as $row)
                    <tr>
                      <td>{{ $no }}</td>
                      <td>{{ $row->stuff_name }}</td>
                      <td>{{ $row->stuff_code }}</td>
                      <td>{{ ($row->total == null ? 0 : $row->total) }}</td>
                      <td>{{ $row->stuff_description }}</td>
                      <td>
                        <div class="row">
                          <a href="{{ url('stuff/'.$row->stuff_code) }}" class="btn btn-success btn-circle ml-2 mr-2" style="margin-bottom: 20px;">
                                    <i class="fas fa-info-circle"></i>
                          </a>
                          <a href="#" onclick="editInventory(this.id)" id="{{ $row->id_inv }}" class="btn btn-info btn-circle ml-2 mr-2">
                            <i class="fas fa-edit"></i>
                          </a>
                          <a href="#" id="{{ $row->id_inv }}" name="inventory" class="btn btn-danger btn-circle" onclick="delete_data(this.id,this.name)">
                            <i class=" fas fa-trash"></i>
                          </a>
                      </div>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>

           <!-- Shoes Modal -->
      <div class="modal fade bd-example-modal-lg" id="Inventory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Inventory</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <form id="inventory_form">
                      <div class="form-group">
                        <label for="stuff-name" class="col-form-label">Stuff Name:</label>
                        <div class="text-left" id="error_stuff_name" style="padding: 2px; color:red;"></div>
                        <input type="text" class="form-control" id="stuff_name">
                      </div>
                      <div class="form-group">
                        <label for="stuff-code" class="col-form-label">Stuff Code:</label>
                        <div class="text-left" id="error_stuff_code" style="padding: 2px; color:red;"></div>
                        <input type="text" class="form-control" id="stuff_code">
                      </div>
                      <div class="form-group">
                      <label for="description" class="col-form-label">Stuff Description:</label>
                      <div class="text-left" id="error_stuff_description" style="padding: 2px; color:red;"></div>
                      <textarea class="form-control" id="stuff_description"></textarea>
                    </div>
                  </form>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" id="inventory_save" onClick="inventory_save();" class="btn btn-primary">Save</button>
                  </div>
              </div>
            </div>
          </div>
        </div>

        <!-- edit shoes modal -->
        <div class="modal fade bd-example-modal-lg" id="editInventory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Inventory</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
              <form id="update_inventory_form">
                      <div class="form-group">
                        <label for="stuff-name" class="col-form-label">Stuff Name:</label>
                        <div class="text-left" id="error_u_stuff_name" style="padding: 2px; color:red;"></div>
                        <input type="text" class="form-control" id="u_stuff_name">
                      </div>
                      <div class="form-group">
                        <label for="stuff-code" class="col-form-label">Stuff Code:</label>
                        <div class="text-left" id="error_u_stuff_code" style="padding: 2px; color:red;"></div>
                        <input type="text" class="form-control" id="u_stuff_code">
                      </div>
                      <div class="form-group">
                      <label for="description" class="col-form-label">Stuff Description:</label>
                      <div class="text-left" id="error_u_stuff_description" style="padding: 2px; color:red;"></div>
                      <textarea class="form-control" id="u_stuff_description"></textarea>
                    </div>
                  </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" id="inventory_update" onclick="update_inventory(this.id)" class="btn btn-primary">Update</button>
                  </div>
              </div>
            </div>
          </div>
        </div>

         <!-- =========================== CAROUSEL =========================== -->
         <div class="modal fade bd-example-modal-lg" id="carousel_thumbnail" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Preview</h5>
              <button type="button" class="close" onclick="close_slideshow();" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <div id="mdb-lightbox-ui"></div>
              <div id="carousel_indicators" class="carousel slide" data-ride="carousel">
                <ol id="thumbnail_indicators" class="carousel-indicators">
                  <li data-target="#carousel_indicators" data-slide-to="0" class="active"></li>
                </ol>
                <div id="thumbnail_item" class="carousel-inner">
                  <div class="carousel-item active">
                    <img class="d-block w-100" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17299ff5b6f%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17299ff5b6f%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="First slide">
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carousel_indicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel_indicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" onclick="close_slideshow();">Close</button>
            </div>
          </div>
        </div>
      </div>
      
      <div class="modal fade bd-example-modal-lg" id="carousel_uploaded" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Uploaded Image</h5>
              <button type="button" class="close" onclick="close_uploaded_thumbnail();" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <div id="mdb-lightbox-ui"></div>
              <div id="carousel_uploaded_indicators" class="carousel slide" data-ride="carousel">
                <ol id="uploaded_indicators" class="carousel-indicators">
                
                  <?php $count=0; ?>
                </ol>
                <div id="uploaded_item" class="carousel-inner">
               
                </div>
                <a class="carousel-control-prev" href="#carousel_uploaded_indicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel_uploaded_indicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" onclick="close_uploaded_thumbnail();">Close</button>
            </div>
          </div>
        </div>
      </div>
      @endsection