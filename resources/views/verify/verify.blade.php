@extends('backend.dashboard')
@section('title','Viatushop - Purchase Order Verification')
@section('content')

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">@yield('title')</h6>
              
            </div>
            <div class="card-body">
              <div class="row mb-4">
                <div class="col-2">
                  <label for="first_date">Start Date:</label><br>
                  <input class="form-control" type="date" id="start_date">
                </div>
                <div class="col-2">
                  <label for="second_date">End Date:</label><br>
                  <input class="form-control" type="date" id="end_date">
                </div>
                <div class="col-2">
                  <label for="status">Status:</label>
                  <select class="form-control" id="status">
                    <option selected value="10">Choose...</option>
                    <option value="0">Waiting</option>
                    <option value="1">Is Doing</option>
                    <option value="2">Done</option>
                </select>
                </div>
                <div class="col-2">
                    <label for="status">Status Verification:</label>
                    <select class="form-control" id="status_verification">
                      <option selected value="10">Choose...</option>
                      <option value="0">Unverified</option>
                      <option value="1">Verified</option>
                      <option value="2">Rejected</option>
                  </select>
                  </div>
                <div class="col-2">
                  <label for="action">Action:</label><br>
                  <button class="btn btn-success btn-square" id="go_ver">Go</>
                </div>
              </div>

              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Customer Name</th>
                      <th>Po Code</th>
                      <th>Date Order</th>
                      <th>Deadline</th>
                      <th>Status</th>
                      <th>Handler</th> 
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
                    @foreach($po as $row)
                    <tr>
                    
                      <td>{{ $no }}</td>
                      <td>{{ $row->customer_name }}</td>
                      <td>{{ $row->id_po }}</td>
                      <td>{{ $row->date_order }}</td>
                      <td>{{ $row->deadline }}</td>
                      <td>
                        @switch($row->status_verification)
                            @case(0)
                              Unverified
                              @break
                            @case(1)
                              Verified
                              @break
                            @case(2)
                              Rejected
                              @break
                        @endswitch
                      </td>
                      <td>{{ $row->username }}</td>
                      <td>
                      <div class="row">
                          <a href="{{ url('po_view/' . $row->id ) }}" class="btn btn-info btn-circle ml-3">
                            <i class="fas fa-eye"></i>
                          </a> 
                          @if($row->status_verification==0)
                            <button class="btn btn-success btn-circle ml-3" style="margin-bottom: 20px;" id="{{ $row->id }}" name="verify" onclick="update_data(this.id,this.name)">
                              <i class="fas fa-check-circle"></i>
                            </button>
                            <button class="btn btn-danger btn-circle ml-3 mr-3" style="margin-bottom: 20px;" id="{{ $row->id }}" name="reject_po" onclick="update_data(this.id,this.name)">
                              <i class="fa fa-ban" aria-hidden="true"></i>
                            </button>
                          @elseif($row->status_verification==1)
                            <button class="btn btn-danger btn-circle ml-3" style="margin-bottom: 20px;" id="{{ $row->id }}" name="verify_cancel" onclick="update_data(this.id,this.name)">
                              <i class="fas fa-window-close"></i>
                            </button>
                          @endif
                        </div>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                  </tbody>
                 
                </table>
              </div>
            </div>
          </div>
      @endsection