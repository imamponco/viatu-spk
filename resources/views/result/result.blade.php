@extends('backend.dashboard')
@section('title','Viatushop - COPRAS-G')
@section('content')

         <!-- Bar Chart -->
         <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Priority Chart</h6>
            </div>
            <div class="card-body">
              <div class="chart-bar">
                <canvas id="myBarChart"></canvas>
              </div>
            </div>
          </div>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Initial Decision-making Matrix</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered display text-center text-center" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Purchase Order No.</th>
                      @foreach($criterias as $row)
                      <th colspan="2">{{ $row->criteria_name }}</th>
                      @endforeach
                    </tr>
                    <tr>
                        <th>Opitimization Direction</th>
                        @foreach($criterias as $row)
                        <th colspan="2">{{ ($row->optimization_direction==1 ? "Max":"Min") }}</th>
                        @endforeach
                    </tr>
                    <tr>
                        <th>Atribute Weight</th>
                        @foreach($criterias as $row)
                        <th colspan="2">{{ $row->eigen_value }}</th>
                        @endforeach
                    </tr>
                    <tr>
                        <th></th>
                        @php
                            $count=1;
                        @endphp
                        @foreach ($criterias as $row)
                        <th colspan="2">Y{{ $count }}</th>
                        @php
                            $count++;
                        @endphp
                        @endforeach

                    </tr>
                    <tr>
                        <th></th>
                        @foreach ($criterias as $row)
                        <th>Y1</th>
                        <th>Y2</th>
                        @endforeach
                    </tr>
                  </thead>
                  <tbody>
                    @php $count=1; @endphp
                    @foreach($po as $p)
                    <tr id="row-{{ $row->id_po }}">
                        <th>{{ $p->id_po }}</th>
                        @foreach($result as $row)
                         @if($p->id_po == $row->id_po)
                            <th>{{ $row->min_value }}</th>
                            <th>{{ $row->max_value }}</th>
                         @endif
                        @endforeach
                        @php $count++; @endphp
                        </tr>
                    @endforeach
                  </tbody>

                </table>
              </div>
            </div>
          </div>

          <!-- Matrix Nilai Kriteria -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Matrix Normalized COPRAS-G method</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered display text-center" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <th>Purchase Order No.</th>
                          @foreach($criterias as $row)
                          <th colspan="2">{{ $row->criteria_name }}</th>
                          @endforeach
                        </tr>
                        <tr>
                            <th>Opitimization Direction</th>
                            @foreach($criterias as $row)
                            <th colspan="2">{{ ($row->optimization_direction==1 ? "Max":"Min") }}</th>
                            @endforeach
                        </tr>
                        <tr>
                            <th>Atribute Weight</th>
                            @foreach($criterias as $row)
                            <th colspan="2">{{ $row->eigen_value }}</th>
                            @endforeach
                        </tr>
                        <tr>
                            <th></th>
                            @php
                                $count=1;
                            @endphp
                            @foreach ($criterias as $row)
                            <th colspan="2">Y{{ $count }}</th>
                            @php
                                $count++;
                            @endphp
                            @endforeach

                        </tr>
                        <tr>
                            <th></th>
                            @foreach ($criterias as $row)
                            <th>Y1</th>
                            <th>Y2</th>
                            @endforeach
                        </tr>
                      </thead>
                  <tbody>
                    @php $count=1; $count2=0; $limit = count($criterias); @endphp
                    @foreach($po as $p)
                    <tr id="row-{{ $row->id_po }}">
                        <th>{{ $p->id_po }}</th>
                        @foreach($result as $row)
                            @if ($count2 == $limit)
                                @php $count2 = 0; @endphp
                            @endif
                            @if($p->id_po == $row->id_po)
                                <th>{{ number_format(round((2*$row->min_value)/($norm[$count2]->amount ==0 ? 1 : $norm[$count2]->amount),3), 3, '.', '') }}</th>
                                <th>{{ number_format(round((2*$row->max_value)/($norm[$count2]->amount ==0 ? 1 : $norm[$count2]->amount),3), 3, '.', '') }}</th>
                            @php $count2++; @endphp
                            @endif
                        @endforeach
                        @php $count++; @endphp
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <!-- Matrix Penjumlahan Tiap Baris -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Matrix Weighted COPRAS-G method</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered display text-center" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <th>Purchase Order No.</th>
                          @foreach($criterias as $row)
                          <th colspan="2">{{ $row->criteria_name }}</th>
                          @endforeach
                        </tr>
                        <tr>
                            <th>Opitimization Direction</th>
                            @foreach($criterias as $row)
                            <th colspan="2">{{ ($row->optimization_direction==1 ? "Max":"Min") }}</th>
                            @endforeach
                        </tr>
                        <tr>
                            <th>Atribute Weight</th>
                            @foreach($criterias as $row)
                            <th colspan="2">{{ $row->eigen_value }}</th>
                            @endforeach
                        </tr>
                        <tr>
                            <th></th>
                            @php
                                $count=1;
                            @endphp
                            @foreach ($criterias as $row)
                            <th colspan="2">Y{{ $count }}</th>
                            @php
                                $count++;
                            @endphp
                            @endforeach

                        </tr>
                        <tr>
                            <th></th>
                            @foreach ($criterias as $row)
                            <th>Y1</th>
                            <th>Y2</th>
                            @endforeach
                        </tr>
                      </thead>
                  <tbody>
                    @php $count=1; $count2=0; $limit = count($criterias); $min=0; $max=0; $srj=0; $sprj=0;@endphp
                    @foreach($po as $p)
                    <tr id="row-{{ $row->id_po }}">
                        <th>{{ $p->id_po }}</th>
                        @foreach($result as $row)
                            @if ($count2 == $limit)
                                @php $count2 = 0; @endphp
                            @endif
                            @if($p->id_po == $row->id_po)
                            @php $x1 = number_format($norm[$count2]->eigen_value*round((2*$row->min_value)/($norm[$count2]->amount == 0 ? 1 : $norm[$count2]->amount  ),3), 3, '.', '');
                                 $x2 = number_format($norm[$count2]->eigen_value*round((2*$row->max_value)/($norm[$count2]->amount == 0 ? 1 : $norm[$count2]->amount  ),3), 3, '.', '');
                            @endphp
                               @if ($norm[$count2]->optimization_direction==0)
                                 @php $min += ($x1+$x2); @endphp
                               @else
                                 @php $max += ($x1+$x2); @endphp
                               @endif

                                <th>{{ $x1 }}</th>
                                <th>{{ $x2 }}</th>
                            @php $count2++; $pj=$max/2; $rj=$min/2; @endphp
                            @endif
                        @endforeach
                        {{-- {{dd($norm[$count2]->optimization_direction)}} --}}
                        @php $count++; $srj= ($min!=0 ? $srj += $rj : 0 ); $sprj=($min!=0 ? $sprj += (1/$rj) : 0 ); $count++; $min=0; $max=0; @endphp
                        </tr>
                    @endforeach
                  </tbody>

                </table>
              </div>
            </div>
          </div>

          <!-- Matrix Hitung Ratio Konsistensi  -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Result</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered display text-center" id="dataTable" width="100%" cellspacing="0">
                  <thead>

                    <tr>
                      <th>No</th>
                      <th>Pj</th>
                      <th>Rj</th>
                      <th>Qj</th>
                      <th>Nj</th>
                      <th>Rank</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php $count=1; $count2=0; $limit = count($criterias); $min=0; $max=0; $q=array(); @endphp
                    @foreach($po as $p)
                    <tr id="nj-{{ $p->id_po }}">
                        <th><a href="{{ url('judgement/'.$p->id_po) }}">{{ $p->id_po }}</a></th>
                        @foreach($result as $row)
                            @if ($count2 == $limit)
                                @php $count2 = 0; @endphp
                            @endif
                            @if($p->id_po == $row->id_po)
                            @php $x1 = number_format($norm[$count2]->eigen_value*round((2*$row->min_value)/($norm[$count2]->amount ==0 ? 1 : $norm[$count2]->amount  ),3), 3, '.', '');
                                 $x2 = number_format($norm[$count2]->eigen_value*round((2*$row->max_value)/($norm[$count2]->amount ==0 ? 1 : $norm[$count2]->amount  ),3), 3, '.', '');
                            @endphp
                               @if ($norm[$count2]->optimization_direction==0)
                                 @php $min += ($x1+$x2); @endphp
                               @else
                                 @php $max += ($x1+$x2); @endphp
                               @endif
                            @php $count2++; $pj=$max/2; $rj=$min/2; @endphp
                            @endif
                        @endforeach
                        @php $qj = ($srj != 0 ? ($pj+$srj/($rj*$sprj)) : $pj );
                             $q[$count-1] = number_format($qj, 3, '.', '');
                        @endphp
                        <th>{{ number_format($pj,3,'.','') }}</th>
                        <th>{{ number_format($rj,4, '.', '') }}</th>
                        <th>{{ number_format($qj, 3, '.', '') }}</th>
                        @php $count++; $min=0; $max=0; $qj=0;@endphp
                        </tr>
                    @endforeach
                    @php $qmax = (!empty($q)== true ? max($q) : null );  @endphp
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <!-- Matrix History Dss -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">History DSS</h6>
            </div>
            <div class="card-body">
              <div class="row">
                <a href="#" id="download_history" name="download_history" class="btn btn-primary btn-icon-split ml-4" style="margin-bottom: 20px;">
                  <span class="icon text-white-50">
                    <i class="fas fa-download"></i>
                  </span>
                  <span class="text">Download</span>
                </a>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered display text-center" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Decisions Maker</th>
                      <th>Po Code</th>
                      <th>Descriptions</th>
                      <th>Decision Date</th>
                      <th>Production Date</th>
                      <th>Finish Date</th>
                      <th>Status Po</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php
                     $no = 1;   
                    @endphp
                    @foreach ($history as $dss)
                      <tr>
                      <td>{{ $no }}</td>
                      <td>{{ $dss->name }}</td>
                      <th><a href="{{ url('judgement/'.$dss->id_po) }}">{{ $dss->id_po }}</a></th>
                      <td>{{ $dss->description_dss }}</td>
                      <td>{{ $dss->created_at->format('Y-m-d') }}</td>
                      <td>{{ $dss->date_production }}</td>
                      <td>{{ $dss->date_finish }}</td>
                      <td>
                        @switch($dss->status)
                          @case(0)
                            Waiting
                            @break
                          @case(1 && NOW() >= $dss->date_production)
                            Is Doing
                            @break
                          @case(1 && NOW() < $dss->date_production)
                            Will doing at {{ $dss->date_production }}
                            @break
                          @case(2)
                            Done
                            @break
                        @endswitch
                      </td>
                      <td>
                        <a href="#" id="{{ $dss->id_dss }}" name="history_dss" class="btn btn-danger btn-circle" onclick="delete_data(this.id,this.name)">
                          <i class=" fas fa-trash"></i>
                        </a>
                      </td>
                      @php
                        $no++;
                      @endphp
                    @endforeach
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          {{-- rank --}}
          @php $rank = rank($q); @endphp

        <!-- Page level custom scripts -->
        {{-- <script src="{{ asset('admin/js/demo/chart-bar-demo.js') }}"></script> --}}

        <script>
            @php $count=0; @endphp

            @foreach($po as $p)

                var nj = "<th>{{ number_format(($q[$count]/($qmax ==0 ? 1 : $qmax  )) * 100, 1, '.', '') }}%</th>"
                var rank = "";
                var action = "";

                @for($i=0; $i<count($rank); $i++)
                    @if($q[$count]==$rank[$i]["value"])
                        var rank = "<th>"+{{$rank[$i]["rank"]}}+"</th>"

                    @endif
                @endfor

                var action = "<th><button class='btn btn-success btn-circle ml-3' style='margin-bottom: 20px;' id='{{ $p->id_po }}' name='is_doing' onclick='update_data(this.id,this.name)'><i class='fas fa-check-circle'></i></button></th>"

                $('#nj-'+'{{ $p->id_po }}').append(nj)
                $('#nj-'+'{{ $p->id_po }}').append(rank)
                $('#nj-'+'{{ $p->id_po }}').append(action)

                @php $count++ @endphp
            @endforeach

            // Set new default font family and font color to mimic Bootstrap's default styling
            Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
            Chart.defaults.global.defaultFontColor = '#858796';

            function number_format(number, decimals, dec_point, thousands_sep) {
            // *     example: number_format(1234.56, 2, ',', ' ');
            // *     return: '1 234,56'
            number = (number + '').replace(',', '').replace(' ', '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function(n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
            }

            // Bar Chart Example
            var ctx = document.getElementById("myBarChart");
            var myBarChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [
                    @foreach($po as $p)
                    "{{ $p->id_po }}",
                    @endforeach
                    ],
                datasets: [{
                label: "Priority",
                backgroundColor: "#4e73df",
                hoverBackgroundColor: "#2e59d9",
                borderColor: "#4e73df",
                data: [
                    @php $count=0; @endphp
                    @foreach($po as $p)
                    {{ number_format(($q[$count]/($qmax == 0 ? 1 : $qmax )) * 100, 1, '.', '') }},
                    @php $count++; @endphp
                    @endforeach

                ],
                }],
            },
            options: {
                maintainAspectRatio: false,
                layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
                },
                scales: {
                xAxes: [{
                    time: {
                    unit: 'alternative'
                    },
                    gridLines: {
                    display: false,
                    drawBorder: false
                    },
                    ticks: {
                    maxTicksLimit: 6
                    },
                    maxBarThickness: 25,
                }],
                yAxes: [{
                    ticks: {
                    min: 0,
                    max: 100,
                    maxTicksLimit: 5,
                    padding: 10,
                    // Include a dollar sign in the ticks
                    callback: function(value, index, values) {
                        return '%' + number_format(value);
                    }
                    },
                    gridLines: {
                    color: "rgb(234, 236, 244)",
                    zeroLineColor: "rgb(234, 236, 244)",
                    drawBorder: false,
                    borderDash: [2],
                    zeroLineBorderDash: [2]
                    }
                }],
                },
                legend: {
                display: false
                },
                tooltips: {
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                caretPadding: 10,
                callbacks: {
                    label: function(tooltipItem, chart) {
                    var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                    return datasetLabel + ': %' + number_format(tooltipItem.yLabel);
                    }
                }
                },
            }
            });

        </script>

        @php

        function rank($numbers){

            $result = array();

            rsort($numbers);

            $arrlength = count($numbers);
            $rank = 1;
            $rt=1;

            for($x = 0; $x < $arrlength; $x++) {

                $result[$x] = "";
                if ($x==0) {
                     $result[$x] = [
                         "rank" => $rank,
                         "value" => $numbers[$x]
                     ] ;
                }
            elseif ($numbers[$x] != $numbers[($x-1)]) {
                        $rank++;
                        $result[$x] = [
                         "rank" => $rank,
                         "value" => $numbers[$x]
                        ] ;

                    }
            else{
                    $result[$x] = [
                         "rank" => $rank,
                         "value" => $numbers[$x]
                     ] ;

                }
            }
            return $result;
        }
        @endphp

      @endsection
