<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['web'])->group(function () {
    Route::middleware(['auth'])->group(function () {

        Route::get('make_report/{id}','ReportController@cetak_pdf');

        Route::middleware(['role_check:Manager Production'])->group(function () {
            Route::resources([
                'users'     => 'AuthController',
                'criteria'  => 'CriteriaController',
                'activity'  => 'ActivityLogController',
                'judgement' => 'JudgementController',
                'result'    => 'ResultController'
            ]);

            //Is doing
            Route::patch('is_doing/{id}', 'PurchaseOrderController@is_doing');

            //Activity
            Route::delete('delete_all','ActivityLogController@destroy_all');

            //History DSS
            Route::delete('history_dss/{id}','ActivityLogController@destroy_dss');
            Route::get('download_history','ActivityLogController@download_history');

            //Judgement
            Route::get('detail_view/{id}','JudgementController@detail_view');

            //Route Weights
            Route::get('weight','CriteriaController@index_weight');
            Route::get('weights','CriteriaController@reload');
            Route::get('weight/{weight}','CriteriaController@show_weight');
            Route::post('weight','CriteriaController@store_weight');
            Route::patch('weight/{weight}','CriteriaController@update_weight');
            Route::delete('weight/{weight}','CriteriaController@delete_weight');

        });

        // Route::middleware(['role_check:Manager Marketing'])->group(function () {

        //     Route::get('verify','PurchaseOrderController@verify_index');
        //     //Detail
        //     Route::get('po_view/{id}','PurchaseOrderController@po_view');

        //     Route::get('po_detail/{id}','PurchaseOrderController@po_detail');


        //     Route::patch('verify/{id}', 'PurchaseOrderController@verify');
        //     Route::patch('verify_cancel/{id}','PurchaseOrderController@cancel_verify');
        //     Route::patch('reject_po/{id}','PurchaseOrderController@reject_po');

        // });

        Route::middleware(['role_check:Marketing,Manager Production,Administrator'])->group(function () {
            Route::resources([
                'shoe'          => 'ShoesModelController',
            ]);
            //notification
            Route::patch('read_notification','ActivityLogController@read_notification');
            //po
            Route::get('get_po/{parameter}','PurchaseOrderController@get_po');
            //all check quality
            Route::get('check_all/{idpo}','QualityController@check_all');

        });

        Route::middleware(['role_check:Marketing'])->group(function () {
            Route::resources([
                'customers'     => 'CustomersController',
                'po'            => 'PurchaseOrderController',
                'additional'    => 'AdditionalController',
            ]);



            //shoes
            Route::get('get_shoes/{id}','ShoesModelController@get_all_shoes');
            Route::post('tr_shoe','ShoesModelController@store_tr_shoes');
            Route::delete('tr_shoe/{id}','ShoesModelController@destroy_tr_shoes');

            //Image
            Route::post('upload_image','ShoesModelController@upload_image');
            Route::post('update_image/{id}','ShoesModelController@update_image');
            Route::delete('image/{id}','ShoesModelController@delete_image');

            //detail
            Route::get('detail/{id}','DetailController@index');
            Route::get('detail/{id}/edit','DetailController@show');
            Route::post('detail/{id}','DetailController@store');
            Route::patch('detail/{id}','DetailController@update');
            Route::delete('detail/{id}','DetailController@destroy');
            Route::delete('delete_detail/{id}','DetailController@destroy_detail');
        });

        Route::middleware(['role_check:Administrator'])->group(function () {
            Route::resources([
                'quality'   => 'QualityController',
                'do'        => 'DeliveryController'
            ]);

            Route::patch('quality_cancel/{id}','QualityController@cancel');

            Route::patch('do_cancel/{id}','DeliveryController@cancel');
            Route::patch('update_resicode/{id}','DeliveryController@update_resicode');
        });

        Route::middleware(['role_check:Warehouse'])->group(function () {
            Route::resources([
               'inventory' => 'InventoryController',
               'stuff'     => 'StuffTransactionsController'
            ]);
        });

       Route::get('dashboard','AuthController@main_dashboard')->name('dashboard');
       Route::post('logout','AuthController@logout');

    });

    //Auth
    Route::post('do_login','Auth\LoginController@do_login');
    // Route::view('/','login')->middleware('auth');
    Route::get('/', function () {
        return view('login');
    })->name('login')->middleware('login_check');

});

